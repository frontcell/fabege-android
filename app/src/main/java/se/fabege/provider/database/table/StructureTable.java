package se.fabege.provider.database.table;

public interface StructureTable {
    String TABLE_NAME = "structure";

    String _ID = "_id";

    String PROPERTY_ID = "property_id";
    String SOURCE = "source";
    String DESTINATION_ID = "destination_id";
    String NAME = "name";
    String DESCRIPTION = "description";
    String LONGITUDE = "longitude";
    String LATITUDE = "latitude";
    String IS_FAVOURITE = "is_favourite";
    String UPDATED = "updated";
    String[] ALL_COLUMNS = new String[]{_ID, PROPERTY_ID, SOURCE, DESTINATION_ID, NAME, DESCRIPTION, LONGITUDE, LATITUDE, IS_FAVOURITE, UPDATED};

    String SQL_CREATE = "CREATE TABLE structure ( _id INTEGER PRIMARY KEY AUTOINCREMENT, property_id TEXT, source TEXT, destination_id TEXT, name TEXT, description TEXT, longitude TEXT, latitude TEXT, is_favourite INTEGER, updated TEXT )";

    String SQL_INSERT = "INSERT INTO structure ( property_id, source, destination_id, name, description, longitude, latitude, is_favourite, updated ) VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ? )";

    String SQL_DROP = "DROP TABLE IF EXISTS structure";

    String WHERE_ID_EQUALS = _ID + "=?";

}