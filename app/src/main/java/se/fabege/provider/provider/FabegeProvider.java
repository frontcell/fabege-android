package se.fabege.provider.provider;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.provider.BaseColumns;
import android.text.TextUtils;

import java.util.ArrayList;
import java.util.List;

import se.fabege.provider.database.FabegeDatabase;
import se.fabege.provider.database.table.StructureTable;

public class FabegeProvider extends ContentProvider {

    public static final String AUTHORITY = "se.fabege.provider.provider";

    public static final Uri AUTHORITY_URI = Uri.parse("content://" + AUTHORITY);

    public static final Uri STRUCTURE_CONTENT_URI = Uri.withAppendedPath(FabegeProvider.AUTHORITY_URI, StructureContent.CONTENT_PATH);


    private static final UriMatcher URI_MATCHER;
    protected FabegeDatabase mDatabase;

    private static final int STRUCTURE_DIR = 0;
    private static final int STRUCTURE_ID = 1;


    static {
        URI_MATCHER = new UriMatcher(UriMatcher.NO_MATCH);
        URI_MATCHER.addURI(AUTHORITY, StructureContent.CONTENT_PATH, STRUCTURE_DIR);
        URI_MATCHER.addURI(AUTHORITY, StructureContent.CONTENT_PATH + "/#", STRUCTURE_ID);

    }

    public static final class StructureContent implements BaseColumns {
        public static final String CONTENT_PATH = "structure";
        public static final String CONTENT_TYPE = "vnd.android.cursor.dir/vnd.fabege_database.structure";
        public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/vnd.fabege_database.structure";
    }


    @Override
    public final boolean onCreate() {
        mDatabase = new FabegeDatabase(getContext());
        return true;
    }

    @Override
    public final String getType(final Uri uri) {
        switch (URI_MATCHER.match(uri)) {
            case STRUCTURE_DIR:
                return StructureContent.CONTENT_TYPE;
            case STRUCTURE_ID:
                return StructureContent.CONTENT_ITEM_TYPE;

            default:
                throw new IllegalArgumentException("Unsupported URI: " + uri);
        }
    }

    @Override
    public final Cursor query(final Uri uri, String[] projection, final String selection, final String[] selectionArgs, final String sortOrder) {
        final SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
        final SQLiteDatabase dbConnection = mDatabase.getReadableDatabase();

        switch (URI_MATCHER.match(uri)) {
            case STRUCTURE_ID:
                queryBuilder.appendWhere(StructureTable._ID + "=" + uri.getLastPathSegment());
            case STRUCTURE_DIR:
                queryBuilder.setTables(StructureTable.TABLE_NAME);
                break;

            default:
                throw new IllegalArgumentException("Unsupported URI:" + uri);
        }

        Cursor cursor = queryBuilder.query(dbConnection, projection, selection, selectionArgs, null, null, sortOrder);
        cursor.setNotificationUri(getContext().getContentResolver(), uri);

        return cursor;

    }


    @Override
    public final Uri insert(final Uri uri, final ContentValues values) {
        final SQLiteDatabase dbConnection = mDatabase.getWritableDatabase();

        try {
            dbConnection.beginTransaction();

            switch (URI_MATCHER.match(uri)) {
                case STRUCTURE_DIR:
                case STRUCTURE_ID:
                    final long structureId = dbConnection.insertOrThrow(StructureTable.TABLE_NAME, null, values);
                    final Uri newStructureUri = ContentUris.withAppendedId(STRUCTURE_CONTENT_URI, structureId);
                    getContext().getContentResolver().notifyChange(newStructureUri, null);

                    dbConnection.setTransactionSuccessful();
                    return newStructureUri;
                default:
                    throw new IllegalArgumentException("Unsupported URI:" + uri);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            dbConnection.endTransaction();
        }

        return null;
    }

    @Override
    public final int update(final Uri uri, final ContentValues values, final String selection, final String[] selectionArgs) {
        final SQLiteDatabase dbConnection = mDatabase.getWritableDatabase();
        int updateCount = 0;
        List<Uri> joinUris = new ArrayList<Uri>();

        try {
            dbConnection.beginTransaction();

            switch (URI_MATCHER.match(uri)) {
                case STRUCTURE_DIR:
                    updateCount = dbConnection.update(StructureTable.TABLE_NAME, values, selection, selectionArgs);

                    dbConnection.setTransactionSuccessful();
                    break;
                case STRUCTURE_ID:
                    final long structureId = ContentUris.parseId(uri);
                    updateCount = dbConnection.update(StructureTable.TABLE_NAME, values,
                            StructureTable._ID + "=" + structureId + (TextUtils.isEmpty(selection) ? "" : " AND (" + selection + ")"), selectionArgs);

                    dbConnection.setTransactionSuccessful();
                    break;

                default:
                    throw new IllegalArgumentException("Unsupported URI:" + uri);
            }
        } finally {
            dbConnection.endTransaction();
        }

        if (updateCount > 0) {
            getContext().getContentResolver().notifyChange(uri, null);

            for (Uri joinUri : joinUris) {
                getContext().getContentResolver().notifyChange(joinUri, null);
            }
        }

        return updateCount;

    }

    @Override
    public final int delete(final Uri uri, final String selection, final String[] selectionArgs) {
        final SQLiteDatabase dbConnection = mDatabase.getWritableDatabase();
        int deleteCount = 0;
        List<Uri> joinUris = new ArrayList<Uri>();

        try {
            dbConnection.beginTransaction();

            switch (URI_MATCHER.match(uri)) {
                case STRUCTURE_DIR:
                    deleteCount = dbConnection.delete(StructureTable.TABLE_NAME, selection, selectionArgs);

                    dbConnection.setTransactionSuccessful();
                    break;
                case STRUCTURE_ID:
                    deleteCount = dbConnection.delete(StructureTable.TABLE_NAME, StructureTable.WHERE_ID_EQUALS, new String[]{uri.getLastPathSegment()});

                    dbConnection.setTransactionSuccessful();
                    break;

                default:
                    throw new IllegalArgumentException("Unsupported URI:" + uri);
            }
        } finally {
            dbConnection.endTransaction();
        }

        if (deleteCount > 0) {
            getContext().getContentResolver().notifyChange(uri, null);

            for (Uri joinUri : joinUris) {
                getContext().getContentResolver().notifyChange(joinUri, null);
            }
        }

        return deleteCount;
    }
}