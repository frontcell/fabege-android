package se.fabege.provider.model;

import android.content.ContentValues;
import android.database.Cursor;
import android.location.Location;

import java.util.ArrayList;
import java.util.List;

import se.fabege.provider.database.table.StructureTable;

public class Structure {
    private long mRowId;
    private String mPropertyId;
    private String mSource;
    private String mDestinationId;
    private String mName;
    private String mDescription;
    private String mLongitude;
    private String mLatitude;
    private int mIsFavourite;
    private String mUpdated;


    private ContentValues mValues = new ContentValues();
    private float mDistance;

    public Structure() {
    }

    public Structure(final Cursor cursor) {
        this(cursor, false);
    }

    public Structure(final Cursor cursor, boolean prependTableName) {
        String prefix = prependTableName ? StructureTable.TABLE_NAME + "_" : "";
        setRowId(cursor.getLong(cursor.getColumnIndex(prefix + StructureTable._ID)));
        setPropertyId(cursor.getString(cursor.getColumnIndex(prefix + StructureTable.PROPERTY_ID)));
        setSource(cursor.getString(cursor.getColumnIndex(prefix + StructureTable.SOURCE)));
        setDestinationId(cursor.getString(cursor.getColumnIndex(prefix + StructureTable.DESTINATION_ID)));
        setName(cursor.getString(cursor.getColumnIndex(prefix + StructureTable.NAME)));
        setDescription(cursor.getString(cursor.getColumnIndex(prefix + StructureTable.DESCRIPTION)));
        setLongitude(cursor.getString(cursor.getColumnIndex(prefix + StructureTable.LONGITUDE)));
        setLatitude(cursor.getString(cursor.getColumnIndex(prefix + StructureTable.LATITUDE)));
        setIsFavourite(cursor.getInt(cursor.getColumnIndex(prefix + StructureTable.IS_FAVOURITE)));
        setUpdated(cursor.getString(cursor.getColumnIndex(prefix + StructureTable.UPDATED)));

    }

    public ContentValues getContentValues() {
        return mValues;
    }

    public Long getRowId() {
        return mRowId;
    }

    public void setRowId(long _id) {
        mRowId = _id;
        mValues.put(StructureTable._ID, _id);
    }

    public void setPropertyId(String property_id) {
        mPropertyId = property_id;
        mValues.put(StructureTable.PROPERTY_ID, property_id);
    }

    public String getPropertyId() {
        return mPropertyId;
    }


    public void setSource(String source) {
        mSource = source;
        mValues.put(StructureTable.SOURCE, source);
    }

    public String getSource() {
        return mSource;
    }


    public void setDestinationId(String destination_id) {
        mDestinationId = destination_id;
        mValues.put(StructureTable.DESTINATION_ID, destination_id);
    }

    public String getDestinationId() {
        return mDestinationId;
    }


    public void setName(String name) {
        mName = name;
        mValues.put(StructureTable.NAME, name);
    }

    public String getName() {
        return mName;
    }


    public void setDescription(String description) {
        mDescription = description;
        mValues.put(StructureTable.DESCRIPTION, description);
    }

    public String getDescription() {
        return mDescription;
    }


    public void setLongitude(String longitude) {
        mLongitude = longitude;
        mValues.put(StructureTable.LONGITUDE, longitude);
    }

    public String getLongitude() {
        return mLongitude;
    }


    public void setLatitude(String latitude) {
        mLatitude = latitude;
        mValues.put(StructureTable.LATITUDE, latitude);
    }

    public String getLatitude() {
        return mLatitude;
    }


    public void setIsFavourite(int is_favourite) {
        mIsFavourite = is_favourite;
        mValues.put(StructureTable.IS_FAVOURITE, is_favourite);
    }

    public int getIsFavourite() {
        return mIsFavourite;
    }

    public boolean getIsFavouriteAsBool() {
        return mIsFavourite == 1;
    }


    public void setUpdated(String updated) {
        mUpdated = updated;
        mValues.put(StructureTable.UPDATED, updated);
    }

    public String getUpdated() {
        return mUpdated;
    }


    public static List<Structure> listFromCursor(Cursor cursor) {
        List<Structure> list = new ArrayList<Structure>();

        if (cursor != null && cursor.moveToFirst()) {
            do {
                list.add(new Structure(cursor));
            } while (cursor.moveToNext());
        }

        return list;
    }

    public void updateDistance(Location location) {
        if (location == null) {
            mDistance = -1;
        } else {
            Location loc1 = new Location("");
            loc1.setLatitude(Double.valueOf(mLatitude));
            loc1.setLongitude(Double.valueOf(mLongitude));

            mDistance = location.distanceTo(loc1);
        }
    }

    public double getDistance() {
        return mDistance;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Structure that = (Structure) o;

        if (!mName.equals(that.mName)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return mName.hashCode();
    }
}