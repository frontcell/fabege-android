package se.fabege.app.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.Loader;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.viewpagerindicator.CirclePageIndicator;

import org.simpleframework.xml.core.Persister;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import se.fabege.api.Api;
import se.fabege.api.TokenFactory;
import se.fabege.api.dto.params.WorkQueryParams;
import se.fabege.api.dto.session.SessionKey;
import se.fabege.api.dto.work.ListEntry;
import se.fabege.api.dto.work.WorkResponse;
import se.fabege.app.R;
import se.fabege.app.activity.HomeActivity;
import se.fabege.app.base.FabegeApplication;
import se.fabege.app.base.ProvidedBaseFragment;
import se.fabege.app.util.Utility;
import se.fabege.app.waw.ui.activity.waw.WAWActivity;
import se.fabege.provider.model.Structure;

/**
 * Created by Grindah on 2015-03-26.
 */
public class HomeFragment extends ProvidedBaseFragment implements Callback<String> {

    private IHomeFragment mListener;
    private FacilityAdapter mAdapter;
    private ViewPager mPager;
    private Structure mCurrentItem = null;
    private WorkResponse mOngoingWork;
    private WorkLoaderTask mWorkLoader;
    private View mWorkCountContainer;
    private TextView mWorkCount;
    private View mOngoingWorkButton;

    public static HomeFragment newInstance() {
        return new HomeFragment();
    }


    public interface IHomeFragment {
        void onSearchClicked();

        void onSubmitWorkClicked(Structure structure);

        void onViewWorkClicked();

        void onReadMoreClicked(String url);

        void onFaqClicked(Structure structure);

        void setActiveStructure(Structure structure);

        void setOngoingWork(WorkResponse response);

    }

    public HomeFragment() {
        // Need empty constructor.
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        startFavouriteLoader();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_fragment_home, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        boolean handled = false;
        switch (item.getItemId()) {
            case R.id.menu_search:
                Log.d(FabegeApplication.TAG, "Menu search clicked");
                mListener.onSearchClicked();
                handled = true;
            default:
                break;
        }
        return handled;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (IHomeFragment) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement IHomeFragment");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        ((HomeActivity) getActivity()).hideToolbar();
        View rootView = inflater.inflate(R.layout.fragment_home, container, false);

        View searchView = rootView.findViewById(R.id.searchView);
        searchView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onSearchClicked();
            }
        });
        View registerButton = rootView.findViewById(R.id.register_button);
        mWorkCountContainer = rootView.findViewById(R.id.workCountContainer);
        mOngoingWorkButton = rootView.findViewById(R.id.view_work_button);

        View readMoreButton = rootView.findViewById(R.id.read_more_button);
        View faqButton = rootView.findViewById(R.id.faq_button);

        View wawButton = rootView.findViewById(R.id.button2);

        mPager = (ViewPager) rootView.findViewById(R.id.viewpager);
        mAdapter = new FacilityAdapter();
        mWorkCount = (TextView) rootView.findViewById(R.id.workCount);
        mPager.setAdapter(mAdapter);

        CirclePageIndicator indicator = rootView.findViewById(R.id.indicator);
        indicator.setViewPager(mPager);

        indicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                mCurrentItem = mAdapter.getItemAt(mPager.getCurrentItem());
                mListener.setActiveStructure(mCurrentItem);
                updateWorkCount();
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onSubmitWorkClicked(mAdapter.getItemAt(mPager.getCurrentItem()));

            }
        });

        mOngoingWorkButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int count = 0;
                if (mOngoingWork != null) {
                    Structure item = mAdapter.getItemAt(mPager.getCurrentItem());
                    List<ListEntry> entriesForId = mOngoingWork.getEntriesForId(Integer.valueOf(item.getPropertyId()).intValue());
                    count = entriesForId.size();
                }

                if (count > 0) {
                    mListener.onViewWorkClicked();
                } else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(getActivity(), R.style.Theme_AppCompat_Light_Dialog));
                    builder.setMessage(R.string.ongoing_work_dialog);
                    builder.setPositiveButton("Ok", null);
                    builder.show();
                }

            }
        });

        readMoreButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Structure item = mAdapter.getItemAt(mPager.getCurrentItem());
                if (item != null) {
                    mListener.onReadMoreClicked("https://www.fabege.se/fastigheter/" +
                            item.getDescription().replace(" ", "-").replace("ö", "o").replace("å", "a").replace("ä", "a") + "/");
                }
            }
        });

        faqButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Structure item = mAdapter.getItemAt(mPager.getCurrentItem());
                if (item != null) {
                    mListener.onFaqClicked(item);
                }
            }
        });

        wawButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), WAWActivity.class));
            }
        });
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().setTitle("");
    }

    @Override
    public void onPause() {
        super.onPause();
    }


    @Override
    public void success(String s, Response response) {
        Log.d(FabegeApplication.TAG, "GCM Successfully updated subscription ids");
    }

    @Override
    public void failure(RetrofitError error) {
        Log.d(FabegeApplication.TAG, "GCM Failed to update subscription ids: " + error);

    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        switch (loader.getId()) {

            case LOADER_FAVOURITES:

                List<Structure> structures = Structure.listFromCursor(data);
                int index = structures.indexOf(mCurrentItem);
                Log.d(FabegeApplication.TAG, "Debug LOADER_FAVOURITES");
                mAdapter.setData(structures);
                mPager.setCurrentItem(index);

                Structure item = mAdapter.getItemAt(mPager.getCurrentItem());
                mListener.setActiveStructure(item);

                // load ongoing work:
                loadOngoingWork(structures);
                break;

        }
    }

    void updateWorkCount() {
        if (mOngoingWork != null) {
            Structure item = mAdapter.getItemAt(mPager.getCurrentItem());
            List<ListEntry> entriesForId = mOngoingWork.getEntriesForId(Integer.valueOf(item.getPropertyId()).intValue());
            mWorkCount.setText(String.valueOf(entriesForId.size()));
            if(entriesForId.isEmpty()) {
                mWorkCountContainer.setVisibility(View.INVISIBLE);
                mWorkCount.setVisibility(View.INVISIBLE);
            } else {
                mWorkCountContainer.setVisibility(View.VISIBLE);
                mWorkCount.setVisibility(View.VISIBLE);
            }
        } else {
            mWorkCountContainer.setVisibility(View.INVISIBLE);
            mWorkCount.setVisibility(View.INVISIBLE);
        }
    }


    private void loadOngoingWork(List<Structure> structures) {
        if (mWorkLoader != null) {
            mWorkLoader.cancel(true);
        }

        if (structures != null && structures.size() > 0) {


            // Only add structures with same property id once.
            Set<Integer> ids = new HashSet<Integer>();
            for (Structure s : structures) {
                ids.add(Integer.valueOf(s.getPropertyId()));
            }

            mWorkLoader = new WorkLoaderTask();
            mWorkLoader.execute(new ArrayList<Integer>(ids));
        }
    }

    private void setOngoingWork(WorkResponse work) {
        mOngoingWork = work;
        if (mListener != null) {
            mListener.setOngoingWork(mOngoingWork);
        }
        updateWorkCount();
    }

    private final class FacilityAdapter extends PagerAdapter {
        final List<Structure> mData = new ArrayList<>();

        public void setData(List<Structure> data) {
            mData.clear();
            mData.addAll(data);
            notifyDataSetChanged();
        }

        @Override
        public int getCount() {
            return mData.size();
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            View v = View.inflate(getActivity(), R.layout.cell_pager, null);
            TextView address = (TextView) v.findViewById(R.id.address);
            TextView name = (TextView) v.findViewById(R.id.name);
            Structure item = mData.get(position);

            address.setText(item.getName());
            name.setText(item.getDescription());
            container.addView(v);
            return v;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }

        Structure getItemAt(int position) {
            return mData.get(position);
        }

        int getIndexOfItem(Structure structures) {
            return mData.indexOf(structures);
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return object == view;

        }

        @Override
        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }

        public List<Structure> getData() {
            return mData;
        }
    }

    public final class WorkLoaderTask extends AsyncTask<List<Integer>, Void, WorkResponse> {

        private static final int CHUNK_SIZE = 50;

        @Override
        protected WorkResponse doInBackground(List<Integer>... p) {
            WorkResponse response = null;
            WorkQueryParams params = null;

            List<Integer> ids = p[0];

            // Chunk up the list and process it in chunks to avoid crash when size of ids > 60.
            int[] chunks = Utility.chunkIt(ids.size(), CHUNK_SIZE);

            for (int i = 0; i < chunks.length; i = i + 2) {

                params = new WorkQueryParams();
                List<Integer> newList = new ArrayList<>(ids.subList(chunks[i], chunks[i + 1]));
                params.structureId.ids = newList;

                StringWriter serializedXml = new StringWriter();

                try {
                    new Persister().write(params, serializedXml);
                } catch (Exception e) {
                    serializedXml = null;
                    e.printStackTrace();
                }

                if (serializedXml != null) {
                    final TokenFactory.Token token = TokenFactory.generateToken();

                    final String finalXml = serializedXml.toString().replaceAll("integer", "int"); // workaround because... gaaaah
                    Log.d(FabegeApplication.TAG, "XML Params: " + serializedXml.toString());

                    SessionKey sessionKey = Api.getInstance().login(token.getUserName(), token.getTimestamp(), token.getHash());

                    try {
                        WorkResponse currentWorkResponse = Api.getInstance().getOngoingWork("Fabege_GetDetail", sessionKey.mKey, finalXml);
                        if (response == null) {
                            response = currentWorkResponse;
                        } else {
                            response.workItems.addAll(currentWorkResponse.workItems);
                        }

                    } catch (Exception e) {
                        Log.d(FabegeApplication.TAG, "Failed getting ongoing works. " + e.toString());
                    }

                }
            }

            return response;
        }

        @Override
        protected void onPostExecute(WorkResponse workResponse) {
            if (!isCancelled()) {
                Log.d(FabegeApplication.TAG, "Work loaded: " + workResponse);
                setOngoingWork(workResponse);
            }
        }
    }
}
