package se.fabege.app.fragment

import android.animation.LayoutTransition
import android.app.Activity
import android.os.Bundle
import android.support.constraint.ConstraintLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.LinearSnapHelper
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import se.fabege.app.R
import se.fabege.app.activity.HomeActivity
import se.fabege.app.base.BaseFragment
import se.fabege.app.waw.data.Result
import se.fabege.app.waw.domain.model.Alert
import se.fabege.app.waw.domain.repository.DataRepository
import se.fabege.app.waw.misc.Injection
import se.fabege.app.waw.misc.Utils
import se.fabege.app.waw.ui.recyclerview.adapter.AdapterDelegateManager
import se.fabege.app.waw.ui.recyclerview.adapter.Item
import se.fabege.app.waw.ui.recyclerview.adapter.delegates.CollapsedAlertAdapterDelegate
import se.fabege.app.waw.ui.recyclerview.adapter.delegates.EmptyAlertAdapterDelegate
import se.fabege.app.waw.ui.recyclerview.adapter.delegates.ExpandedAlertAdapterDelegate
import se.fabege.app.waw.ui.recyclerview.adapter.items.CollapsedAlertItem
import se.fabege.app.waw.ui.recyclerview.adapter.items.EmptyAlertItem
import se.fabege.app.waw.ui.recyclerview.adapter.items.ExpandedAlertItem

class InterstitialFragment : BaseFragment(), DataRepository.AlertsCallback, CollapsedAlertAdapterDelegate.ClickListener, ExpandedAlertAdapterDelegate.ClickListener {

    private var mListener: InterstitialFragment.IInterstitialFragment? = null

    private val parentJob = Job()
    private val dispatcherProvider = Injection.provideCoroutinesDispatcherProvider()
    private val scope = CoroutineScope(dispatcherProvider.ui + parentJob)

    private val keyValueRepository = Injection.provideDefaultKeyValueRepository()
    private val getAlerts = Injection.provideGetAlertsUseCase()
    private val subscribeAlerts = Injection.provideSubscribeAlertsUserCase()
    private val unsubscribe = Injection.provideUnsubscribeUseCase()

    private lateinit var alertsRecyclerView: RecyclerView
    private lateinit var alertsAdapter: AlertsAdapter
    private lateinit var alertIcon: ImageView

    private lateinit var alerts: List<Alert>

    interface IInterstitialFragment {
        fun onHomeClicked()

        fun onWAWClicked()
    }


    override fun onAttach(activity: Activity?) {
        super.onAttach(activity)
        try {
            mListener = activity as InterstitialFragment.IInterstitialFragment?
        } catch (e: ClassCastException) {
            throw ClassCastException(activity!!.toString() + " must implement IInterstitialFragment")
        }

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        (activity as HomeActivity).hideToolbar()
        val rootView = inflater.inflate(R.layout.fragment_interstitial, container, false)

        val cl = rootView.findViewById<ConstraintLayout>(R.id.constraintLayout)
        cl.layoutTransition.enableTransitionType(LayoutTransition.CHANGING)


        val homeLayout = rootView.findViewById<View>(R.id.homeLayout)
        val wawLayout = rootView.findViewById<View>(R.id.wawLayout)

        alertIcon = rootView.findViewById<ImageView>(R.id.alertIcon)

        alertsRecyclerView = rootView.findViewById<RecyclerView>(R.id.alertsRecyclerView)
        alertsRecyclerView.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)

        alertsRecyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {

            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                    alertIcon.visibility = View.VISIBLE
                } else {
                    alertIcon.visibility = View.INVISIBLE
                }
            }
        })

        val snapHelper = LinearSnapHelper()
        snapHelper.attachToRecyclerView(alertsRecyclerView)

        val adapterDelegateManager = AdapterDelegateManager<List<Item>>()
        adapterDelegateManager.addDelegate(EmptyAlertAdapterDelegate(context!!))
        adapterDelegateManager.addDelegate(CollapsedAlertAdapterDelegate(context!!, this))
        adapterDelegateManager.addDelegate(ExpandedAlertAdapterDelegate(context!!, this))

        alertsAdapter = AlertsAdapter(adapterDelegateManager, listOf(EmptyAlertItem()))
        alertsRecyclerView.adapter = alertsAdapter

        homeLayout.setOnClickListener { mListener!!.onHomeClicked() }

        wawLayout.setOnClickListener { mListener!!.onWAWClicked() }


        getAndShowAlerts()

        return rootView
    }


    override fun onResume() {
        super.onResume()
        subscribeAlerts(this)
    }

    override fun onPause() {
        super.onPause()
        unsubscribe()
    }


    override fun onAlertsChanged() {
        getAndShowAlerts()
    }

    override fun onCollapsedAlertClicked(id: String) {
        keyValueRepository.putBoolean("showExpandedAlerts", true)
        showExpandedAlerts(getIndexOfId(id), alerts)
    }

    override fun onExpandedAlertClicked(id: String) {
        keyValueRepository.putBoolean("showExpandedAlerts", false)
        showCollapsedAlerts(getIndexOfId(id), alerts)
    }

    private fun getIndexOfId(id: String): Int {
        for ((index, alert) in alerts.withIndex()) {
            if (id == alert.id) {
                return index
            }
        }
        return -1
    }

    private fun getAndShowAlerts() {
        scope.launch(dispatcherProvider.network) {
            val result = getAlerts()
            withContext(dispatcherProvider.ui) {
                if (result is Result.Success) {
                    alerts = result.data.filter { alert -> alert.workspaceId.isEmpty() && Utils.getNowInMillis() >= alert.publishAt && Utils.getNowInMillis() < alert.unpublishAt }
                    if (keyValueRepository.getBoolean("showExpandedAlerts", true)) {
                        showExpandedAlerts(-1, alerts)
                    } else {
                        showCollapsedAlerts(-1, alerts)
                    }
                }
            }
        }
    }


    private fun showCollapsedAlerts(scrollPos: Int, alerts: List<Alert>) {
        alertIcon.visibility = if (alerts.isEmpty()) View.INVISIBLE else View.VISIBLE
        if (alerts.isEmpty()) {
            alertsAdapter.updateItems(listOf(EmptyAlertItem()))
        } else {
            alertsAdapter.updateItems(alerts.map { alert -> CollapsedAlertItem(alert.id, CollapsedAlertItem.ViewModel(alert)) })
            if (scrollPos != -1) {
                alertsRecyclerView.scrollToPosition(scrollPos)
            }
        }
    }

    private fun showExpandedAlerts(scrollPos: Int, alerts: List<Alert>) {
        alertIcon.visibility = if (alerts.isEmpty()) View.INVISIBLE else View.VISIBLE
        if (alerts.isEmpty()) {
            alertsAdapter.updateItems(listOf(EmptyAlertItem()))
        } else {
            alertsAdapter.updateItems(alerts.map { alert -> ExpandedAlertItem(alert.id, ExpandedAlertItem.ViewModel(alert)) })
            if (scrollPos != -1) {
                alertsRecyclerView.scrollToPosition(scrollPos)
            }
        }
    }


    companion object {
        fun newInstance(): InterstitialFragment {
            return InterstitialFragment()
        }
    }
}
