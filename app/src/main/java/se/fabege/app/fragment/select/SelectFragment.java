package se.fabege.app.fragment.select;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.Loader;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import se.fabege.app.R;
import se.fabege.app.activity.HomeActivity;
import se.fabege.app.base.FabegeApplication;
import se.fabege.app.base.ProvidedBaseFragment;
import se.fabege.app.fragment.select.adapter.StructureAdapterBase;
import se.fabege.app.storage.FabegeConcreteProvider;
import se.fabege.app.util.PermissionHelper;
import se.fabege.provider.database.table.StructureTable;
import se.fabege.provider.model.Structure;
import se.fabege.provider.provider.FabegeProvider;

/**
 * Created by Grindah on 2015-04-01.
 */
public class SelectFragment extends ProvidedBaseFragment implements PermissionHelper.Callback {


    public static final String SORT_ORDER = StructureTable.IS_FAVOURITE + " DESC, " + StructureTable.NAME + " ASC";
    private final String mDbTag = "ROLLBACK_CANDIDATES";
    private final String mCommitTag = "DO_NOT ROLLBACK";
    private StructureAdapterBase mAdapter;
    private Location mLastKnownPosition;
    private View mDoneButton;

    private ISelectFragment mListener;
    public static final String MODE_SEARCH = "SEARCH_MODE";
    public static final String SELECT_MODE = "SELECT_MODE";
    public static final int SELECT_MODE_FAVOURITES = 0;
    public static final int SELECT_MODE_SINGLE = 1;
    public static final String ROW_ID = "ROW_ID";

    private boolean mInSearchMode = false;
    private boolean consumeBackPress = false;
    private long mRowId = -1;
    private int mSelectMode;
    private Structure mSelectedStructure = null;
    private LocationListener mLocationListener;

    public static SelectFragment newInstance() {
        Bundle b = new Bundle();
        b.putBoolean("SEARCH_MODE", false);
        return new SelectFragment();
    }

    public static SelectFragment newInstance(boolean searchMode, long rowId) {
        Bundle b = new Bundle();
        b.putBoolean(MODE_SEARCH, searchMode);
        b.putInt(SELECT_MODE, SELECT_MODE_SINGLE);
        b.putLong(ROW_ID, rowId);
        SelectFragment selectFragment = new SelectFragment();
        selectFragment.setArguments(b);
        return selectFragment;
    }

    public static SelectFragment newInstance(boolean searchMode) {
        Bundle b = new Bundle();
        b.putBoolean(MODE_SEARCH, searchMode);
        b.putInt(SELECT_MODE, SELECT_MODE_FAVOURITES);
        b.putLong(ROW_ID, -1l);

        SelectFragment selectFragment = new SelectFragment();
        selectFragment.setArguments(b);
        return selectFragment;
    }

    public interface ISelectFragment {
        void onSelectionDone(boolean wasSearchMode);

        void onStructureSelected(Structure structure);
    }

    public SelectFragment() {
        // Need empty constructor.
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        startFavouriteLoader();
        startAllLoader();

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_fragment_select, menu);


        MenuItem search = menu.findItem(R.id.search_user);

        //Keep a global variable of this so you can set it within the next listener
        SearchView actionView = (SearchView) search.getActionView();
        actionView.setQueryHint("Sök adress");
        actionView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String query) {
                Log.d(FabegeApplication.TAG, "Query: " + query);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String text) {
                Log.d(FabegeApplication.TAG, "Query Text: " + text);
                if (mAdapter != null) {
                    mAdapter.setFilter(text);
                }
                return true;
            }
        });
        super.onCreateOptionsMenu(menu, inflater);
    }

    @SuppressLint("MissingPermission")
    private void getLastKnownLocation() {
        LocationManager lm = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        Location loc = lm.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

        if (loc == null) {
            // There is nothing we can do, must wait for location updates.
        } else {
            // Use this location as an early fix before we get location updates.
            mLastKnownPosition = loc;
            mAdapter.setLocation(mLastKnownPosition);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (ISelectFragment) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement IHomeFragment");
        }
    }

    private void commitTransaction(boolean commit) {
        String where = StructureTable.UPDATED + " = ?";
        String[] whereArgs = new String[]{mDbTag};
        ContentResolver contentResolver = getActivity().getContentResolver();
        List<Structure> structures = Structure.listFromCursor(contentResolver.query(FabegeConcreteProvider.STRUCTURE_CONTENT_URI, null, where, whereArgs, null));
        for (Structure s : structures) {
            s.setUpdated(mCommitTag);
            if (!commit) {
                s.setIsFavourite(s.getIsFavourite() == 1 ? 0 : 1); // flip the flag back
            }
            contentResolver.update(FabegeProvider.STRUCTURE_CONTENT_URI, s.getContentValues(), StructureTable.WHERE_ID_EQUALS, new String[]{s.getRowId().toString()});
        }

        if (commit) {
            Log.d(FabegeApplication.TAG, "Transaction committed " + structures.size() + " favourites");
        } else {
            Log.d(FabegeApplication.TAG, "Transaction reverted " + structures.size() + " favourites");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ((HomeActivity) getActivity()).showToolbar();
        Bundle arguments = getArguments();
        if (arguments != null) {
            mInSearchMode = arguments.getBoolean(MODE_SEARCH, false);
            mRowId = arguments.getLong(ROW_ID);
            mSelectMode = arguments.getInt(SELECT_MODE);
        }


        final View rootView = inflater.inflate(R.layout.fragment_select, container, false);
        rootView.findViewById(R.id.title).setVisibility(mInSearchMode ? View.GONE : View.VISIBLE);
        mDoneButton = rootView.findViewById(R.id.done);
        mDoneButton.setEnabled(mSelectMode == SELECT_MODE_FAVOURITES);
        mDoneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                commitTransaction(true);
                if (mSelectMode == SELECT_MODE_FAVOURITES) {
                    mListener.onSelectionDone(mInSearchMode);
                } else {
                    mListener.onStructureSelected(mSelectedStructure);
                }
            }
        });
        RecyclerView listView = (RecyclerView) rootView.findViewById(R.id.recyclerView);
        listView.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        listView.setLayoutManager(layoutManager);

        mAdapter = mSelectMode == SELECT_MODE_FAVOURITES ? new FavouritesAdapter(mDbTag) : new SelectAdapter(mRowId, new SelectAdapter.OnStructureClickListener() {

            @Override
            public void onStructureClick(Structure structure) {
                // Make sure we change
                mSelectedStructure = structure;
                mDoneButton.setEnabled(true);
                ((SelectAdapter) mAdapter).setRowId(mSelectedStructure.getRowId());
                mAdapter.notifyDataSetChanged();
            }
        });
        listView.setAdapter(mAdapter);

        mLocationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                Log.d(FabegeApplication.TAG, "New position: " + location);
                mLastKnownPosition = location;
                if (mAdapter != null) {
                    mAdapter.setLocation(mLastKnownPosition);
                }
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {
            }

            @Override
            public void onProviderEnabled(String provider) {
            }

            @Override
            public void onProviderDisabled(String provider) {
            }
        };
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().setTitle(getString(R.string.fragment_select_title));
        if (PermissionHelper.isLocationPermissionGranted(getContext())) {
            requestLocationUpdates();
        } else {
            PermissionHelper.requestLocationPermission(this);
        }
    }

    @SuppressLint("MissingPermission")
    private void requestLocationUpdates() {
        getLastKnownLocation();
        LocationManager lm = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, mLocationListener);
    }

    @Override
    public void onResult(@NotNull String[] permissions, @NotNull PermissionHelper.PermissionResult[] results) {
        // No validation.
        switch (results[0]) {
            case GRANTED:
                requestLocationUpdates(); /* continue with task */
                break;
            case DENIED:
            case CANCELED:
                break;
            default:
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        PermissionHelper.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }


    @Override
    public boolean consumeBackPress() {
        return consumeBackPress;
    }

    @Override
    public void onPause() {
        super.onPause();
        if (isRemoving()) {
            commitTransaction(false);
        }
        try {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);

            if (imm.isAcceptingText()) { // verify if the soft keyboard is open
                imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
            }
        } catch (NullPointerException e) {
            Log.e(FabegeApplication.TAG, "Caught exception when trying to hide keyboard");
        }


        LocationManager locationManager = (LocationManager)
                getActivity().getSystemService(Context.LOCATION_SERVICE);
        locationManager.removeUpdates(mLocationListener);

    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        switch (loader.getId()) {
            case LOADER_ALL:
                data.moveToFirst();
                mAdapter.setData(Structure.listFromCursor(data));
                break;
            case LOADER_FAVOURITES:
                boolean hasFavourites = data.getCount() > 0;
                if (mSelectMode == SELECT_MODE_FAVOURITES) {
                    mDoneButton.setEnabled(hasFavourites);
                }
                if (mInSearchMode) {
                    ((HomeActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(hasFavourites);
                    consumeBackPress = !hasFavourites;
                }
                break;
        }
    }
}

