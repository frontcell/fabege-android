package se.fabege.app.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import se.fabege.app.R;
import se.fabege.app.activity.HomeActivity;
import se.fabege.app.base.BaseFragment;
import se.fabege.provider.model.Structure;

/**
 * Created by Grindah on 2015-04-19.
 */
public class FaqFragment extends BaseFragment {

    private IFaqFragment mListener;
    private FaqAdapter mAdapter;

    public enum FaqItem {
        ELEVATORS(R.string.faq_elevators, "#elevators"),
        CLEANING(R.string.faq_cleaning, "#cleaning"),
        ENVIRONMENT(R.string.faq_environment, "#environmentandenergy"),
        SECURITY(R.string.faq_security, "#security"),
        ADVERTISING(R.string.faq_signing, "#signs"),
        PARKING(R.string.faq_parking, "#parking");
        private final int mItemName;
        private final String mUrlSuffix;


        private FaqItem(@StringRes int itemName, String urlSuffix) {
            mItemName = itemName;
            mUrlSuffix = urlSuffix;
        }

        public int getItemName() {
            return mItemName;
        }

        public String getUrlSuffix() {
            return mUrlSuffix;
        }
    }

    public interface IFaqFragment {
        public void onShowFaqItem(FaqItem faqItem, String url);
    }

    public static FaqFragment newInstance() {
        return new FaqFragment();
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (IFaqFragment) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement IFaqFragment");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        ((HomeActivity) getActivity()).showToolbar();
        View rootView = inflater.inflate(R.layout.fragment_faq, container, false);
        ListView listView = (ListView) rootView.findViewById(R.id.listView);
        mAdapter = new FaqAdapter();
        listView.setAdapter(mAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Structure item = getActiveStructure();
                FaqItem faqItem = (FaqItem) mAdapter.getItem(position);
                if (item != null) {
                    String url = "http://fabege.se/For-vara-hyresgaster/Fastigheter/" +
                            item.getDescription().replace(" ", "-")
                                    .replace("ö", "o")
                                    .replace("å", "a")
                                    .replace("ä", "a")
                            + "/"
                            + faqItem.getUrlSuffix();
                    mListener.onShowFaqItem(faqItem, url);

                }
            }
        });
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().setTitle(R.string.button_faq);
    }

    private final class FaqAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return FaqItem.values().length;
        }

        @Override
        public Object getItem(int position) {
            return FaqItem.values()[position];
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View v = convertView;
            if (v == null) {
                v = View.inflate(parent.getContext(), R.layout.cell_faq, null);
            }

            final TextView title = (TextView) v.findViewById(R.id.title);
            title.setText(FaqItem.values()[position].getItemName());


            return v;
        }
    }


}
