package se.fabege.app.fragment.select;

import android.view.View;
import android.widget.TextView;

import se.fabege.app.R;
import se.fabege.app.fragment.select.adapter.AbstractStructureViewHolder;
import se.fabege.provider.model.Structure;

/**
 * Created by Grindah on 2015-04-16.
 */
public class SelectableStructureViewHolder extends AbstractStructureViewHolder {
    public final TextView mTitle;
    public final TextView mDistance;
    private long mSelectedRowId;
    private final SelectAdapter.OnStructureClickListener mClickListener;
    private Structure mData;

    public SelectableStructureViewHolder(View v, long selectedRowId, SelectAdapter.OnStructureClickListener clickListener) {
        super(v);
        mClickListener = clickListener;
        mSelectedRowId = selectedRowId;
        mTitle = (TextView) v.findViewById(R.id.title);
        mDistance = (TextView) v.findViewById(R.id.distance);
        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mData != null) {
                    mClickListener.onStructureClick(mData);
                }
            }
        });
    }

    void setSelectedRow(long rowId) {
        mSelectedRowId = rowId;
    }

    @Override
    public void bindData(Structure data) {
        mData = data;
        mTitle.setText(data.getName());
        if (mSelectedRowId == mData.getRowId()) {
            mTitle.setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0, R.drawable.ic_checkmark, 0);
        } else {
            mTitle.setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0, 0, 0);
        }
        mDistance.setText(distanceToString(data.getDistance()));
    }
}
