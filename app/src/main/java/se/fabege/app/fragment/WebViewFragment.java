package se.fabege.app.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import se.fabege.app.R;
import se.fabege.app.activity.HomeActivity;
import se.fabege.app.base.BaseFragment;
import se.fabege.app.base.FabegeApplication;

/**
 * Created by Grindah on 2015-04-13.
 */
public class WebViewFragment extends BaseFragment {


    private static final String PARAM_URL = "URL";
    private WebView mWebView;
    private String mUrlToLoad;
    private View mProgress;

    public static WebViewFragment newInstance(String url) {
        Bundle b = new Bundle();
        b.putString(PARAM_URL, url);

        WebViewFragment webFragment = new WebViewFragment();
        webFragment.setArguments(b);
        return webFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        ((HomeActivity) getActivity()).showToolbar();
        View rootView = inflater.inflate(R.layout.fragment_webview, container, false);
        mUrlToLoad = getArguments().getString(PARAM_URL);
        mWebView = (WebView) rootView.findViewById(R.id.webView);

        mWebView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                // do your handling codes here, which url is the requested url
                // probably you need to open that url rather than redirect:
                mWebView.loadUrl(url);
                return false; // then it is not handled by default action
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                mProgress.setVisibility(View.GONE);
            }
        });
        mProgress = rootView.findViewById(R.id.progressBar);

        WebSettings webSettings = mWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(FabegeApplication.TAG, "Loading url " + mUrlToLoad);
        mWebView.loadUrl(mUrlToLoad);
    }
}
