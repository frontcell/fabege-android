package se.fabege.app.fragment.select.adapter;

import android.location.Location;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import se.fabege.app.R;
import se.fabege.app.base.FabegeApplication;
import se.fabege.provider.model.Structure;

/**
 * Created by Grindah on 2015-04-16.
 */
public abstract class StructureAdapterBase extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    protected static final int HEADER = 0;
    protected static final int STRUCTURE = 1;
    protected static final int BUTTONS = 2;
    private String mFilter = "";
    private List<MetaData> mData = new ArrayList<MetaData>();
    private List<Structure> mRawData;
    private ButtonViewHolder.SortOption mSortOption = ButtonViewHolder.SortOption.ALFABETIC;
    private Location location;


    protected final class MetaData {
        int type = 0;
        Structure data = null;

        public MetaData(int type) {
            this.type = type;
        }

        public MetaData(int type, Structure data) {
            this.type = type;
            this.data = data;
        }
    }

    public StructureAdapterBase() {
    }

    public void setFilter(String filter) {
        if (filter == null) {
            mFilter = "";
        } else {
            mFilter = filter;
        }
        setData(mRawData);
    }

    public abstract AbstractStructureViewHolder getStructureViewHolder(ViewGroup parent);


    /**
     * This method has room for optimizations... The filtering could for instance be done in the db query
     *
     * @param data
     */
    public void setData(List<Structure> data) {

        // Update the distances
        for (Structure s : data) {
            s.updateDistance(location);
        }

        // Store the raw data and clear the working set
        mRawData = data;
        mData.clear();

        if (data != null && data.size() > 0) {
            // sort the incoming data:
            Collections.sort(data, new Comparator<Structure>() {
                @Override
                public int compare(Structure lhs, Structure rhs) {
                    int c;
                    c = Integer.valueOf(rhs.getIsFavourite()).compareTo(Integer.valueOf(lhs.getIsFavourite()));
                    if (mSortOption == ButtonViewHolder.SortOption.ALFABETIC) {
                        if (c == 0) {
                            c = lhs.getName().compareTo(rhs.getName());
                        }
                    } else {
                        if (c == 0) {
                            c = Double.compare(lhs.getDistance(), rhs.getDistance());
                        }
                    }
                    return c;
                }
            });

            // Add the data that passes the filter to mData:
            for (Structure s : data) {
                if (s.getName().toLowerCase().contains(mFilter)) {
                    mData.add(new MetaData(STRUCTURE, s));
                }
            }

            // Add favourite header if applicable
            mData.add(0, new MetaData(HEADER));


            //Find the spot for the buttons:
            for (int i = 0; i < mData.size(); i++) {
                MetaData metaData = mData.get(i);
                if (metaData.type == STRUCTURE) {
                    Structure s = metaData.data;
                    if (s.getIsFavourite() == 0) {
                        // add the buttons
                        MetaData buttons = new MetaData(BUTTONS);
                        mData.add(i, buttons);
                        break;
                    }
                }
            }
        }


        notifyDataSetChanged();

    }

    public void setLocation(Location location) {
        // Sanity check.
        if (location == null) {
            return;
        }

        Log.d(FabegeApplication.TAG, "Got position: " + location);
        this.location = location;
        for (MetaData s : mData) {
            if (s.type == STRUCTURE) {
                s.data.updateDistance(location);
            }
        }
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case HEADER:
                return new HeaderViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_header, parent, false));
            case STRUCTURE:
                return getStructureViewHolder(parent);
            case BUTTONS:
                return new ButtonViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_buttons, parent, false), new ButtonViewHolder.SortCallback() {
                    @Override
                    public void onSortOptionChanged(ButtonViewHolder.SortOption option) {
                        mSortOption = option;
                        setData(mRawData);
                    }
                });

        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        int itemViewType = getItemViewType(position);
        switch (itemViewType) {
            case HEADER:
                ((HeaderViewHolder) holder).mTitle.setText("Favoriter");
                break;
            case STRUCTURE:
                Structure data = mData.get(position).data;
                AbstractStructureViewHolder structureHolder = (AbstractStructureViewHolder) holder;
                if (!onBindStructureViewHolder(structureHolder, position, data)) {
                    structureHolder.bindData(data);
                }
                break;

            case BUTTONS:
                ((ButtonViewHolder) holder).distanceButton.setEnabled(location == null ? false : true);
                break;
        }
    }

    protected boolean onBindStructureViewHolder(AbstractStructureViewHolder holder, int position, Structure structure) {
        return false;
    }

    @Override
    public int getItemViewType(int position) {
        return mData.get(position).type;
    }


    @Override
    public int getItemCount() {
        return mData.size();
    }
}


