package se.fabege.app.fragment.select.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import se.fabege.provider.model.Structure;

/**
 * Created by Grindah on 2015-04-16.
 */
public abstract class AbstractStructureViewHolder extends RecyclerView.ViewHolder {
    private Structure mData;

    public AbstractStructureViewHolder(View itemView) {
        super(itemView);
    }


    public abstract void bindData(Structure data);

    public String distanceToString(double distance) {
        String result = "";
        if (distance == -1) {
            result = "";
        } else if (distance > 1000) {
            result = String.format("%.0f", distance / 1000) + "km";
        } else {
            result = String.format("%.0f", distance) + "m";
        }
        return result;
    }
}
