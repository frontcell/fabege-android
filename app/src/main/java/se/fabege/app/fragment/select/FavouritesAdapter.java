package se.fabege.app.fragment.select;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import se.fabege.app.R;
import se.fabege.app.fragment.select.adapter.AbstractStructureViewHolder;
import se.fabege.app.fragment.select.adapter.StructureAdapterBase;

/**
 * Created by Grindah on 2015-04-12.
 */
public class FavouritesAdapter extends StructureAdapterBase {

    private final String mDbTag;

    public FavouritesAdapter(String dbTag) {
        super();
        mDbTag = dbTag;
    }

    @Override
    public AbstractStructureViewHolder getStructureViewHolder(ViewGroup parent) {
        return new FavoritableStructureViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_structure_fav, parent, false), mDbTag);
    }


}

