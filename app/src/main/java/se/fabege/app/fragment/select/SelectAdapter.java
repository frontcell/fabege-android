package se.fabege.app.fragment.select;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import se.fabege.app.R;
import se.fabege.app.fragment.select.adapter.AbstractStructureViewHolder;
import se.fabege.app.fragment.select.adapter.StructureAdapterBase;
import se.fabege.provider.model.Structure;

/**
 * Created by Grindah on 2015-04-16.
 */
public class SelectAdapter extends StructureAdapterBase {
    private long mRowId;
    private final OnStructureClickListener mClickListener;

    public interface OnStructureClickListener {
        public void onStructureClick(Structure structure);
    }

    public SelectAdapter(long rowId, OnStructureClickListener clickListener) {
        super();
        mRowId = rowId;
        mClickListener = clickListener;
    }

    public void setRowId(long rowId) {
        mRowId = rowId;
    }

    @Override
    public AbstractStructureViewHolder getStructureViewHolder(ViewGroup parent) {
        return new SelectableStructureViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_structure_sel, parent, false), mRowId, mClickListener);

    }

    @Override
    protected boolean onBindStructureViewHolder(AbstractStructureViewHolder holder, int position, Structure structure) {
        ((SelectableStructureViewHolder) holder).setSelectedRow(mRowId); // downcast - very ugly
        holder.bindData(structure);
        return true;
    }
}
