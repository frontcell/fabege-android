package se.fabege.app.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import se.fabege.api.dto.work.ListEntry;
import se.fabege.api.dto.work.WorkResponse;
import se.fabege.app.R;
import se.fabege.app.activity.HomeActivity;
import se.fabege.app.base.BaseFragment;
import se.fabege.app.util.Utility;
import se.fabege.provider.model.Structure;

/**
 * Created by Grindah on 2015-03-29.
 */
public class WorkFragment extends BaseFragment {


    private IWorkFragment mListener;

    public static WorkFragment newInstance() {
        return new WorkFragment();
    }

    public interface IWorkFragment {
        WorkResponse getOngoingWork();

        Structure getActiveStructure();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (IWorkFragment) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement IWorkFragment");
        }
    }

    public WorkFragment() {
        // Need empty constructor.
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ((HomeActivity) getActivity()).showToolbar();
        final View rootView = inflater.inflate(R.layout.fragment_work, container, false);
        RecyclerView listView = (RecyclerView) rootView.findViewById(R.id.recyclerView);
        listView.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        listView.setLayoutManager(layoutManager);
        WorkAdapter workAdapter = new WorkAdapter();
        workAdapter.setData(getWork());
        listView.setAdapter(workAdapter);

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().setTitle(getString(R.string.fragment_work_title));
    }

    public Collection<ListEntry> getWork() {
        ArrayList<ListEntry> workList = new ArrayList<>();
        Structure activeStructure = mListener.getActiveStructure();
        WorkResponse ongoingWork = mListener.getOngoingWork();

        if (activeStructure != null && ongoingWork != null) {
            workList.addAll(ongoingWork.getEntriesForId(Integer.valueOf(activeStructure.getPropertyId()).intValue()));
        }

        // Reverse order of items.
        Collections.reverse(workList);
        return workList;
    }


    public class WorkAdapter extends RecyclerView.Adapter<WorkHolder> {

        private final ArrayList<ListEntry> mWorkList;

        public WorkAdapter() {
            mWorkList = new ArrayList<>();
        }

        public void setData(Collection<ListEntry> data) {
            mWorkList.clear();
            mWorkList.addAll(data);
            notifyDataSetChanged();
        }


        @Override
        public WorkHolder onCreateViewHolder(ViewGroup viewGroup, int position) {
            View itemView = LayoutInflater.
                    from(viewGroup.getContext()).
                    inflate(R.layout.cell_ongoing_work, viewGroup, false);

            return new WorkHolder(itemView);
        }

        @Override
        public void onBindViewHolder(WorkHolder workHolder, int position) {
            ListEntry item = mWorkList.get(position);
            workHolder.date.setText(item.date != null ? Utility.formatDateString(item.date) : "");
            workHolder.title.setText(item.title != null ? item.title : "");
            workHolder.description.setText(Html.fromHtml(item.description != null ? item.description : ""));
        }


        @Override
        public int getItemCount() {
            return mWorkList.size();
        }
    }


    static class WorkHolder extends RecyclerView.ViewHolder {
        protected TextView title;
        protected TextView description;
        protected TextView date;


        public WorkHolder(View v) {
            super(v);
            title = (TextView) v.findViewById(R.id.title);
            description = (TextView) v.findViewById(R.id.description);
            date = (TextView) v.findViewById(R.id.date);
        }
    }

}