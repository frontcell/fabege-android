package se.fabege.app.fragment.select.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RadioGroup;

import se.fabege.app.R;

/**
 * Created by Grindah on 2015-04-16.
 */
class ButtonViewHolder extends RecyclerView.ViewHolder {

    final View distanceButton;

    enum SortOption {
        DISTANCE,
        ALFABETIC;
    }

    interface SortCallback {
        void onSortOptionChanged(SortOption option);
    }

    public ButtonViewHolder(View v, final SortCallback cb) {
        super(v);
        final RadioGroup buttons = (RadioGroup) v.findViewById(R.id.sort_group);
        distanceButton = v.findViewById(R.id.sort_distance);
        buttons.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.sort_distance:
                        if (cb != null) {
                            cb.onSortOptionChanged(SortOption.DISTANCE);
                        }
                        break;
                    case R.id.sort_alfabetical:
                        if (cb != null) {
                            cb.onSortOptionChanged(SortOption.ALFABETIC);
                        }
                        break;
                }
            }
        });
    }

}
