package se.fabege.app.fragment;

import android.app.Activity;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import se.fabege.app.R;
import se.fabege.app.base.ProvidedBaseFragment;

/**
 * Created by Grindah on 2015-03-29.
 */
public class SplashFragment extends ProvidedBaseFragment {

    private ISplashFragment mListener;
    private Handler mHandler = new Handler();

    private Runnable mDummyLoader = new Runnable() {
        @Override
        public void run() {
            mListener.onLoadComplete();
        }
    };

    public static SplashFragment newInstance() {
        return new SplashFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        startAllLoader();
        launchDatabaseUpdate();

    }

    public SplashFragment() {
        // Need empty constructor.
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        switch (loader.getId()) {
            case LOADER_ALL:
                if (data.getCount() > 0) {
                    mHandler.postDelayed(mDummyLoader, 1000);
                }
                break;
        }
    }

    public interface ISplashFragment {
        void onLoadComplete();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (ISplashFragment) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement ISplashFragment");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_splash, container, false);
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mHandler.removeCallbacks(mDummyLoader);
    }
}