package se.fabege.app.fragment

import android.support.v7.util.DiffUtil
import se.fabege.app.waw.ui.recyclerview.adapter.AdapterDelegateManager
import se.fabege.app.waw.ui.recyclerview.adapter.Item
import se.fabege.app.waw.ui.recyclerview.adapter.ListAdapter
import se.fabege.app.waw.ui.recyclerview.adapter.items.CollapsedAlertItem

class AlertsAdapter(manager: AdapterDelegateManager<List<Item>>, items: List<Item>) : ListAdapter<List<Item>>(items) {
    init {
        this.manager = manager
    }

    fun updateItems(items: List<Item>) {
        this.items = items
        notifyDataSetChanged()
    }

    class DiffCallback(private val newItems: List<Item>, private val oldItems: List<Item>) : DiffUtil.Callback() {

        override fun getOldListSize(): Int {
            return oldItems.size
        }

        override fun getNewListSize(): Int {
            return newItems.size
        }

        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            if (oldItems[oldItemPosition]::class != newItems[newItemPosition]::class) {
                return false
            }

            val oldId = (oldItems[oldItemPosition] as CollapsedAlertItem).id
            val newId = (newItems[newItemPosition] as CollapsedAlertItem).id
            return oldId == newId
        }

        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            return false
        }
    }
}