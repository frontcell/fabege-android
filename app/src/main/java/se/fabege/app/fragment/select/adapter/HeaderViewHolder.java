package se.fabege.app.fragment.select.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import se.fabege.app.R;

/**
 * Created by Grindah on 2015-04-16.
 */
class HeaderViewHolder extends RecyclerView.ViewHolder {

    public final TextView mTitle;

    public HeaderViewHolder(View v) {
        super(v);
        mTitle = (TextView) v.findViewById(R.id.title);
    }

}
