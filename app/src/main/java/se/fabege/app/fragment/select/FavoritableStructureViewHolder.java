package se.fabege.app.fragment.select;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import se.fabege.app.R;
import se.fabege.app.analytics.AnalyticsImpl;
import se.fabege.app.analytics.AnalyticsUtils;
import se.fabege.app.fragment.select.adapter.AbstractStructureViewHolder;
import se.fabege.provider.database.table.StructureTable;
import se.fabege.provider.model.Structure;
import se.fabege.provider.provider.FabegeProvider;

/**
 * Created by Grindah on 2015-04-16.
 */
class FavoritableStructureViewHolder extends AbstractStructureViewHolder {
    public final TextView mTitle;
    public final TextView mDistance;
    public final ImageView mCheckmark;
    private Structure mData;

    public FavoritableStructureViewHolder(View v, final String dbTag) {
        super(v);
        mTitle = (TextView) v.findViewById(R.id.title);
        mDistance = (TextView) v.findViewById(R.id.distance);
        mCheckmark = (ImageView) v.findViewById(R.id.checkbox);
        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mData != null) {
                    mData.setIsFavourite(mData.getIsFavourite() == 1 ? 0 : 1);
                    if (mData.getIsFavouriteAsBool()) {
                        AnalyticsImpl.getInstance().trackEvent(AnalyticsUtils.makeAddPropertyEvent(mData));
                    }
                    mData.setUpdated(dbTag);
                    v.getContext().getContentResolver().update(FabegeProvider.STRUCTURE_CONTENT_URI, mData.getContentValues(), StructureTable.WHERE_ID_EQUALS, new String[]{mData.getRowId().toString()});
                }
            }
        });

    }

    @Override
    public void bindData(Structure data) {
        mData = data;

        mTitle.setText(data.getName());
        mDistance.setText(distanceToString(data.getDistance()));
        mCheckmark.setImageResource(mData.getIsFavourite() == 1 ? R.drawable.ic_favorite_selected : R.drawable.ic_favorite_default);
    }
}
