package se.fabege.app.fragment.register;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.simpleframework.xml.core.Persister;

import java.io.StringWriter;

import se.fabege.api.dto.Task;
import se.fabege.app.R;
import se.fabege.app.activity.HomeActivity;
import se.fabege.app.analytics.AnalyticsImpl;
import se.fabege.app.analytics.AnalyticsUtils;
import se.fabege.app.base.FabegeApplication;
import se.fabege.app.loader.SubmitTaskLoader;
import se.fabege.provider.model.Structure;

/**
 * Created by Grindah on 2015-03-29.
 */
public class RegisterFragment extends AbstractRegisterFragment implements android.support.v4.app.LoaderManager.LoaderCallbacks<Integer> {
    public static final int SUBMIT_LOADER_ID = 1234;
    private IRegisterFragment mListener;
    private EditText mAddressField;
    private EditText mCompanyField;
    private EditText mPhoneNumberField;
    private EditText mEmailField;
    private EditText mContactField;
    private EditText mDescriptionField;
    private Button mAttachButton;
    private Button mChangeFacilityButton;
    private ImageView mImageView;
    private static final String ARG_ROW_ID = "ROW_ID";
    private Structure mActiveStructure;
    private Button mSubmitButton;
    private Bitmap mBitmap;
    private ProgressDialog mProgress;
    private SubmitTaskLoader.SubmitData mSubmitData;
    private final Handler mHandler = new Handler();

    public static RegisterFragment newInstance() {

        RegisterFragment f = new RegisterFragment();
        return f;
    }

    public RegisterFragment() {
        // Need empty constructor.
    }


    public interface IRegisterFragment {
        void onSubmitComplete();

        void onChangeStructure();

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (IRegisterFragment) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement IRegisterFragment");
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ((HomeActivity) getActivity()).showToolbar();
        final View rootView = inflater.inflate(R.layout.fragment_register, container, false);
        mAddressField = (EditText) rootView.findViewById(R.id.address_field);
        mCompanyField = (EditText) rootView.findViewById(R.id.company_field);
        mContactField = (EditText) rootView.findViewById(R.id.contact_field);
        mEmailField = (EditText) rootView.findViewById(R.id.email_field);
        mPhoneNumberField = (EditText) rootView.findViewById(R.id.phone_field);
        mDescriptionField = (EditText) rootView.findViewById(R.id.description_field);
        mSubmitButton = (Button) rootView.findViewById(R.id.submit_button);
        mSubmitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onSubmit();
            }
        });
        mChangeFacilityButton = (Button) rootView.findViewById(R.id.select_button);
        mChangeFacilityButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onChangeStructure();
            }
        });
        mAttachButton = (Button) rootView.findViewById(R.id.select_photo);
        mImageView = (ImageView) rootView.findViewById(R.id.image);
        mAttachButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CharSequence colors[] = new CharSequence[]{"Kamera", "Galleri"};

                AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(getActivity(), R.style.Theme_AppCompat_Light_Dialog));

                builder.setTitle("Bifoga bild från");

                builder.setItems(colors, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            default: // fallthrough
                            case 0:
                                dispatchTakePictureIntent();
                                break;
                            case 1:
                                dispatchSelectPictureIntent();
                                break;

                        }
                    }
                });
                builder.show();
            }
        });

        TextView emergencyNumberTextView = (TextView) rootView.findViewById(R.id.emergency_number_text);
        emergencyNumberTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                initiateEmergencyPhonenumberCall();
            }
        });

        return rootView;
    }


    @Override
    public void onResume() {
        super.onResume();
        getActivity().setTitle(getString(R.string.fragment_register_title));
        restoreEnteredInfo();
        mActiveStructure = getActiveStructure();
        mAddressField.setText(mActiveStructure.getName());
    }

    @Override
    public void onPause() {
        super.onPause();
        storeEnteredInfo();
    }

    @Override
    protected void onBitmapLoaded(Bitmap bitmap) {
        mBitmap = bitmap;
        mImageView.setImageBitmap(bitmap);
    }

    public void onSubmit() {
        if (mProgress != null) {
            mProgress.dismiss();
            mProgress = null;
        }
        if (!TextUtils.isEmpty(mCompanyField.getText().toString())
                && !TextUtils.isEmpty(mContactField.getText().toString())
                && !TextUtils.isEmpty(mEmailField.getText().toString())
                && !TextUtils.isEmpty(mPhoneNumberField.getText().toString())
                && !TextUtils.isEmpty(mDescriptionField.getText().toString())) {
            mProgress = ProgressDialog.show(new ContextThemeWrapper(getActivity(), R.style.Theme_AppCompat_Light_Dialog), getString(R.string.task_progress_title), getString(R.string.task_progress_message), true);
            storeEnteredInfo();
            mSubmitData = new SubmitTaskLoader.SubmitData(createTaskXml(), mBitmap);
            getLoaderManager().initLoader(SUBMIT_LOADER_ID, null, this);
        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(getActivity(), R.style.Theme_AppCompat_Light_Dialog));
            builder.setMessage("Fyll i alla fält");
            builder.setPositiveButton("Ok", null);
            builder.show();
        }
    }


    @Override
    public android.support.v4.content.Loader<Integer> onCreateLoader(int id, Bundle args) {
        return new SubmitTaskLoader(getActivity(), mSubmitData);
    }

    @Override
    public void onLoadFinished(android.support.v4.content.Loader<Integer> loader, Integer data) {
        if (mProgress != null) {
            mProgress.dismiss();
            mProgress = null;
        }
        if (loader.getId() == SUBMIT_LOADER_ID) {
            switch (data.intValue()) {
                case 0:
                    Toast.makeText(getActivity(), R.string.task_toast_success, Toast.LENGTH_SHORT).show();
                    AnalyticsImpl.getInstance().trackEvent(AnalyticsUtils.makeServiceRequestSuccessEvent(mActiveStructure, mBitmap != null));
                    mHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            mListener.onSubmitComplete();
                        }
                    });
                    break;
                default:
                    AnalyticsImpl.getInstance().trackEvent(AnalyticsUtils.makeServiceRequestFailEvent(mActiveStructure, mBitmap != null));
                    Toast.makeText(getActivity(), R.string.task_toast_failure, Toast.LENGTH_SHORT).show();
                    break;
            }
        }
        getLoaderManager().destroyLoader(loader.getId());
    }

    @Override
    public void onLoaderReset(android.support.v4.content.Loader<Integer> loader) {

    }


    String createTaskXml() {
        String retVal = null;
        Task task = new Task();
        task.placement = mCompanyField.getText().toString();
        task.description = mDescriptionField.getText().toString();
        task.profession = new Task.Profession();
        task.profession.id = "157";
        task.reportedBy = new Task.ReportedBy();
        task.reportedBy.email = mEmailField.getText().toString();
        task.reportedBy.name = mContactField.getText().toString();
        task.reportedBy.phone = mPhoneNumberField.getText().toString();
        task.structure = new Task.House();
        task.structure.destinationId = getActiveStructure().getSource();

        StringWriter serializedXml = new StringWriter();


        try {
            new Persister().write(task, serializedXml);
            Log.d(FabegeApplication.TAG, "Task xml: " + serializedXml.toString());
            retVal = serializedXml.toString();
        } catch (Exception e) {
            serializedXml = null;
            e.printStackTrace();
        }
        return retVal;
    }

    void storeEnteredInfo() {
        SharedPreferences pref = getActivity().getApplicationContext().getSharedPreferences("RegisterPrefs", 0);
        SharedPreferences.Editor editor = pref.edit();

        StringBuilder builder = new StringBuilder();
        builder.append(mCompanyField.getText().toString().isEmpty() ? " " : mCompanyField.getText().toString());
        builder.append("::");
        builder.append(mContactField.getText().toString().isEmpty() ? " " : mContactField.getText().toString());
        builder.append("::");
        builder.append(mEmailField.getText().toString().isEmpty() ? " " : mEmailField.getText().toString());
        builder.append("::");
        builder.append(mPhoneNumberField.getText().toString().isEmpty() ? " " : mPhoneNumberField.getText().toString());
        editor.putString("FieldEntries", builder.toString());
        editor.commit();
    }

    void restoreEnteredInfo() {
        SharedPreferences pref = getActivity().getApplicationContext().getSharedPreferences("RegisterPrefs", 0);
        String storedInfo = pref.getString("FieldEntries", " :: :: :: ");
        String[] values = storedInfo.split("::");
        Log.d(FabegeApplication.TAG, "Values " + values.length + " " + storedInfo);
        mCompanyField.setText(values[0].trim());
        mContactField.setText(values[1].trim());
        mEmailField.setText(values[2].trim());
        mPhoneNumberField.setText(values[3].trim());
    }

    void initiateEmergencyPhonenumberCall() {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + "020990990"));
        if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
            startActivity(intent);
        }
    }
}
