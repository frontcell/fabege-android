package se.fabege.app.loader;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.content.AsyncTaskLoader;
import android.text.TextUtils;
import android.util.Log;

import org.simpleframework.xml.core.Persister;

import java.io.ByteArrayOutputStream;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;

import retrofit.RetrofitError;
import retrofit.client.Header;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;
import retrofit.mime.TypedInput;
import se.fabege.api.Api;
import se.fabege.api.TokenFactory;
import se.fabege.api.dto.DocumentFile;
import se.fabege.api.dto.session.SessionKey;
import se.fabege.app.base.FabegeApplication;

/**
 * Created by Grindah on 2015-04-26.
 */
public class SubmitTaskLoader extends AsyncTaskLoader<Integer> {
    private final SubmitData mData;
    private Integer mResult = null;

    public static final class SubmitData {
        Bitmap submitImage = null;
        String taskXml = null;

        public SubmitData(String xml, Bitmap bmp) {
            this.taskXml = xml;
            this.submitImage = bmp;
        }
    }

    public SubmitTaskLoader(Context context, SubmitData data) {
        super(context);
        mData = data;
    }

    @Override
    protected void onStartLoading() {
        if (mResult != null) {
            deliverResult(mResult);
        } else {
            forceLoad();
        }
    }

    @Override
    public Integer loadInBackground() {
        int result = -1;

        // Generate token and login:

        final TokenFactory.Token token = TokenFactory.generateToken();
        final SessionKey sessionKey = Api.getInstance().login(token.getUserName(), token.getTimestamp(), token.getHash());

        if (sessionKey != null) {
            // login successful

            try {
                //Upload task xml:
                final TypedInput inXml = new TypedByteArray("", mData.taskXml.getBytes("UTF-8"));
//                final TypedInput inXml = new TypedByteArray("application/xml", mData.taskXml.getBytes("UTF-8"));
                Response response = Api.getInstance().createTask("DeDUDBWS_CreateTask", sessionKey.mKey, inXml);

                // read the created task id from the response header:
                String taskId = null;
                for (Header header : response.getHeaders()) {
                    if (header != null && header.getName() != null && header.getName().equals("ID")) {
                        taskId = header.getValue();
                    }
                }
                Log.d(FabegeApplication.TAG, "Create task resulted in task with ID: " + taskId);

                String imageXml = createImageXml(taskId);

                result = 0;
                //  Upload the bitmap:
                if (mData.submitImage != null && !TextUtils.isEmpty(imageXml) && !TextUtils.isEmpty(taskId)) {
                    result = -1;

                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    mData.submitImage.compress(Bitmap.CompressFormat.JPEG, 60, stream);
                    final byte[] byteArray = stream.toByteArray();
                    TypedByteArray data = new TypedByteArray("image/jpeg", byteArray);
                    Response response2 = Api.getInstance().uploadFile(sessionKey.mKey, imageXml, data);
                    if (response2.getStatus() == 201) {
                        Log.d(FabegeApplication.TAG, "Uploaded file for task: " + taskId + " size: " + byteArray.length + " bytes");
                        result = 0;
                    } else {
                        Log.d(FabegeApplication.TAG, "Failed to upload file for task: " + taskId);
                    }
                }

            } catch (RetrofitError|UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
        mResult = Integer.valueOf(result);
        return mResult;
    }

    private String createImageXml(String taskId) {
        String retVal = null;
        DocumentFile document = new DocumentFile();
        document.fileName = "image.jpg";
        document.objectType = "Task";
        document.objectId = taskId;
        StringWriter serializedXml = new StringWriter();

        try {
            new Persister().write(document, serializedXml);
            Log.d(FabegeApplication.TAG, "Document xml: " + serializedXml.toString());
            retVal = serializedXml.toString();
        } catch (Exception e) {
            serializedXml = null;
            e.printStackTrace();
        }
        return retVal;
    }
}
