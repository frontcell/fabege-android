package se.fabege.app.util;

/**
 * Created by pqv on 17/09/15.
 */
public class Utility {

    /**
     * Given an inputSize and a chunkSize returns an array with startIndexes and endIndexes for each chunk.
     * Each index pair of the array n (where n is even) and n+1 (where n is odd) denotes the startIndex and endIndex for a chunk.
     * E.g. With an inputSize of 1024 and chunkSize of 500 the returning array is following [0, 500, 500, 1000, 1000, 1024].
     * @param inputSize
     * @param chunkSize
     * @return
     */
    public static int[] chunkIt(int inputSize, int chunkSize) {
        int[] chunks = new int[(int)Math.ceil(inputSize / (double)chunkSize) * 2];

        for (int i = 0, count = 0; i < chunks.length; i++) {
            chunks[i] = count;
            // Next value of count.
            if (i % 2 == 0) {
                count = Math.min(inputSize, (i / 2 * chunkSize) + chunkSize);
            }
        }
        return chunks;
    }

    /**
     * Format date strings of "2015-09-18 00:00:00" to "2015-09-18".
     * @param date
     * @return
     */
    public static String formatDateString(String date) {
        if (date == null) {
            return "";
        }

        String formattedDate = "";
        try {
            formattedDate = date.substring(0, 10);
        } catch (IndexOutOfBoundsException e) {
            // Nop.
        }

        return formattedDate;
    }
}
