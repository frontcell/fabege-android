package se.fabege.app.util

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat


class PermissionHelper {

    enum class PermissionResult { GRANTED, DENIED, CANCELED }

    interface Callback {
        fun onResult(permissions: Array<String>, results: Array<PermissionResult>)
    }

    companion object {

        private const val LOCATION_REQUEST = 12345

        @JvmStatic
        fun isLocationPermissionGranted(context: Context): Boolean {
            return ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
        }

        @JvmStatic
        fun requestLocationPermission(fragment: Fragment) {
            fragment.requestPermissions(arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), LOCATION_REQUEST)
        }

        @JvmStatic
        fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray, callback: Callback) {
            when (requestCode) {
                LOCATION_REQUEST -> callback.onResult(permissions, mapToEnum(permissions, grantResults))
            }
        }

        private fun mapToEnum(permissions: Array<String>, grantResults: IntArray): Array<PermissionResult> {
            if (grantResults.isEmpty()) {
                return permissions.map { PermissionResult.CANCELED }.toTypedArray()
            } else {
                return grantResults.map {
                    if (it == PackageManager.PERMISSION_GRANTED) PermissionResult.GRANTED else PermissionResult.DENIED
                }.toTypedArray()
            }
        }
    }
}