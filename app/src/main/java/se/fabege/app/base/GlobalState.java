package se.fabege.app.base;

import android.content.Context;

public class GlobalState {
    private static Context context;

    public static void setContext(Context context) {
        GlobalState.context = context;
    }

    public static Context getContext() {
        return context;
    }
}
