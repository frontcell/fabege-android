package se.fabege.app.base;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;

import se.fabege.app.storage.UpdateService;
import se.fabege.provider.database.table.StructureTable;
import se.fabege.provider.provider.FabegeProvider;

/**
 * Created by Grindah on 2015-04-12.
 */
public abstract class ProvidedBaseFragment extends BaseFragment implements LoaderManager.LoaderCallbacks<Cursor> {

    protected static final int LOADER_ALL = 1001;
    protected static final int LOADER_FAVOURITES = 1002;

    private final String where = StructureTable.IS_FAVOURITE + " = ?";
    private final String[] whereArgs = new String[]{"1"};

    protected void startFavouriteLoader() {
        getLoaderManager().initLoader(LOADER_FAVOURITES, null, this);
    }

    protected void startAllLoader() {
        getLoaderManager().initLoader(LOADER_ALL, null, this);
    }

    protected void launchDatabaseUpdate() {
        FragmentActivity activity = getActivity();
        Intent intent = new Intent(activity, UpdateService.class);
        intent.setAction(UpdateService.ACTION_UPDATE_DB);
        activity.startService(intent);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        Loader<Cursor> l = null;
        switch (id) {
            case LOADER_ALL:
                l = new CursorLoader(getActivity(), FabegeProvider.STRUCTURE_CONTENT_URI, null, null, null, null);
                break;
            case LOADER_FAVOURITES:
                l = new CursorLoader(getActivity(), FabegeProvider.STRUCTURE_CONTENT_URI, null, where, whereArgs, StructureTable.NAME + " ASC");
                break;

        }
        return l;
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
    }
}
