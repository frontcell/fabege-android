package se.fabege.app.base;

import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import io.github.inflationx.viewpump.ViewPumpContextWrapper;
import se.fabege.app.R;

/**
 * Created by Grindah on 2015-03-26.
 */
public class BaseActivity extends AppCompatActivity {

    @Override
    public void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }

    protected void pushFragment(BaseFragment fragment) {
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.replace(R.id.container, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    protected void popBackStack() {
        FragmentManager manager = getSupportFragmentManager();
        manager.popBackStack();

    }
}
