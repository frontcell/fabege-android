package se.fabege.app.base;

import android.app.Activity;
import android.support.v4.app.Fragment;

import se.fabege.provider.model.Structure;

/**
 * Created by Grindah on 2015-03-26.
 */
public abstract class BaseFragment extends Fragment {

    private IFabegeBaseFragment mListener;

    public interface IFabegeBaseFragment {
        Structure getActiveStructure();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (IFabegeBaseFragment) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement IFabegeBaseFragment");
        }
    }

    /**
     * Override this to consume backpresses in the fragments.
     *
     * @return
     */
    public boolean consumeBackPress() {
        return false;
    }

    protected Structure getActiveStructure() {
        return mListener.getActiveStructure();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }
}
