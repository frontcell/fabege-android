package se.fabege.app.base;

import android.app.Application;
import android.support.multidex.MultiDexApplication;

import io.github.inflationx.calligraphy3.CalligraphyConfig;
import io.github.inflationx.calligraphy3.CalligraphyInterceptor;
import io.github.inflationx.viewpump.ViewPump;
import se.fabege.app.R;

/**
 * Created by Grindah on 2015-03-26.
 */
public class FabegeApplication extends MultiDexApplication {
    public static final String TAG = "Fabege";

    @Override
    public void onCreate() {
        super.onCreate();
        GlobalState.setContext(getApplicationContext());

        ViewPump.init(ViewPump.builder()
                .addInterceptor(new CalligraphyInterceptor(
                        new CalligraphyConfig.Builder()
                                .setFontAttrId(R.attr.fontPath)
                                .build()))
                .build());
    }
}
