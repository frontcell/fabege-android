package se.fabege.app.waw.ui.fragment.onboarding

import android.support.v4.app.Fragment

class OnboardingPageFactory {
    companion object {
        fun newInstance(position: Int): Fragment {
            return when (position) {
                0 -> OnboardingPage2()
                1 -> OnboardingPage1()
                2 -> OnboardingPage3()
                3 -> OnboardingPage4()
                else -> throw IllegalArgumentException("OnboardingPage${position + 1} does not exist")
            }
        }

        fun count(): Int {
            return 4
        }
    }
}