package se.fabege.app.waw.ui.fragment.faq

import se.fabege.app.waw.ui.recyclerview.adapter.AdapterDelegateManager
import se.fabege.app.waw.ui.recyclerview.adapter.Item
import se.fabege.app.waw.ui.recyclerview.adapter.ListAdapter

class FaqAdapter(manager: AdapterDelegateManager<List<Item>>, items: List<Item>) : ListAdapter<List<Item>>(items) {
    init {
        this.manager = manager
    }

    fun updateItems(items: List<Item>) {
        this.items = items
        notifyDataSetChanged()
    }
}