package se.fabege.app.waw.ui.recyclerview.adapter

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup

abstract class AdapterDelegate<T> {
    abstract fun isForViewType(items: T, pos: Int): Boolean
    abstract fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder
    abstract fun onBindViewHolder(items: T, pos: Int, viewHolder: RecyclerView.ViewHolder)
}