package se.fabege.app.waw.ui.recyclerview.adapter.items

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.TextView
import se.fabege.app.R
import se.fabege.app.waw.ui.recyclerview.adapter.Item

data class WorkspaceMetaDataExtraItem(val viewModel: ViewModel) : Item {

    data class ViewModel(val extra: String)

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val extraTextView = itemView.findViewById<TextView>(R.id.extraTextView)
    }

    fun isContentSame(other: WorkspaceMetaDataExtraItem): Boolean {
        return this.viewModel.extra == other.viewModel.extra
    }
}