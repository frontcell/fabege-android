package se.fabege.app.waw.data.datasource

import com.google.android.gms.tasks.Task
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.iid.InstanceIdResult

class FirebaseInstanceIdRemoteDataSource {
    fun getInstanceId(): Task<InstanceIdResult> {
        return FirebaseInstanceId.getInstance().instanceId
    }
}