package se.fabege.app.waw.ui

import android.content.Intent
import se.fabege.app.waw.data.Message

interface BaseView<T : BasePresenter> {
    fun setPresenter(presenter: T)
    fun showMessage(message: Message)
    fun showBusyDialog()
    fun dismissBusyDialog()
    fun exit()
}

interface BasePresenter {
    fun start(intent: Intent?)
    fun resume(intent: Intent?)
    fun stop()
    fun destroy()
}

interface Back {
    fun onBackPressed(): Boolean
    fun goBack()
}