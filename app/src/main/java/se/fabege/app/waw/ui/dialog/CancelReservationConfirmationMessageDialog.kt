package se.fabege.app.waw.ui.dialog

import android.app.Dialog
import android.graphics.Color
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v7.app.AlertDialog
import android.text.Spannable
import android.text.SpannableString
import android.view.LayoutInflater
import android.view.View
import android.widget.TextView
import se.fabege.app.R
import se.fabege.app.waw.data.CancelReservationConfirmationMessage
import se.fabege.app.waw.data.ReservationConfirmationMessage
import se.fabege.app.waw.ui.widget.ColorUnderlineSpan
import java.lang.IllegalStateException

class CancelReservationConfirmationMessageDialog : DialogFragment() {
    lateinit var cancelReservationConfirmationMessage: CancelReservationConfirmationMessage

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return activity?.let {
            val builder = AlertDialog.Builder(it)
            val view = LayoutInflater.from(it).inflate(R.layout.waw_dialog_cancel_reservation_confirmation, null)
            val positive = view.findViewById<View>(R.id.positiveFrameLayout)
            val negative = view.findViewById<View>(R.id.negativeFrameLayout)
            val positiveTextView = view.findViewById<TextView>(R.id.positiveTextView)
            val negativeTextView = view.findViewById<TextView>(R.id.negativeTextView)
            val positiveText = positiveTextView.text
            val negativeText = negativeTextView.text

            val message2Textview = view.findViewById<TextView>(R.id.message2TextView)
            message2Textview.text = cancelReservationConfirmationMessage.message

            val positiveSpannable = SpannableString(positiveText)
            positiveSpannable.setSpan(ColorUnderlineSpan(Color.parseColor("#8A2082"), 0, positiveText.length), 0, positiveText.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
            positiveTextView.text = positiveSpannable

            val negativeSpannable = SpannableString(negativeText)
            negativeSpannable.setSpan(ColorUnderlineSpan(Color.parseColor("#8A2082"), 0, negativeText.length), 0, negativeText.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
            negativeTextView.text = negativeSpannable
            positive.setOnClickListener {
                cancelReservationConfirmationMessage.clickListener.onPositiveCancelReservationButtonClicked()
                dialog.dismiss()
            }
            negative.setOnClickListener {
                cancelReservationConfirmationMessage.clickListener.onNegativeCancelReservationButtonClicked()
                dialog.dismiss()
            }
            builder.setView(view)
            builder.create()
        } ?: throw IllegalStateException("Activity cannot be null")
    }

    interface ClickListener {
        fun onPositiveCancelReservationButtonClicked()
        fun onNegativeCancelReservationButtonClicked()
    }
}