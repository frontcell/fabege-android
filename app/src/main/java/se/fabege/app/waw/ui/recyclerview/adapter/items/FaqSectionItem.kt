package se.fabege.app.waw.ui.recyclerview.adapter.items

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import se.fabege.app.R
import se.fabege.app.waw.domain.model.FaqSection
import se.fabege.app.waw.ui.recyclerview.adapter.Item

data class FaqSectionItem(val viewModel: ViewModel) : Item {

    data class ViewModel(val faqSection: FaqSection)

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val sectionTitleTextView = itemView.findViewById<TextView>(R.id.sectionTitleTextView)
        val sectionContainer = itemView.findViewById<LinearLayout>(R.id.sectionContainer)
        val arrowImageView = itemView.findViewById<ImageView>(R.id.arrowImageView)
    }
}