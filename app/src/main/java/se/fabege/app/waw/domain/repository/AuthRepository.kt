package se.fabege.app.waw.domain.repository

import se.fabege.app.waw.data.Result
import se.fabege.app.waw.domain.model.AuthUser

interface AuthRepository {

    fun isLoggedIn(): Result<Boolean>

    fun getUser(): Result<AuthUser>

    fun isEmailLink(emailLink: String): Result<Boolean>

    suspend fun sendEmailLink(email: String): Result<Unit>

    suspend fun loginWithEmailLink(emailLink: String): Result<AuthUser>

    suspend fun loginWithCustomToken(token: String): Result<AuthUser>

    suspend fun logout(): Result<Boolean>
}