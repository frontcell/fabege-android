package se.fabege.app.waw.ui.recyclerview.adapter.items

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.TextView
import se.fabege.app.R
import se.fabege.app.waw.ui.recyclerview.adapter.Item

data class WorkspaceSummaryItem(val viewModel: ViewModel) : Item {

    data class ViewModel(val summary: String)

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val summaryTextView: TextView = itemView.findViewById(R.id.summaryTextView)
    }
}