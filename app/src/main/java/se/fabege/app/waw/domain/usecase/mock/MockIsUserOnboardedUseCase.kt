package se.fabege.app.waw.domain.usecase.mock

import se.fabege.app.waw.domain.usecase.IsUserOnboardedUseCase

class MockIsUserOnboardedUseCase : IsUserOnboardedUseCase() {

    override fun invoke(): Boolean {
        return false
    }
}