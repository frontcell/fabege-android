package se.fabege.app.waw.ui.fragment

import android.content.Context
import android.support.v4.app.Fragment
import se.fabege.app.waw.data.Message

abstract class BaseFragment: Fragment() {

    protected var baseEventHandler: Events? = null

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        baseEventHandler = context as? Events
        if (baseEventHandler == null) {
            throw ClassCastException("$context must implement BaseFragment.Events")
        }
    }

    fun showMessage(message: Message) {
        baseEventHandler?.showMessage(message)
    }

    fun showBusyDialog() {
        baseEventHandler?.showBusyDialog()
    }

    fun dismissBusyDialog() {
        baseEventHandler?.dismissBusyDialog()
    }

    fun exit() {
        baseEventHandler?.exit()
    }

    interface Events {
        fun showMessage(message: Message)
        fun showBusyDialog()
        fun dismissBusyDialog()
        fun forceBackPressed()
        fun exit()
    }
}