package se.fabege.app.waw.domain.usecase.mock

import se.fabege.app.waw.data.Result
import se.fabege.app.waw.data.error.DataRepositoryException
import se.fabege.app.waw.data.error.WAWUserMissingException
import se.fabege.app.waw.domain.model.WAWUser
import se.fabege.app.waw.domain.repository.AuthRepository
import se.fabege.app.waw.domain.repository.CacheRepository
import se.fabege.app.waw.domain.repository.DataRepository
import se.fabege.app.waw.domain.usecase.GetWAWUserUseCase
import se.fabege.app.waw.misc.Injection
import se.fabege.app.waw.misc.exhaustive

class MockGetWAWUserUseCaseImpl(private val dataRepository: DataRepository) : GetWAWUserUseCase() {
    override suspend fun invoke(): Result<WAWUser> {
        val result = Injection.provideGetUserUseCase().invoke()

        return when (result) {
            is Result.Success -> {
                val result2 = dataRepository.getUsers(result.data.uid)
                return when (result2) {
                    is Result.Success -> {
                        when (result2.data.size) {
                            0 -> Result.Error(WAWUserMissingException())
                            1 -> {
                                Injection.provideCacheRepository().putWAWUser(result2.data.first())
                                Result.Success(result2.data.first())
                            }
                            else -> Result.Error(DataRepositoryException())
                        }
                    }
                    is Result.Error -> result2
                }.exhaustive

            }
            is Result.Error -> result
        }.exhaustive
    }
}