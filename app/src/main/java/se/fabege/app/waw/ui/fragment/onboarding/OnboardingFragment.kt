package se.fabege.app.waw.ui.fragment.onboarding

import android.content.Context
import android.os.Bundle
import android.support.v4.view.ViewPager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.viewpagerindicator.CirclePageIndicator
import se.fabege.app.R
import se.fabege.app.waw.ui.fragment.ToolbarFragment

class OnboardingFragment : ToolbarFragment(), OnboardingPage4.Events {

    private var callback: Events? = null

    private lateinit var pager: ViewPager
    private lateinit var adapter: OnboardingAdapter
    private lateinit var indicator: CirclePageIndicator
    private lateinit var previousButton: View
    private lateinit var nextButton: View

    init {
        xmlLayoutId = R.layout.waw_fragment_onboarding
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        callback = context as? Events
        if (callback == null) {
            throw ClassCastException("$context must implement OnboardingFragment.Events")
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = super.onCreateView(inflater, container, savedInstanceState)
        pager = view?.findViewById<ViewPager>(R.id.pager)!!

        previousButton = view.findViewById(R.id.previousImageView)!!
        previousButton.visibility = View.INVISIBLE
        previousButton.setOnClickListener {
            if (pager.currentItem >= 1) {
                pager.setCurrentItem(pager.currentItem - 1, true)
            }
        }

        nextButton = view.findViewById(R.id.nextImageView)!!
        nextButton.setOnClickListener {
            if (pager.currentItem < adapter.count - 1) {
                pager.setCurrentItem(pager.currentItem + 1, true)
            }
        }

        adapter = OnboardingAdapter(childFragmentManager)
        pager.adapter = adapter

        indicator = view.findViewById<CirclePageIndicator?>(R.id.indicator)!!
        indicator.setViewPager(pager)
        indicator.setOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrolled(p0: Int, p1: Float, p2: Int) {
                if (p0 == adapter.count - 2 && p1 > 0.5) {
                    indicator.visibility = View.INVISIBLE
                }
            }

            override fun onPageSelected(p0: Int) {
                when (p0) {
                    0 -> {
                        previousButton.visibility = View.INVISIBLE
                        nextButton.visibility = View.VISIBLE
                        indicator.visibility = View.VISIBLE

                    }
                    3 -> {
                        previousButton.visibility = View.VISIBLE
                        nextButton.visibility = View.INVISIBLE
                        indicator.visibility = View.INVISIBLE
                    }
                    else -> {
                        previousButton.visibility = View.VISIBLE
                        nextButton.visibility = View.VISIBLE
                        indicator.visibility = View.VISIBLE
                    }
                }
            }

            override fun onPageScrollStateChanged(p0: Int) {
                when (p0) {
                    ViewPager.SCROLL_STATE_DRAGGING -> {
                        previousButton.animate()
                                .alpha(0f)
                        nextButton.animate()
                                .alpha(0f)
                    }
                    ViewPager.SCROLL_STATE_IDLE -> {
                        previousButton.animate().alpha(1f).setListener(null)
                        nextButton.animate().alpha(1f).setListener(null)
                    }
                }
            }

        })
        return view
    }

    override fun onDoneButtonClick() {
        callback?.onOnboardingDoneButtonClick()
    }

    interface Events {
        fun onOnboardingDoneButtonClick()
    }
}