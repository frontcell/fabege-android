package se.fabege.app.waw.ui.dialog

import android.app.Dialog
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v7.app.AlertDialog
import android.view.LayoutInflater
import android.view.View
import android.widget.TextView
import se.fabege.app.R
import se.fabege.app.waw.data.TextMessage
import java.lang.IllegalStateException

class TextMessageDialog : DialogFragment() {
    lateinit var textMessage: TextMessage

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return activity?.let {
            val builder = AlertDialog.Builder(it)
            val view = LayoutInflater.from(it).inflate(R.layout.waw_dialog_text_message, null)
            val titleTextView = view.findViewById<TextView>(R.id.titleTextView)
            val messageTextView = view.findViewById<TextView>(R.id.messageTextView)
            val closeImageButton = view.findViewById<View>(R.id.closeImageButton)
            titleTextView.text = textMessage.title
            messageTextView.text = textMessage.message
            closeImageButton.setOnClickListener {
                dialog.dismiss()
            }
            builder.setView(view)
            builder.create()
        } ?: throw IllegalStateException("Activity cannot be null")
    }
}