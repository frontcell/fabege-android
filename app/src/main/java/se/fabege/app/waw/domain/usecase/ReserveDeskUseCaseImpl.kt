package se.fabege.app.waw.domain.usecase

import se.fabege.app.waw.data.Result
import se.fabege.app.waw.domain.model.Reservation
import se.fabege.app.waw.domain.model.WAWUser
import se.fabege.app.waw.domain.repository.DataRepository
import se.fabege.app.waw.misc.exhaustive

class ReserveDeskUseCaseImpl(private val dataRepository: DataRepository): ReserveDeskUseCase() {

    override suspend fun invoke(user: WAWUser, toAdd: List<Reservation>, toRemove: List<Reservation>): Result<Unit> {
        val result = dataRepository.handleReservations(user.id, toAdd, toRemove)
        return when (result) {
            is Result.Success -> result
            is Result.Error -> result
        }.exhaustive
    }
}