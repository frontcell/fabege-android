package se.fabege.app.waw.domain.usecase

import se.fabege.app.waw.domain.repository.DataRepository

class SubscribeWorkspacesMetadataUseCaseImpl(private val dataRepository: DataRepository) : SubscribeWorkspacesMetadataUseCase() {
    override fun invoke(callback: DataRepository.MetadataCallback) {
        dataRepository.subscribeWorkspacesMetadata(callback)
    }
}