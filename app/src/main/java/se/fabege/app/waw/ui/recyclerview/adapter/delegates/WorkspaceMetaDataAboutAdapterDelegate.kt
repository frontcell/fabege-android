package se.fabege.app.waw.ui.recyclerview.adapter.delegates

import android.content.Context
import android.os.Build
import android.text.Html
import android.text.util.Linkify
import android.view.LayoutInflater
import android.view.ViewGroup
import com.squareup.picasso.Picasso
import se.fabege.app.R
import se.fabege.app.waw.ui.recyclerview.adapter.Item
import se.fabege.app.waw.ui.recyclerview.adapter.ListAdapterDelegate
import se.fabege.app.waw.ui.recyclerview.adapter.items.WorkspaceMetaDataAboutItem

class WorkspaceMetaDataAboutAdapterDelegate(private val context: Context) : ListAdapterDelegate<WorkspaceMetaDataAboutItem, Item, WorkspaceMetaDataAboutItem.ViewHolder>() {
    override fun isForViewType(item: Item, items: List<Item>, pos: Int): Boolean {
        return item is WorkspaceMetaDataAboutItem
    }

    override fun onCreateViewHolder(parent: ViewGroup): WorkspaceMetaDataAboutItem.ViewHolder {
        return WorkspaceMetaDataAboutItem.ViewHolder(LayoutInflater.from(context).inflate(R.layout.waw_rv_item_workspace_metadata_about, parent, false))
    }

    override fun onBindViewHolder(item: WorkspaceMetaDataAboutItem, viewHolder: WorkspaceMetaDataAboutItem.ViewHolder) {
        viewHolder.titleTextView.text = context.getString(R.string.waw_metadata_about, item.viewModel.name)
        if (item.viewModel.imageUrl.isNotEmpty()) {
            Picasso.get().load(item.viewModel.imageUrl).into(viewHolder.imageView)
        }

        viewHolder.descTextView.text = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) Html.fromHtml(item.viewModel.description, Html.FROM_HTML_MODE_COMPACT) else Html.fromHtml(item.viewModel.description)
        Linkify.addLinks(viewHolder.descTextView, Linkify.ALL)
    }
}