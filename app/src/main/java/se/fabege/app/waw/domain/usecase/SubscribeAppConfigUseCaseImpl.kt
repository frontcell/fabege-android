package se.fabege.app.waw.domain.usecase

import se.fabege.app.waw.domain.repository.DataRepository

class SubscribeAppConfigUseCaseImpl(private val dataRepository: DataRepository): SubscribeAppConfigUseCase() {
    override fun invoke(callback: DataRepository.AppConfigCallback) {
        dataRepository.subscribeAppConfig(callback)
    }
}