package se.fabege.app.waw.domain.model

abstract class Key(val timeSlotId: String)

class SuccessKey(val id: String, timeSlotId: String): Key(timeSlotId)

class ErrorKey(val message: String, timeSlotId: String): Key(timeSlotId)