package se.fabege.app.waw.data.error

import java.lang.RuntimeException

class SendSignInLinkToEmailException : RuntimeException()