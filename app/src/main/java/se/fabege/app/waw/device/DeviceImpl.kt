package se.fabege.app.waw.device

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.widget.Toast
import se.fabege.app.R
import se.fabege.app.analytics.AnalyticsImpl
import se.fabege.app.analytics.Event
import se.fabege.app.waw.data.SelectCalendarMessage
import se.fabege.app.waw.misc.Injection
import se.fabege.app.waw.ui.activity.waw.WAWActivity
import se.fabege.app.waw.ui.dialog.SelectCalendarMessageDialog

class DeviceImpl(private val activity: Activity) : Device, CalendarManager.Callback, SelectCalendarMessageDialog.ClickListener {
    private var calendarManager: CalendarManager = CalendarManager(activity.applicationContext.contentResolver, this)
    private lateinit var calendars: CalendarManager.Calendars
    private lateinit var events: List<CalendarManager.Event>

    override fun addCalendarEvents(events: List<CalendarManager.Event>) {
        this.events = events
        calendarManager.getCalendarsAsync()
    }

    override fun deleteCalendarEvents(events: List<CalendarManager.Event>) {
        val keyValueRepository = Injection.provideKeyValueRepositoryFactory().get("device")

        for (event in events) {
            val eventId = keyValueRepository.getLong(event.id)
            if (eventId != 0L) {
                calendarManager.deleteEventAsync(eventId)
            }
        }
    }

    override fun requestCalendarPermissions(callback: Device.CalendarPermissionsCallback) {
        (activity as WAWActivity).calendarPermissionsCallback = callback
        ActivityCompat.requestPermissions(activity, arrayOf(Manifest.permission.WRITE_CALENDAR, Manifest.permission.READ_CALENDAR), WAWActivity.REQUEST_CALENDAR_PERMISSIONS_CODE)
    }

    override fun isCalendarPermissionsGranted(): Boolean {
        return ContextCompat.checkSelfPermission(activity, android.Manifest.permission.WRITE_CALENDAR) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(activity, android.Manifest.permission.READ_CALENDAR) == PackageManager.PERMISSION_GRANTED
    }


    override fun onGetCalendarsComplete(calendars: CalendarManager.Calendars?) {
        this.calendars = calendars!!
        (activity as WAWActivity).showMessage(SelectCalendarMessage(calendars, this))
    }

    override fun onCalendarClicked(calendarId: Long) {
        for (event in events) {
            calendarManager.addEventAsync(calendarId, event)
        }
    }

    override fun onAddEventComplete(id: String, eventId: Long) {
        val keyValueRepository = Injection.provideKeyValueRepositoryFactory().get("device")
        keyValueRepository.putLong(id, eventId)
        Toast.makeText(activity, R.string.waw_calendar_event_added, Toast.LENGTH_SHORT).show()
    }

    override fun onDeleteEventComplete() {
        Toast.makeText(activity, R.string.waw_calendar_event_removed, Toast.LENGTH_SHORT).show()
    }

    override fun onAddReminderComplete(remainderId: Long) {
        // ignore
    }


    override fun openEmailApp() {
        activity.startActivity(createOpenEmailAppIntent())
    }

    override fun openParakeyGooglePlayPage() {
        val intent = Intent(Intent.ACTION_VIEW).apply {
            data = Uri.parse(
                    "https://play.google.com/store/apps/details?id=co.parakey.parakey")
            setPackage("com.android.vending")
        }
        activity.startActivity(intent)
    }

    override fun openFabegeGooglePlayPage() {
        val intent = Intent(Intent.ACTION_VIEW).apply {
            data = Uri.parse(
                    "https://play.google.com/store/apps/details?id=se.fabege.app")
            setPackage("com.android.vending")
        }
        activity.startActivity(intent)
    }

    override fun openGoogleMapApp(latitude: Float, longitude: Float) {
        activity.startActivity(createOpenGoogleMapIntent(latitude, longitude))
    }

    override fun openParakeyApp(email: String, password: String, requestCode: Int) {
        val intent = createOpenParakeyAppIntent(email, password)
        activity.startActivityForResult(intent, requestCode)
    }

    override fun callPhoneNumber(phoneNumber: String) {
        activity.startActivity(createCallPhoneNumberIntent(phoneNumber))
    }

    override fun sendEmail(email: String) {
        activity.startActivity(createSendEmailIntent(email))
    }

    override fun isGoogleMapAppInstalled(): Boolean {
        return createOpenGoogleMapIntent(0f, 0f).resolveActivity(activity.packageManager) != null
    }

    override fun canCallPhoneNumber(): Boolean {
        return createCallPhoneNumberIntent("dummy@number").resolveActivity(activity.packageManager) != null
    }

    override fun isEmailAppInstalled(): Boolean {
        return createOpenEmailAppIntent().resolveActivity(activity.packageManager) != null
    }

    override fun isParakeyAppInstalled(): Boolean {
//        return true
        return createOpenParakeyAppIntent("dummy@email.com", "dummy@password").resolveActivity(activity.packageManager) != null
    }

    override fun canSendEmail(): Boolean {
        return createSendEmailIntent("dummy@email.com").resolveActivity(activity.packageManager) != null
    }

    private fun createOpenEmailAppIntent(): Intent {
        return Intent(Intent.ACTION_MAIN).apply {
            addCategory(Intent.CATEGORY_APP_EMAIL)
            addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        }
    }

    private fun createCallPhoneNumberIntent(phoneNumber: String): Intent {
        return Intent(Intent.ACTION_DIAL).apply {
            data = Uri.parse("tel:$phoneNumber")
        }
    }

    private fun createOpenParakeyAppIntent(email: String, password: String): Intent {
        return Intent(
                Intent.ACTION_VIEW,
                Uri.parse("parakeyapp://parakey?username=$email&code=$password")
        )
    }

    private fun createSendEmailIntent(email: String): Intent {
        return Intent(Intent.ACTION_SEND).apply {
            type = "*/*"
            putExtra(Intent.EXTRA_EMAIL, arrayOf(email))
        }
    }

    private fun createOpenGoogleMapIntent(latitude: Float, longitude: Float): Intent {
        val gmmIntentUri = Uri.parse("google.navigation:q=$latitude,$longitude")
        val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
        mapIntent.setPackage("com.google.android.apps.maps")
        return mapIntent
    }

    override fun trackPageView(name: String) {
        val event = Event(name, emptyMap())
        AnalyticsImpl.getInstance().trackPageView(event)
    }

    override fun trackEvent(name: String) {
        val event = Event(name, emptyMap())
        AnalyticsImpl.getInstance().trackEvent(event)
    }

    private fun createMap(key: String, value: String): Map<String, String> {
        val map = HashMap<String, String>()
        map[key] = value
        return map
    }
}