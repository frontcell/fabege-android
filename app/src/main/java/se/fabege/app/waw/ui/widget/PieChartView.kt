package se.fabege.app.waw.ui.widget

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.RectF
import android.util.AttributeSet
import android.view.View


class PieChartView(context: Context, attrs: AttributeSet) : View(context, attrs) {
    private var paint: Paint = Paint()
    private var colors: Array<Int> = arrayOf(/*Color.parseColor("#E6E1DC"),*/ Color.parseColor("#C289BF"), Color.parseColor("#8B1984"))
    private lateinit var rectF: RectF
    private lateinit var values: List<Int>
    private var unitDegree: Float = 0f

    init {
        paint.isAntiAlias = true
        paint.color = colors[0]
        paint.strokeWidth = 3f
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        rectF = RectF(0f, 0f, w.toFloat(), h.toFloat())
    }

    override fun onDraw(canvas: Canvas) {
        var offset = 270f

        for ((index, value) in values.withIndex()) {
            paint.color = colors[index]
            canvas.drawArc(rectF, offset, value * unitDegree, true, paint)
            offset += value * unitDegree
            offset %= 360f
        }
    }

    fun setValues(values: List<Int>) {
        this.values = values
        var sum = 0
        for (value in values) {
            sum += value
        }
        unitDegree = 360f / sum

        requestLayout()
    }

}

