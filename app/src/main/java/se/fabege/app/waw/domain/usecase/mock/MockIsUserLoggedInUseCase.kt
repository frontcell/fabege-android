package se.fabege.app.waw.domain.usecase.mock

import se.fabege.app.waw.domain.usecase.IsUserLoggedInUseCase

class MockIsUserLoggedInUseCase: IsUserLoggedInUseCase() {

    override fun invoke(): Boolean {
        return true
    }
}