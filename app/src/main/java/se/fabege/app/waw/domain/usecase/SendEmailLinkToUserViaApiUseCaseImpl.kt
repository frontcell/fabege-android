package se.fabege.app.waw.domain.usecase

import se.fabege.app.waw.data.Result
import se.fabege.app.waw.domain.repository.ApiRepository
import se.fabege.app.waw.misc.exhaustive

class SendEmailLinkToUserViaApiUseCaseImpl(private val apiRepository: ApiRepository) : SendEmailLinkToUserUseCase() {
    override suspend fun invoke(email: String): Result<Unit> {
        val result = apiRepository.sendEmailLink(email)
        return when (result) {
            is Result.Success -> result
            is Result.Error -> result
        }.exhaustive
    }
}