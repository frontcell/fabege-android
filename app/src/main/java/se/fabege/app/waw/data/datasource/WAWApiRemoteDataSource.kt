package se.fabege.app.waw.data.datasource

import se.fabege.app.waw.data.Result
import se.fabege.app.waw.data.api.WAWApi
import se.fabege.app.waw.data.error.WAWApiException

class WAWApiRemoteDataSource(private val wawApi: WAWApi) {

    suspend fun sendSignInLinkToUid(uid: String): Result<Unit> {
        return try {
            val response = wawApi.authentificate(uid).await()
            if (response.isSuccessful) {
                Result.Success(Unit)
            } else {
                Result.Error(WAWApiException())
            }
        } catch (e: Exception) {
            Result.Error(WAWApiException())
        }
    }

    companion object {
        @Volatile
        private var INSTANCE: WAWApiRemoteDataSource? = null

        fun getInstance(wawApi: WAWApi): WAWApiRemoteDataSource {
            return INSTANCE ?: synchronized(this) {
                INSTANCE ?: WAWApiRemoteDataSource(wawApi).also { INSTANCE = it }
            }
        }
    }
}