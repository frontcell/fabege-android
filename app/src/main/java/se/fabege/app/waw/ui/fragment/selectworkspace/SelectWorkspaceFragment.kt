package se.fabege.app.waw.ui.fragment.selectworkspace

import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import se.fabege.app.R
import se.fabege.app.waw.misc.Injection
import se.fabege.app.waw.misc.Utils
import se.fabege.app.waw.ui.fragment.ToolbarFragment
import se.fabege.app.waw.ui.recyclerview.adapter.AdapterDelegateManager
import se.fabege.app.waw.ui.recyclerview.adapter.Item
import se.fabege.app.waw.ui.recyclerview.adapter.delegates.WorkspaceAdapterDelegate
import se.fabege.app.waw.ui.recyclerview.adapter.delegates.WorkspaceSummaryAdapterDelegate
import se.fabege.app.waw.ui.widget.DividerItemDecoration

class SelectWorkspaceFragment : ToolbarFragment(), SelectWorkspaceContract.View {
    private var selectWorkspaceEventHandler: Events? = null

    private lateinit var presenter: SelectWorkspaceContract.Presenter
    private lateinit var adapter: SelectWorkspaceAdapter
    private lateinit var keyButton: FrameLayout
    private lateinit var keyArrow: View
    private lateinit var keyText: View

    init {
        xmlLayoutId = R.layout.waw_fragment_select_workspace
    }

    override fun setPresenter(presenter: SelectWorkspaceContract.Presenter) {
        this.presenter = presenter
        presenter.start(activity?.intent)
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        selectWorkspaceEventHandler = context as? SelectWorkspaceFragment.Events
        if (selectWorkspaceEventHandler == null) {
            throw ClassCastException("$context must implement SelectWorkspaceFragment.Events")
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        SelectWorkspacePresenter(this, Injection.provideDevice(activity!!))
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = super.onCreateView(inflater, container, savedInstanceState)

        toolbarTitleTextView?.text = getString(R.string.waw_fragment_select_workspace_toolbar_title)

        toolbarFaqButton?.setOnClickListener {
            presenter.onFaqButtonClicked()
        }

        val recyclerView = view!!.findViewById<RecyclerView>(R.id.recyclerView)
        recyclerView.layoutManager = LinearLayoutManager(context)

        val adapterDelegateManager = AdapterDelegateManager<List<Item>>()
        adapterDelegateManager.addDelegate(WorkspaceSummaryAdapterDelegate(context!!))
        adapterDelegateManager.addDelegate(WorkspaceAdapterDelegate(context!!, presenter))

        adapter = SelectWorkspaceAdapter(adapterDelegateManager, emptyList())

        val dividerItemDecoration = DividerItemDecoration(context!!, LinearLayoutManager.VERTICAL)
        dividerItemDecoration.setDrawable(ContextCompat.getDrawable(context!!, R.drawable.waw_divider)!!)
        recyclerView.addItemDecoration(dividerItemDecoration)
        recyclerView.adapter = adapter

        keyButton = view.findViewById(R.id.keyFrameLayout)
        keyArrow = view.findViewById(R.id.keyArrowImageView)
        keyText = view.findViewById(R.id.keyArrowTextView)
        setKeyButtonEnabled(false)
        return view
    }

    override fun onResume() {
        super.onResume()
        presenter.resume(activity?.intent)
    }

    override fun onStop() {
        super.onStop()
        presenter.stop()
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.destroy()
    }

    override fun showWorkspaces(items: List<Item>) {
        adapter.updateItems(items)
    }

    override fun showReserveDesk(workspaceId: String, deskId: String) {
        selectWorkspaceEventHandler?.showReserveDesk(workspaceId, deskId)
    }

    override fun showFaq() {
        selectWorkspaceEventHandler?.showFaq()
    }

    override fun setKeyButtonEnabled(enabled: Boolean) {
        if (enabled) {
            keyButton.elevation = Utils.dpToPx(10).toFloat()
            keyButton.setBackgroundColor(Color.parseColor("#8A2082"))
            keyButton.setOnClickListener { presenter.onKeyButtonClicked() }
            keyArrow.visibility = View.VISIBLE
            keyText.visibility = View.VISIBLE
        } else {
            keyButton.elevation = 0f
            keyButton.setBackgroundColor(Color.parseColor("#E6E1DC"))
            keyButton.setOnClickListener(null)
            keyArrow.visibility = View.INVISIBLE
            keyText.visibility = View.INVISIBLE
        }
    }


    interface Events {
        fun showReserveDesk(workspaceId: String, deskId: String)
        fun showFaq()
    }
}