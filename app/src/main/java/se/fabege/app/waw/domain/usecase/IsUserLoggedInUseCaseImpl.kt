package se.fabege.app.waw.domain.usecase

import se.fabege.app.waw.domain.repository.AuthRepository
import se.fabege.app.waw.data.Result
import se.fabege.app.waw.misc.exhaustive

class IsUserLoggedInUseCaseImpl(private val authRepository: AuthRepository): IsUserLoggedInUseCase() {

    override operator fun invoke(): Boolean {
        val result = authRepository.isLoggedIn()

        return when (result) {
            is Result.Success -> result.data
            is Result.Error -> false
        }.exhaustive
    }
}