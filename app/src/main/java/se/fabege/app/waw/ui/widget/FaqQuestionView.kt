package se.fabege.app.waw.ui.widget

import android.content.Context
import android.graphics.Color
import android.os.Build
import android.text.Html
import android.text.Spannable
import android.text.SpannableString
import android.text.util.Linkify
import android.view.LayoutInflater
import android.widget.FrameLayout
import android.widget.TextView
import se.fabege.app.R
import se.fabege.app.waw.domain.model.FaqQuestion

class FaqQuestionView(context: Context) : FrameLayout(context) {

    lateinit var questionTextView: TextView
    lateinit var answerTextView: TextView

    init {
        LayoutInflater.from(context).inflate(R.layout.waw_view_faq_question, this, true)
        questionTextView = findViewById(R.id.questionTitleTextView)
        answerTextView = findViewById(R.id.answerTextView)
    }

    fun update(faqQuestion: FaqQuestion) {
        val questionSpannable = SpannableString(faqQuestion.question)
        questionSpannable.setSpan(ColorUnderlineSpan(Color.parseColor("#8A2082"), 0, questionSpannable.length), 0, questionSpannable.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        questionTextView.text = questionSpannable
        answerTextView.text = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) Html.fromHtml(faqQuestion.answer, Html.FROM_HTML_MODE_COMPACT) else Html.fromHtml(faqQuestion.answer)
        Linkify.addLinks(answerTextView, Linkify.ALL)
    }
}