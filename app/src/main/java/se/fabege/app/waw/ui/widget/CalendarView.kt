package se.fabege.app.waw.ui.widget

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.FrameLayout
import android.widget.LinearLayout
import android.widget.TextView
import se.fabege.app.R
import se.fabege.app.waw.misc.toDayMonth
import se.fabege.app.waw.misc.toLegend
import se.fabege.app.waw.misc.toWeekday
import se.fabege.app.waw.ui.recyclerview.adapter.items.CalendarItem
import java.util.*

class CalendarView(context: Context, attrs: AttributeSet) : FrameLayout(context, attrs) {
    private var dateContainer: LinearLayout
    private var dataContainer: LinearLayout
    private var callback: ClickListener? = null

    init {
        LayoutInflater.from(context).inflate(R.layout.waw_view_calendar, this, true)
        dateContainer = findViewById(R.id.linearLayout1)
        dataContainer = findViewById(R.id.linearLayout2)
    }

    private interface TableItem {
        fun getLegend(): String
    }

    private abstract class Data(val decoration: CalendarItem.Decoration) : TableItem {
        open fun getCount(): Int {
            return 1
        }

        open fun getIds(): List<String> {
            return emptyList()
        }

        override fun getLegend(): String {
            return decoration.toLegend()
        }

        override fun toString(): String {
            return "[$decoration]"
        }
    }

    private class EmptyData : Data(CalendarItem.Decoration.NONE)

    private class SingleData(decoration: CalendarItem.Decoration, val id: String) : Data(decoration) {
        override fun getIds(): List<String> {
            return listOf(id)
        }
    }

    private class MergedData(decoration: CalendarItem.Decoration, val _count: Int, val _ids: List<String>) : Data(decoration) {

        override fun getCount(): Int {
            return _count
        }

        override fun getIds(): List<String> {
            return _ids
        }
    }

    private abstract class Padding(var layoutId: Int?) : TableItem {
        override fun getLegend(): String {
            return "P"
        }
    }

    private class DataPadding : Padding(null) {
        override fun toString(): String {
            return "[D PADD]"
        }
    }

    private class RowPadding : Padding(null) {
        override fun toString(): String {
            return "[R PADD]"
        }
    }

    fun update(viewModel: CalendarItem.ViewModel) {
        var table = buildTable(viewModel.rows)
        prettyPrint(table)
        table = reduceTable(table)
        table = decoratePadding(table)

        dateContainer.removeAllViews()
        dataContainer.removeAllViews()
        for ((index, row) in viewModel.rows.withIndex()) {
            dateContainer.addView(createDateView(row.date))
//            dataContainer.addView(createDataView(row.data))
            if (!last(index, viewModel)) {
                dateContainer.addView(LayoutInflater.from(context).inflate(R.layout.waw_view_calendar_date_filler, dateContainer, false))
//                dataContainer.addView(createDataViewFiller(row.data, viewModel.rows[index + 1].data))
            }
        }

        for ((index, row) in table.withIndex()) {
            if (index % 2 == 0) {
                dataContainer.addView(createDataView(row))
            } else {
                dataContainer.addView(createPaddingView(row))
            }

        }
    }

    private fun createDataView(items: Array<TableItem>): View {

        fun getLayoutId(item: Data): Int {
            return when (item.decoration) {
                CalendarItem.Decoration.NONE -> R.layout.waw_view_calendar_data_none
                CalendarItem.Decoration.HOLIDAY -> R.layout.waw_view_calendar_data_none
                CalendarItem.Decoration.OPEN -> R.layout.waw_view_calendar_data_open
                CalendarItem.Decoration.CLOSED -> R.layout.waw_view_calendar_data_closed
                CalendarItem.Decoration.FULL -> R.layout.waw_view_calendar_data_full
                CalendarItem.Decoration.RESERVED -> R.layout.waw_view_calendar_data_reserved
                CalendarItem.Decoration.SELECTED -> R.layout.waw_view_calendar_data_selected
            }
        }

        val inflater = LayoutInflater.from(context)
        val root = inflater.inflate(R.layout.waw_view_calendar_data, dataContainer, false) as LinearLayout

        for (item in items) {
            when (item) {
                is SingleData -> {
                    val view = inflater.inflate(getLayoutId(item), dataContainer, false)
                    view.tag = item.getIds()
                    view.setOnClickListener { v -> callback?.onRowDataClicked(v.tag as List<String>) }
                    root.addView(view)
                }
                is MergedData -> {
                    val view = inflater.inflate(getLayoutId(item), dataContainer, false)
                    val params = view.layoutParams as LinearLayout.LayoutParams
                    params.weight = item.getCount().toFloat()
                    view.layoutParams = params
                    view.tag = item.getIds()
                    view.setOnClickListener { v -> callback?.onRowDataClicked(v.tag as List<String>) }
                    root.addView(view)
                }
                is EmptyData -> {
                    val view = inflater.inflate(getLayoutId(item), dataContainer, false)
                    root.addView(view)
                }
                is DataPadding -> {
                    root.addView(inflater.inflate(item.layoutId!!, root, false))
                }
            }
        }

        return root
    }

    private fun createPaddingView(items: Array<TableItem>): View {
        val inflater = LayoutInflater.from(context)
        val root = inflater.inflate(R.layout.waw_view_calendar_data_filler, dataContainer, false) as LinearLayout

        for (item in items) {
            when (item) {
                is Padding -> {
                    root.addView(inflater.inflate(item.layoutId!!, root, false))
                }
            }
        }
        return root
    }

    private fun buildTable(rows: List<CalendarItem.Row>): Array<Array<TableItem>> {
        fun numOfTableRows(): Int {
            return if (rows.isEmpty()) {
                0
            } else {
                rows.size + rows.size - 1
            }
        }

        fun dataRowSize(index: Int): Int {
            return rows[index].data.size + rows[index].data.size - 1
        }

        fun nextDataRowSize(index: Int): Int {
            return dataRowSize(index + 1)
        }

        fun numOfRowTableItems(index: Int): Int {
            return if (index % 2 == 0) {
                // Data row
                val dataRowSize = dataRowSize(index / 2)
                if (dataRowSize <= 0) 1 else dataRowSize
            } else {
                // Padding row, peek into next data row
                val nextDataRowSize = nextDataRowSize(index / 2)
                if (nextDataRowSize <= 0) 1 else nextDataRowSize
            }
        }

        fun createTableItem(rowIndex: Int, itemIndex: Int): TableItem {
            return if (rowIndex % 2 == 0) {
                // Data row
                if (itemIndex % 2 == 0) {
                    // Data item
                    val row = rows[rowIndex / 2]
                    if (row.data.isNotEmpty()) {
                        val rowData = row.data[itemIndex / 2]
                        SingleData(rowData.decoration, rowData.id)
                    } else {
                        EmptyData()
                    }
                } else {
                    // DataPadding item
                    DataPadding()
                }
            } else {
                // RowPadding row
                RowPadding()
            }
        }

        return Array(numOfTableRows()) { rowIndex -> Array(numOfRowTableItems(rowIndex)) { itemIndex -> createTableItem(rowIndex, itemIndex) } }
    }

    private fun reduceTable(table: Array<Array<TableItem>>): Array<Array<TableItem>> {

        fun getTableItem(rowIndex: Int, itemIndex: Int): TableItem {
            return table[rowIndex][itemIndex]
        }

        fun getData(rowIndex: Int, itemIndex: Int): Data {
            return table[rowIndex][itemIndex] as Data
        }

        fun hasNextData(rowIndex: Int, itemIndex: Int): Boolean {
            return table[rowIndex].size - 1 > itemIndex
        }

        fun getNextData(rowIndex: Int, itemIndex: Int): Data {
            return getData(rowIndex, itemIndex + 2)
        }

        fun mergeCurrentWithNext(rowIndex: Int, itemIndex: Int): Data {
            val data = getData(rowIndex, itemIndex)
            val nextData = getNextData(rowIndex, itemIndex)
            return MergedData(data.decoration, data.getCount() + nextData.getCount(), listOf(data.getIds(), nextData.getIds()).flatten())
        }

        fun merge(rowIndex: Int, itemIndex: Int) {
            val currentArray = table[rowIndex]

            val newArray = Array(currentArray.size - 2) { index ->
                if (index == itemIndex) {
                    mergeCurrentWithNext(rowIndex, itemIndex)
                } else {
                    getTableItem(rowIndex, if (index > itemIndex) index + 2 else index)
                }
            }
            table[rowIndex] = newArray

            if (rowIndex > 0) {
                val paddingArray = Array<TableItem>(newArray.size) { RowPadding() }

                table[rowIndex - 1] = paddingArray
            }
        }

        fun reduce(): Boolean {
            for (row in 0 until table.size step 2) {
                for (item in 0 until table[row].size step 2) {
                    if (hasNextData(row, item)) {
                        val data = getData(row, item)
                        val nextData = getNextData(row, item)
                        if (data.decoration != CalendarItem.Decoration.OPEN && data.decoration == nextData.decoration) {
                            merge(row, item)
                            return true
                        }
                    }

                }
            }
            return false
        }

        while (reduce()) {
            prettyPrint(table)
        }

        return table
    }

    private fun decoratePadding(table: Array<Array<TableItem>>): Array<Array<TableItem>> {
        fun getLeftRight(row: Int, col: Int): String {
            val left = if (col == 0) "X" else table[row][col - 1].getLegend()
            val right = if (col == table[row].lastIndex) "X" else table[row][col + 1].getLegend()

            return left + right
        }

        fun getTopBottom(row: Int, col: Int): String {
            val top = if (row == 0) "X" else if (col > table[row - 1].lastIndex) table[row - 1][table[row - 1].lastIndex].getLegend() else table[row - 1][col].getLegend()
            val bottom = table[row + 1][col].getLegend()

            return top + bottom
        }

        for ((r, row) in table.withIndex()) {
            for ((c, col) in row.withIndex()) {
                when (table[r][c]) {
                    is DataPadding -> {
                        (table[r][c] as DataPadding).layoutId = R.layout.waw_view_calendar_space_6dp
                    }
                    is RowPadding -> {
                        if (c % 2 == 0) {
                            when(getTopBottom(r, c)) {
                                "NN", "HH", "ON", "OH", "NC", "HC", "HO", "HR"-> (table[r][c] as RowPadding).layoutId = R.layout.waw_view_calendar_space_transparent
                                else -> (table[r][c] as RowPadding).layoutId = R.layout.waw_view_calendar_space
                            }
                        } else {
                            (table[r][c] as RowPadding).layoutId = R.layout.waw_view_calendar_space_6dp
                        }
                    }
                }
            }
        }
        return table
    }

    private fun prettyPrint(table: Array<Array<TableItem>>) {
        for (row in table) {
            for (item in row) {
                System.out.printf("%-10s", item)
            }
            println()
        }
    }

    fun last(index: Int, viewModel: CalendarItem.ViewModel): Boolean {
        return index == viewModel.rows.size - 1
    }

    fun setClickListener(clickListener: ClickListener) {
        this.callback = clickListener
    }

    private fun createDateView(date: Date): View {
        val inflater = LayoutInflater.from(context)
        val view = inflater.inflate(R.layout.waw_view_calendar_date, dateContainer, false)

        val weekdayTextView = view.findViewById<TextView>(R.id.weekdayTextView)
        weekdayTextView.text = date.toWeekday()

        val dayMonthTextView = view.findViewById<TextView>(R.id.dayMonthTextView)
        dayMonthTextView.text = date.toDayMonth()

        return view
    }


    private fun adjacentIndexesIsNoneOrHoliday(index: Int, rowDatas: List<CalendarItem.RowData>): Boolean {
        return indexLeftIsNoneOrHoliday(index, rowDatas) && indexRightIsNoneOrHoliday(index, rowDatas)
    }

    private fun indexIsNoneOrHoliday(index: Int, rowDatas: List<CalendarItem.RowData>): Boolean {
        return rowDatas[index].decoration == CalendarItem.Decoration.NONE || rowDatas[index].decoration == CalendarItem.Decoration.HOLIDAY
    }

    private fun indexLeftIsNoneOrHoliday(index: Int, rowDatas: List<CalendarItem.RowData>): Boolean {
        return if (index <= 0) {
            true
        } else {
            rowDatas[index - 1].decoration == CalendarItem.Decoration.NONE || rowDatas[index - 1].decoration == CalendarItem.Decoration.HOLIDAY
        }
    }

    private fun indexRightIsNoneOrHoliday(index: Int, rowDatas: List<CalendarItem.RowData>): Boolean {
        return if (index >= rowDatas.size - 1) {
            true
        } else {
            rowDatas[index + 1].decoration == CalendarItem.Decoration.NONE || rowDatas[index + 1].decoration == CalendarItem.Decoration.HOLIDAY
        }
    }

    interface ClickListener {
        fun onRowDataClicked(ids: List<String>)
    }
}