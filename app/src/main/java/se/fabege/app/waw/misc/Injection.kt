package se.fabege.app.waw.misc

import android.app.Activity
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import kotlinx.coroutines.Dispatchers
import retrofit2.Retrofit
import se.fabege.app.base.GlobalState
import se.fabege.app.waw.data.*
import se.fabege.app.waw.data.api.WAWApi
import se.fabege.app.waw.data.datasource.FirebaseAuthRemoteDataSource
import se.fabege.app.waw.data.datasource.FirebaseFirestoreRemoteDataSource
import se.fabege.app.waw.data.datasource.FirebaseInstanceIdRemoteDataSource
import se.fabege.app.waw.data.datasource.WAWApiRemoteDataSource
import se.fabege.app.waw.data.repository.ApiRepositoryImpl
import se.fabege.app.waw.data.repository.AuthRepositoryImpl
import se.fabege.app.waw.data.repository.CacheRepositoryImpl
import se.fabege.app.waw.data.repository.DataRepositoryImpl
import se.fabege.app.waw.device.Device
import se.fabege.app.waw.device.DeviceImpl
import se.fabege.app.waw.domain.model.KeyValueRepositoryFactory
import se.fabege.app.waw.domain.repository.*
import se.fabege.app.waw.domain.usecase.*
import se.fabege.app.waw.domain.usecase.mock.MockGetUserUseCase
import se.fabege.app.waw.domain.usecase.mock.MockGetWAWUserUseCaseImpl
import se.fabege.app.waw.domain.usecase.mock.MockIsUserLoggedInUseCase

class Injection {
    companion object {
        fun provideCoroutinesDispatcherProvider() = CoroutinesDispatcherProvider(
                Dispatchers.Main,
                Dispatchers.Default,
                Dispatchers.IO)

        fun provideDevice(activity: Activity): Device {
            return DeviceImpl(activity)
        }

        fun provideDataRepository(): DataRepository {
            return DataRepositoryImpl.getInstance(Pair(FirebaseFirestoreRemoteDataSource(), FirebaseInstanceIdRemoteDataSource()))
        }

        fun provideAuthRepository(): AuthRepository {
            return AuthRepositoryImpl(FirebaseAuthRemoteDataSource, provideKeyValueRepositoryFactory().get("authrepository"))
        }

        fun provideKeyValueRepositoryFactory(): KeyValueRepositoryFactory {
            return KeyValueRepositoryFactoryImpl(GlobalState.getContext())
        }

        fun provideDefaultKeyValueRepository(): KeyValueRepository {
            return provideKeyValueRepositoryFactory().get("default")
        }

        fun provideApiRepository(): ApiRepository {
            return ApiRepositoryImpl(WAWApiRemoteDataSource(provideWAWApi()), provideKeyValueRepositoryFactory().get("authrepository"))
        }

        fun provideIsUserLoggedInUseCase(): IsUserLoggedInUseCase {
            return IsUserLoggedInUseCaseImpl(provideAuthRepository())
//            return MockIsUserLoggedInUseCase()
        }

        fun provideGetUserUseCase(): GetUserUseCase {
            return GetUserUseCaseImpl(provideAuthRepository())
//            return MockGetUserUseCase()
        }

        fun provideSendEmailLinkToUserUserCase(): SendEmailLinkToUserUseCase {
//            return SendEmailLinkToUserUseCaseImpl(provideAuthRepository())
            return SendEmailLinkToUserViaApiUseCaseImpl(provideApiRepository())
        }

        fun provideIsLogInWithEmailLinkUseCase(): IsLogInWithEmailLinkUseCase {
//            return IsLogInWithEmailLinkUseCaseImpl(provideAuthRepository())
            return IsLogInWithEmailLinkViaApiUseCaseImpl()
        }

        fun provideLogInUserWithEmailLinkUseCase(): LogInUserWithEmailLinkUseCase {
//            return LogInUserWithEmailLinkUseCaseImpl(provideAuthRepository())
            return LogInUserWithEmailLinkViaApiUseCaseImpl(provideAuthRepository())
        }

        fun provideIsUserOnboardedUseCase(): IsUserOnboardedUseCase {
            return IsUserOnboardedUseCaseImpl(provideGetUserUseCase(), provideKeyValueRepositoryFactory())
        }

        fun provideSetUserOnboardedUseCase(): SetUserOnboardedUseCase {
            return SetUserOnboardedUseCaseImpl(provideGetUserUseCase(), provideKeyValueRepositoryFactory())
        }

        fun provideIsEmailEligibleUseCase(): IsEmailEligibleUseCase {
            return IsEmailEligibleUseCaseImpl(provideDataRepository())
        }

        fun provideLogoutUserUseCase(): LogoutUserUseCase {
            return LogoutUserUserCaseImpl(provideAuthRepository())
        }

        fun provideGetWorkspacesUseCase(): GetWorkspacesUseCase {
            return GetWorkspacesUseCaseImpl(provideDataRepository())
        }

        fun provideGetWorkspaceUseCase(): GetWorkspaceUseCase {
            return GetWorkspaceUseCaseImpl(provideDataRepository())
        }

        fun provideSubscribeWorkspaceCalendarUseCase(): SubcribeWorkspaceCalendarUseCase {
            return SubscribeWorkspaceCalendarUseCaseImpl(provideDataRepository())
        }

        fun provideSubscripbeWAWUserUseCase(): SubscribeWAWUserUseCase {
            return SubscribeWAWUserUseCaseImpl(provideDataRepository())
        }

        fun provideSubscribeAppConfigUseCase(): SubscribeAppConfigUseCase {
            return SubscribeAppConfigUseCaseImpl(provideDataRepository())
        }

        fun provideUnsubscribeUseCase(): UnsubscribeUseCase {
            return UnsubscribeUseCaseImpl(provideDataRepository())
        }

        fun provideGetWAWUserUseCase(): GetWAWUserUseCase {
            return GetWAWUserUseCaseImpl(provideAuthRepository(), provideDataRepository())
//            return MockGetWAWUserUseCaseImpl(provideDataRepository())
        }

        fun provideGetWAWUserWithEmailUseCase(): GetWAWUserWithEmailUseCase {
            return GetWAWUserWithEmailUseCaseImpl(provideDataRepository())
        }

        fun provideReserveDeskUseCase(): ReserveDeskUseCase {
            return ReserveDeskUseCaseImpl(provideDataRepository())
        }

        fun provideGetFaqUseCase(): GetFaqUseCase {
            return GetFaqUseCaseImpl(provideDataRepository())
        }

        fun provideCleanWAWUserUseCase(): CleanWAWUserUseCase {
            return CleanWAWUserUseCaseImpl(provideDataRepository())
        }

        fun provideTakeOffUseCase(): TakeOffUseCase {
            return TakeOffUseCaseImpl()
        }

        fun provideIsAppEligibleUseCase(): IsAppEligibleUseCase {
            return IsAppEligibleUseCaseImpl(provideDataRepository())
        }

        fun provideCacheRepository(): CacheRepository {
            return CacheRepositoryImpl
        }

        fun provideGetDeviceRegistrationTokenUseCase(): GetDeviceRegistrationTokenUseCase {
            return GetDeviceRegistrationTokenUseCaseImpl(provideDataRepository())
        }

        fun provideSetDeviceRegistrationTokenUseCase(): SetDeviceRegistrationTokenUseCase {
            return SetDeviceRegistrationTokenUseCaseImpl(provideDataRepository())
        }

        fun provideWAWApi(): WAWApi {
            return Retrofit.Builder()
                    .baseUrl(WAWApi.ENDPOINT)
                    .addCallAdapterFactory(CoroutineCallAdapterFactory())
                    .build()
                    .create(WAWApi::class.java)
        }

        fun provideGetAlertsUseCase(): GetAlertsUseCase {
            return GetAlertsUseCaseImpl(provideDataRepository())
        }

        fun provideSubscribeAlertsUserCase(): SubscribeAlertsUseCase {
            return SubscribeAlertsUseCaseImpl(provideDataRepository())
        }

        fun provideSubscribeWorkspacesMetadataUseCase(): SubscribeWorkspacesMetadataUseCase {
            return SubscribeWorkspacesMetadataUseCaseImpl(provideDataRepository())
        }
    }
}

