package se.fabege.app.waw.ui.fragment.selectworkspace

import se.fabege.app.waw.ui.BasePresenter
import se.fabege.app.waw.ui.BaseView
import se.fabege.app.waw.ui.recyclerview.adapter.Item
import se.fabege.app.waw.ui.recyclerview.adapter.delegates.WorkspaceAdapterDelegate

interface SelectWorkspaceContract {
    interface View: BaseView<Presenter> {
        fun showWorkspaces(items: List<Item>)
        fun showReserveDesk(workspaceId: String, deskId: String)
        fun showFaq()
        fun setKeyButtonEnabled(enabled: Boolean)
    }

    interface Presenter: BasePresenter, WorkspaceAdapterDelegate.ClickListener {
        fun onKeyButtonClicked()
        fun onFaqButtonClicked()
    }
}