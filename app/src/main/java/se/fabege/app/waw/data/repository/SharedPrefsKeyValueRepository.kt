package se.fabege.app.waw.data.repository

import android.content.Context
import android.content.SharedPreferences
import se.fabege.app.waw.domain.repository.KeyValueRepository

class SharedPrefsKeyValueRepository(context: Context, name: String) : KeyValueRepository {

    private val sharedPref: SharedPreferences = context.getSharedPreferences(name, Context.MODE_PRIVATE)

    override fun putBoolean(key: String, value: Boolean) {
        with(sharedPref.edit()) {
            putBoolean(key, value)
            commit()
        }
    }

    override fun getBoolean(key: String, defaultValue: Boolean): Boolean {
        return sharedPref.getBoolean(key, defaultValue)
    }

    override fun putString(key: String, value: String) {
        with(sharedPref.edit()) {
            putString(key, value)
            commit()
        }
    }

    override fun getString(key: String): String? {
        return sharedPref.getString(key, null)
    }

    override fun putLong(key: String, value: Long) {
        with(sharedPref.edit()) {
            putLong(key, value)
            commit()
        }
    }

    override fun getLong(key: String): Long {
        return sharedPref.getLong(key, 0)
    }
}