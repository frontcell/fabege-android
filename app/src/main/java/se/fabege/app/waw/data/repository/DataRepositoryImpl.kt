package se.fabege.app.waw.data.repository

import android.util.Log
import com.google.firebase.firestore.ListenerRegistration
import kotlinx.coroutines.tasks.await
import se.fabege.app.waw.data.datasource.FirebaseFirestoreRemoteDataSource
import se.fabege.app.waw.data.Result
import se.fabege.app.waw.data.datasource.FirebaseInstanceIdRemoteDataSource
import se.fabege.app.waw.data.error.DataRepositoryException
import se.fabege.app.waw.data.error.DocumentNotFoundException
import se.fabege.app.waw.data.firebase.Day
import se.fabege.app.waw.data.firebase.FaqSection
import se.fabege.app.waw.data.firebase.Property
import se.fabege.app.waw.data.firebase.User
import se.fabege.app.waw.domain.model.*
import se.fabege.app.waw.domain.repository.DataRepository
import se.fabege.app.waw.misc.*
import java.lang.Exception

class DataRepositoryImpl private constructor(remoteDataSources: Pair<FirebaseFirestoreRemoteDataSource, FirebaseInstanceIdRemoteDataSource>) : DataRepository {

    companion object : SingletonHolder<DataRepositoryImpl, Pair<FirebaseFirestoreRemoteDataSource, FirebaseInstanceIdRemoteDataSource>>(::DataRepositoryImpl) {
        private val TAG = "Fabege WAW " + DataRepositoryImpl::class.java.simpleName
    }

    private val firestoreRemote: FirebaseFirestoreRemoteDataSource = remoteDataSources.first
    private val instanceIdRemote: FirebaseInstanceIdRemoteDataSource = remoteDataSources.second

    private var subscriptions = mutableListOf<ListenerRegistration>()

    override suspend fun getUser(userId: String): Result<WAWUser> {
        val task = firestoreRemote.getUser(userId)
        try {
            task.await()
        } finally {
            if (task.isSuccessful) {
                return if (task.result!!.exists()) {
                    val document = task.result!!
                    val user = document.toObject(User::class.java)
                    user?.let {
                        user.id = document.id
                    }
                    if (user != null) {
                        Result.Success(user.toWAWUser())
                    } else {
                        Result.Error(DataRepositoryException())
                    }
                } else {
                    Result.Error(DocumentNotFoundException())
                }
            } else {
                Log.e(TAG, task.exception.toString())
                return Result.Error(DataRepositoryException())
            }
        }
    }

    override suspend fun getUsers(email: String): Result<List<WAWUser>> {
        val task = firestoreRemote.getUsers(email)
        try {
            task.await()
        } finally {
            if (task.isSuccessful) {
                return if (task.result!!.isEmpty) {
                    Result.Success(emptyList())
                } else {
                    val result = mutableListOf<WAWUser>()
                    for (document in task.result!!.documents) {
                        val user = document.toObject(User::class.java)
                        user?.let {
                            user.id = document.id
                            result.add(user.toWAWUser())

                        }
                    }
                    Result.Success(result)
                }
            } else {
                Log.e(TAG, task.exception.toString())
                return Result.Error(DataRepositoryException())
            }
        }
    }

    override suspend fun getWorkspaces(): Result<List<Workspace>> {
        val task = firestoreRemote.getProperties()
        try {
            task.await()
        } finally {
            if (task.isSuccessful) {
                return if (task.result!!.isEmpty) {
                    Result.Success(emptyList())
                } else {
                    val result = mutableListOf<Workspace>()
                    for (document in task.result!!.documents) {
                        val property = document.toObject(Property::class.java) ?: continue
                        if (property.visible == false) continue
                        val workspaceMetadata = property.toWorkspaceMetadata()
                        result.add(Workspace(document.id, workspaceMetadata, getCalendar(document.id, workspaceMetadata)))
                    }
                    Result.Success(result)
                }
            } else {
                Log.e(TAG, task.exception.toString())
                return Result.Error(DataRepositoryException())
            }
        }
    }

    override suspend fun getWorkSpace(workspaceId: String): Result<Workspace> {
        val task = firestoreRemote.getProperty(workspaceId)
        try {
            task.await()
        } finally {
            return if (task.isSuccessful) {
                if (task.result!!.exists()) {
                    val document = task.result!!
                    val property = document.toObject(Property::class.java)
                    val workspaceMetaData = property!!.toWorkspaceMetadata()
                    Result.Success(Workspace(document.id, workspaceMetaData, getCalendar(document.id, workspaceMetaData)))
                } else {
                    Result.Error(DocumentNotFoundException())
                }
            } else {
                Log.e(TAG, task.exception.toString())
                Result.Error(DataRepositoryException())
            }
        }
    }

    private suspend fun getCalendar(id: String, workspaceMetadata: WorkspaceMetadata): WorkspaceCalendar? {
        val task = firestoreRemote.getSchedule(id)
        try {
            task.await()
        } finally {
            if (task.isSuccessful) {
                return if (task.result!!.isEmpty) {
                    null
                } else {
                    val days = mutableListOf<Day>()
                    for (document in task.result!!.documents) {
                        var day: Day? = null
                        try {
                            day = document.toObject(Day::class.java)
                        } catch (ignore: Exception) {

                        }
                        day?.let {
                            day.id = document.id
                            days.add(day)
                        }
                    }
                    WorkspaceCalendar(id, days.map { it.toCalendarDay(id, workspaceMetadata.timeSlotInfo) }, workspaceMetadata.deskInfo, workspaceMetadata.timeSlotInfo)
                }
            } else {
                Log.e(TAG, task.exception.toString())
                return null
            }
        }
    }

    override suspend fun cleanUser(userId: String): Result<Unit> {
        val task = firestoreRemote.applyToUserClean(userId)
        try {
            task.await()
        } finally {
            return if (task.isSuccessful) {
                Result.Success(Unit)
            } else {
                Log.e(TAG, task.exception.toString())
                Result.Error(DataRepositoryException())
            }
        }
    }

    override suspend fun handleReservations(userId: String, toAdd: List<Reservation>, toCancel: List<Reservation>): Result<Unit> {
        val task = firestoreRemote.applyToSchedule(userId, toAdd.map { it.id }, toCancel.map { it.id })
        try {
            task.await()
        } finally {
            return if (task.isSuccessful) {
                Result.Success(Unit)
            } else {
                Log.e(TAG, task.exception.toString())
                Result.Error(DataRepositoryException())
            }
        }
    }

    override suspend fun getFaq(): Result<Faq> {
        val task = firestoreRemote.getFaqs()
        try {
            task.await()
        } finally {
            if (task.isSuccessful) {
                if (task.result!!.isEmpty) {
                    return Result.Success(Faq(emptyList()))
                } else {
                    val list = mutableListOf<FaqSection>()

                    for (document in task.result!!.documents) {
                        val faqSection = document.toObject(FaqSection::class.java)
                        if (faqSection != null) {
                            list.add(faqSection)
                        }
                    }

                    return Result.Success(Faq(list.map {
                        se.fabege.app.waw.domain.model.FaqSection(it.name
                                ?: "", it.questions!!.map { question ->
                            FaqQuestion(question.title ?: "", question.text ?: "")
                        })
                    }))
                }
            } else {
                Log.e(TAG, task.exception.toString())
                return Result.Error(DataRepositoryException())
            }
        }
    }

    override suspend fun getAppConfig(): Result<AppConfig> {
        val task = firestoreRemote.getAppConfig()
        try {
            task.await()
        } finally {
            return if (task.isSuccessful) {
                if (task.result!!.exists()) {
                    val document = task.result!!
                    val app = document.toObject(se.fabege.app.waw.data.firebase.AppConfig::class.java)!!
                    Result.Success(app.toAppConfig())
                } else {
                    Result.Error(DocumentNotFoundException())
                }
            } else {
                Log.e(TAG, task.exception.toString())
                Result.Error(DataRepositoryException())
            }
        }
    }

    override suspend fun getDeviceRegistrationToken(): Result<String> {
        val task = instanceIdRemote.getInstanceId()
        try {
            task.await()
        } finally {
            return if (task.isSuccessful) {
                Result.Success(task.result!!.token)
            } else {
                Log.e(TAG, task.exception.toString())
                Result.Error(DataRepositoryException())
            }
        }
    }

    override suspend fun setDeviceRegistrationToken(userId: String, token: String): Result<Unit> {
        val task = firestoreRemote.applyToUser(userId, token)
        try {
            task.await()
        } finally {
            return if (task.isSuccessful) {
                Result.Success(Unit)
            } else {
                Log.e(TAG, task.exception.toString())
                Result.Error(DataRepositoryException())
            }
        }
    }

    override suspend fun getAlerts(): Result<List<Alert>> {
        val task = firestoreRemote.getAlerts()
        try {
            task.await()
        } finally {
            if (task.isSuccessful) {
                return if (task.result!!.isEmpty) {
                    Result.Success(emptyList())
                } else {
                    val result = mutableListOf<Alert>()
                    for (document in task.result!!.documents) {
                        val alert = document.toObject(se.fabege.app.waw.data.firebase.Alert::class.java)
                                ?: continue
                        // set document.id as alert.id if id not set
                        alert.id = alert.id ?: document.id
                        result.add(alert.toAlert())

                        result.sortByDescending {
                            it.publishAt
                        }
                    }
                    Result.Success(result)
                }
            } else {
                Log.e(TAG, task.exception.toString())
                return Result.Error(DataRepositoryException())
            }
        }
    }


    override fun subscribeWorkspaceCalendar(workspaceId: String, callback: DataRepository.Callback) {
        subscriptions.add(firestoreRemote.subscribeOnSchedule(workspaceId, callback))
    }

    override fun subscribeAlerts(callback: DataRepository.AlertsCallback) {
        subscriptions.add(firestoreRemote.subscribeOnAlerts(callback))
    }

    override fun subscribeWorkspacesMetadata(callback: DataRepository.MetadataCallback) {
        subscriptions.add(firestoreRemote.subscribeOnWorkspacesMetadata(callback))
    }

    override fun subscribeUser(userId: String, callback: DataRepository.UserCallback) {
        subscriptions.add(firestoreRemote.subscribeOnUser(userId, callback))
    }

    override fun subscribeAppConfig(callback: DataRepository.AppConfigCallback) {
        subscriptions.add(firestoreRemote.subscribeOnAppConfig(callback))
    }

    override fun unsubscribe() {
        for (subscription in subscriptions) {
            subscription.remove()
        }
        subscriptions.clear()
    }


}