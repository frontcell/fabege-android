package se.fabege.app.waw.data

import android.content.Context
import se.fabege.app.waw.data.repository.SharedPrefsKeyValueRepository
import se.fabege.app.waw.domain.model.KeyValueRepositoryFactory
import se.fabege.app.waw.domain.repository.KeyValueRepository

class KeyValueRepositoryFactoryImpl(private val context: Context) : KeyValueRepositoryFactory {

    override fun get(name: String): KeyValueRepository {
        return SharedPrefsKeyValueRepository(context, "${context.packageName}.$name")
    }
}