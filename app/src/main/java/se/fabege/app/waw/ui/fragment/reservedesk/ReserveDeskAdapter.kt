package se.fabege.app.waw.ui.fragment.reservedesk

import android.support.v7.util.DiffUtil
import kotlinx.coroutines.newFixedThreadPoolContext
import se.fabege.app.waw.ui.recyclerview.adapter.AdapterDelegateManager
import se.fabege.app.waw.ui.recyclerview.adapter.Item
import se.fabege.app.waw.ui.recyclerview.adapter.ListAdapter
import se.fabege.app.waw.ui.recyclerview.adapter.items.*

class ReserveDeskAdapter(manager: AdapterDelegateManager<List<Item>>, items: List<Item>) : ListAdapter<List<Item>>(items) {
    init {
        this.manager = manager
    }

    fun updateItems(items: List<Item>) {
        val diffResult = DiffUtil.calculateDiff(DiffCallback(items, this.items))
        this.items = items
        diffResult.dispatchUpdatesTo(this)
    }


    class DiffCallback(val newItems: List<Item>, val oldItems: List<Item>) : DiffUtil.Callback() {

        override fun getOldListSize(): Int {
            return oldItems.size
        }

        override fun getNewListSize(): Int {
            return newItems.size
        }

        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            return oldItems[oldItemPosition]::class == newItems[newItemPosition]::class
        }

        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            val obj = oldItems[oldItemPosition]
            return when (obj) {
                is CalendarHeaderItem -> obj.isContentSame(newItems[newItemPosition] as CalendarHeaderItem)
                is CalendarFooterItem -> obj.isContentSame(newItems[newItemPosition] as CalendarFooterItem)
                is WorkspaceMetaDataAboutItem -> obj.isContentSame(newItems[newItemPosition] as WorkspaceMetaDataAboutItem)
                is WorkspaceMetaDataFacilityItem -> obj.isContentSame(newItems[newItemPosition] as WorkspaceMetaDataFacilityItem)
                is WorkspaceMetaDataCapacityItem -> obj.isContentSame(newItems[newItemPosition] as WorkspaceMetaDataCapacityItem)
                is WorkspaceMetaDataExtraItem -> obj.isContentSame(newItems[newItemPosition] as WorkspaceMetaDataExtraItem)
                is WorkspaceMetaDataAddressItem -> obj.isContentSame(newItems[newItemPosition] as WorkspaceMetaDataAddressItem)
                is WorkspaceMetaDataContactItem -> obj.isContentSame(newItems[newItemPosition] as WorkspaceMetaDataContactItem)
                else -> false
            }
        }
    }
}
