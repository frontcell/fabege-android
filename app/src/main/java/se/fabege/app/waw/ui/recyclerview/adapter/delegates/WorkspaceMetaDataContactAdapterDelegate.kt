package se.fabege.app.waw.ui.recyclerview.adapter.delegates

import android.content.Context
import android.graphics.Color
import android.telephony.PhoneNumberUtils
import android.text.Spannable
import android.text.SpannableString
import android.view.LayoutInflater
import android.view.ViewGroup
import com.squareup.picasso.Picasso
import se.fabege.app.R
import se.fabege.app.waw.ui.recyclerview.adapter.Item
import se.fabege.app.waw.ui.recyclerview.adapter.ListAdapterDelegate
import se.fabege.app.waw.ui.recyclerview.adapter.items.WorkspaceMetaDataContactItem
import se.fabege.app.waw.ui.widget.ColorUnderlineSpan

class WorkspaceMetaDataContactAdapterDelegate(private val context: Context, private val clickListener: ClickListener) : ListAdapterDelegate<WorkspaceMetaDataContactItem, Item, WorkspaceMetaDataContactItem.ViewHolder>() {
    override fun isForViewType(item: Item, items: List<Item>, pos: Int): Boolean {
        return item is WorkspaceMetaDataContactItem
    }

    override fun onCreateViewHolder(parent: ViewGroup): WorkspaceMetaDataContactItem.ViewHolder {
        return WorkspaceMetaDataContactItem.ViewHolder(LayoutInflater.from(context).inflate(R.layout.waw_rv_item_workspace_metadata_contact, parent, false))
    }

    override fun onBindViewHolder(item: WorkspaceMetaDataContactItem, viewHolder: WorkspaceMetaDataContactItem.ViewHolder) {
        viewHolder.nameTextView.text = item.viewModel.contactInfo.name
        viewHolder.titleTextView.text = item.viewModel.contactInfo.title

        val emailSpannable = SpannableString(item.viewModel.contactInfo.email)
        emailSpannable.setSpan(ColorUnderlineSpan(Color.parseColor("#8A2082"), 0, item.viewModel.contactInfo.email.length), 0, item.viewModel.contactInfo.email.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        viewHolder.emailTextView.text = emailSpannable

        var phoneNumber = item.viewModel.contactInfo.phoneNumber
        if (!phoneNumber.startsWith("+")) {
            phoneNumber = "+$phoneNumber"
        }
        val formattedPhoneNumber = PhoneNumberUtils.formatNumber(phoneNumber, "se")
        if (formattedPhoneNumber != null) {
            val phoneNumberSpannable = SpannableString(formattedPhoneNumber)
            phoneNumberSpannable.setSpan(ColorUnderlineSpan(Color.parseColor("#8A2082"), 0, formattedPhoneNumber.length), 0, formattedPhoneNumber.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
            viewHolder.phoneNumberTextView.text = phoneNumberSpannable
        }


        if (item.viewModel.contactInfo.imageUrl.isNotEmpty()) {
            Picasso.get().load(item.viewModel.contactInfo.imageUrl).into(viewHolder.contactImageView)
        }

        viewHolder.emailTextView.setOnClickListener {
            clickListener.onEmailClicked()
        }

        viewHolder.phoneNumberTextView.setOnClickListener {
            clickListener.onPhoneNumberClicked()
        }
    }

    interface ClickListener {
        fun onEmailClicked()
        fun onPhoneNumberClicked()
    }
}