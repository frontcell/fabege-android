package se.fabege.app.waw.domain.usecase

import se.fabege.app.waw.domain.model.KeyValueRepositoryFactory
import se.fabege.app.waw.data.Result
class SetUserOnboardedUseCaseImpl(private val getUser: GetUserUseCase, private val keyValueRepositoryFactory: KeyValueRepositoryFactory): SetUserOnboardedUseCase() {

    override fun invoke() {
        val result = getUser()
        if (result is Result.Success) {
            val keyValueRepository = keyValueRepositoryFactory.get(result.data.uid)
            keyValueRepository.putBoolean("onboarded", true)
        }
    }
}