package se.fabege.app.waw.domain.model

data class Alert(val id: String, val title: String, val message: String, val workspaceId: String, val publishAt: Long, val unpublishAt: Long, val type: String)