package se.fabege.app.waw.ui.fragment.selectworkspace

import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import se.fabege.app.R
import se.fabege.app.base.GlobalState
import se.fabege.app.waw.data.GenericErrorMessage
import se.fabege.app.waw.data.Result
import se.fabege.app.waw.data.TextMessage
import se.fabege.app.waw.device.Device
import se.fabege.app.waw.domain.model.Alert
import se.fabege.app.waw.domain.model.Workspace
import se.fabege.app.waw.domain.repository.DataRepository
import se.fabege.app.waw.misc.Injection
import se.fabege.app.waw.misc.Utils
import se.fabege.app.waw.ui.BasePresenterImpl
import se.fabege.app.waw.ui.activity.waw.WAWActivity
import se.fabege.app.waw.ui.fragment.reservedesk.CalendarManager
import se.fabege.app.waw.ui.recyclerview.adapter.items.WorkspaceItem

class SelectWorkspacePresenter(private val view: SelectWorkspaceContract.View, device: Device) : BasePresenterImpl(view, device), SelectWorkspaceContract.Presenter, DataRepository.UserCallback {

    private val cacheRepository = Injection.provideCacheRepository()
    private val getWorkspaces = Injection.provideGetWorkspacesUseCase()
    private val getAlerts = Injection.provideGetAlertsUseCase()
    private val cleanUser = Injection.provideCleanWAWUserUseCase()

    private var workspaces: List<Workspace> = emptyList()
    private var alerts: List<Alert> = emptyList()

    init {
        view.setPresenter(this)
    }

    override fun onPreResume() {
        super.onPreResume()
        if (cacheRepository.hasWAWUser() && cacheRepository.hasWorkspaces()) {
            wawUser = cacheRepository.getWAWUser()
            workspaces = cacheRepository.getWorkspaces()
            view.showWorkspaces(workspaces.map { workspace ->
                createWorkspaceItem(workspace)
            }.sortedBy { it.viewModel.name })
            view.setKeyButtonEnabled(enableKeyButton())
        } else {
            view.showBusyDialog()
        }
    }

    override fun onResume() {
        device.trackPageView("WAW_list_properties")

        scope.launch(dispatcherProvider.computation) {
            val workspacesResult = getWorkspaces()
            val alertsResult = getAlerts()
            withContext(dispatcherProvider.ui) {
                view.dismissBusyDialog()
            }
            when (alertsResult) {
                is Result.Success -> alerts = alertsResult.data
            }
            when (workspacesResult) {
                is Result.Success -> {
                    workspaces = workspacesResult.data
                    showWorkspaces()
                }
                is Result.Error -> {
                    view.showMessage(GenericErrorMessage())
                }
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        Injection.provideCacheRepository().clear()
    }

    override fun onAlertsChanged(alerts: List<Alert>) {
        this.alerts = alerts
        showWorkspaces()
    }

    // WorkspaceAdapterDelegate.ClickListener
    override fun onWorkspaceClicked(id: String) {
        device.trackPageView(id)
        view.showReserveDesk(id, "ignore")
    }

    override fun onKeyButtonClicked() {
        if (device.isParakeyAppInstalled()) {
            device.trackEvent("WAW_Open_door")
            device.openParakeyApp(wawUser!!.email, wawUser!!.generatePassword(), WAWActivity.OPEN_PARAKEY_APP_REQUEST_CODE)
        } else {
            view.showMessage(TextMessage(GlobalState.getContext().getString(R.string.waw_dialog_generic_error_title), GlobalState.getContext().getString(R.string.waw_dialog_parakey_app_not_found_message)))

        }
    }

    override fun onFaqButtonClicked() {
        device.trackPageView("WAW_FAQ")
        view.showFaq()
    }

    private fun createWorkspaceItem(workspace: Workspace): WorkspaceItem {
        val calendarManager = CalendarManager(wawUser, workspace.calendar)
        val keys = calendarManager.getReservationsNowAndAfter()
        return WorkspaceItem(workspace.id, WorkspaceItem.ViewModel(workspace.metaData.about.name, workspace.metaData.address.address, workspace.metaData.media.iconUrl, keys.isNotEmpty(), keys.size, haveAlerts(workspace.id)))
    }

    private fun haveAlerts(workspaceId: String): Boolean {
        return alerts.any { alert -> alert.workspaceId == workspaceId && Utils.getNowInMillis() >= alert.publishAt && Utils.getNowInMillis() < alert.unpublishAt }
    }

    private fun showWorkspaces() {
        scope.launch(dispatcherProvider.ui) {
            view.showWorkspaces(workspaces.map { workspace ->
                createWorkspaceItem(workspace)
            }.sortedBy { it.viewModel.name })
            view.setKeyButtonEnabled(enableKeyButton())
        }
    }

    private fun enableKeyButton(): Boolean {
        for (workspace in workspaces) {
            val calendarManager = CalendarManager(wawUser, workspace.calendar)
            if (calendarManager.getReservationsNow().isNotEmpty()) {
                return true
            }
        }
        return false
    }

}