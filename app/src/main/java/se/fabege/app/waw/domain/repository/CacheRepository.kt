package se.fabege.app.waw.domain.repository

import se.fabege.app.waw.domain.model.WAWUser
import se.fabege.app.waw.domain.model.Workspace

interface CacheRepository {
    fun hasWorkspaces(): Boolean
    fun getWorkspaces(): List<Workspace>
    fun putWorkspaces(workspaces: List<Workspace>)

    fun hasWAWUser(): Boolean
    fun getWAWUser(): WAWUser
    fun putWAWUser(wawUser: WAWUser)

    fun clear()
}