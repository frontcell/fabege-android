package se.fabege.app.waw.ui.widget

import android.content.Context
import android.graphics.*
import android.graphics.Paint.ANTI_ALIAS_FLAG
import android.util.AttributeSet
import android.view.View


class CircleView : View {

    private val mCutPaint = Paint(ANTI_ALIAS_FLAG)
    private var mBitmap: Bitmap? = null
    private var mInternalCanvas: Canvas? = null

    constructor(context: Context) : super(context) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle) {
        init()
    }

    private fun init() {
        mCutPaint.xfermode = PorterDuffXfermode(PorterDuff.Mode.CLEAR)
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)

        if (mInternalCanvas != null) {
            mInternalCanvas!!.setBitmap(null)
            mInternalCanvas = null
        }

        if (mBitmap != null) {
            mBitmap!!.recycle()
            mBitmap = null
        }

        mBitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888)
        mInternalCanvas = Canvas(mBitmap)
    }

    override fun onDraw(canvas: Canvas) {
        if (mInternalCanvas == null || mBitmap == null) {
            return
        }

        val width = width
        val height = height

        // make the radius as large as possible within the view bounds
        val radius = Math.min(width, height) / 2

        mInternalCanvas!!.drawColor( Color.parseColor("#FBFBFA"))
        mInternalCanvas!!.drawCircle((width / 2).toFloat(), (height / 2).toFloat(), radius.toFloat(), mCutPaint)

        canvas.drawBitmap(mBitmap, 0f, 0f, null)
    }
}