package se.fabege.app.waw.ui.fragment.loginactivateparakey

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import se.fabege.app.R
import se.fabege.app.waw.device.Device
import se.fabege.app.waw.misc.Injection
import se.fabege.app.waw.ui.activity.waw.WAWActivity
import se.fabege.app.waw.ui.fragment.ToolbarFragment


class LogInActivateParakeyFragment : ToolbarFragment() {

    companion object {
        fun newInstance(email: String, password: String): LogInActivateParakeyFragment {
            val args = Bundle()
            args.putString("email", email)
            args.putString("password", password)
            val fragment = LogInActivateParakeyFragment()
            fragment.arguments = args
            return fragment
        }
    }

    private var callback: Events? = null

    private lateinit var device: Device

    init {
        xmlLayoutId = R.layout.waw_fragment_login_activate_parakey
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        callback = context as? Events
        if (callback == null) {
            throw ClassCastException("$context must implement LogInActivateParakeyFragment.Events")
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        device = Injection.provideDevice(activity!!)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = super.onCreateView(inflater, container, savedInstanceState)
        val sendButton = view?.findViewById<View>(R.id.button2)
        sendButton?.setOnClickListener {
            device.openParakeyApp(arguments!!.getString("email")!!, arguments!!.getString("password")!!, WAWActivity.ACTIVATE_PARAKEY_APP_REQUEST_CODE)
        }
        return view
    }

    interface Events {
        fun onParakeyAppActivated()
    }
}