package se.fabege.app.waw.domain.usecase.mock

import se.fabege.app.waw.data.Result
import se.fabege.app.waw.domain.model.AuthUser
import se.fabege.app.waw.domain.usecase.GetUserUseCase

class MockGetUserUseCase : GetUserUseCase() {

    override operator fun invoke(): Result<AuthUser> {
        return Result.Success(AuthUser("NZLG961ttbVrF3cBjShA6FMAnhv2"))
    }
}