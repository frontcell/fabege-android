package se.fabege.app.waw.ui.recyclerview.adapter.items

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.TextView
import se.fabege.app.R
import se.fabege.app.waw.domain.model.WorkspaceMetadata
import se.fabege.app.waw.ui.recyclerview.adapter.Item
import se.fabege.app.waw.ui.widget.PieChartView

data class WorkspaceMetaDataCapacityItem(val viewModel: ViewModel) : Item {

    data class ViewModel(val capacityInfo: WorkspaceMetadata.CapacityInfo)

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val pieChartView = itemView.findViewById<PieChartView>(R.id.pieChartView)
        val desksTextView = itemView.findViewById<TextView>(R.id.desksTextView)
        val conferenceRoomsTextView = itemView.findViewById<TextView>(R.id.conferenceRoomsTextView)
        val phoneRoomsTextView = itemView.findViewById<TextView>(R.id.phoneRoomsTextView)
    }

    fun isContentSame(other: WorkspaceMetaDataCapacityItem): Boolean {
        return this.viewModel.capacityInfo.desks == other.viewModel.capacityInfo.desks &&
                this.viewModel.capacityInfo.conferenceRooms == other.viewModel.capacityInfo.conferenceRooms &&
                this.viewModel.capacityInfo.phoneRooms == other.viewModel.capacityInfo.phoneRooms
    }
}