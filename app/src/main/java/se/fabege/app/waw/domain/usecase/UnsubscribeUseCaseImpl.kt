package se.fabege.app.waw.domain.usecase

import se.fabege.app.waw.domain.repository.DataRepository

class UnsubscribeUseCaseImpl(private val dataRepository: DataRepository): UnsubscribeUseCase() {

    override fun invoke() {
        dataRepository.unsubscribe()
    }
}