package se.fabege.app.waw.domain.model

data class Workspace(val id: String, val metaData: WorkspaceMetadata, val calendar: WorkspaceCalendar?)