package se.fabege.app.waw.misc

import se.fabege.app.waw.data.Result
import se.fabege.app.waw.data.firebase.*
import se.fabege.app.waw.data.firebase.Alert
import se.fabege.app.waw.data.firebase.AppConfig
import se.fabege.app.waw.domain.model.*
import se.fabege.app.waw.ui.recyclerview.adapter.items.CalendarItem
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

val <T> T.exhaustive: T
    get() = this

fun AppConfig.toAppConfig(): se.fabege.app.waw.domain.model.AppConfig {
    return se.fabege.app.waw.domain.model.AppConfig(minAllowedVersion)
}

fun Alert.toAlert(): se.fabege.app.waw.domain.model.Alert {
    return se.fabege.app.waw.domain.model.Alert(id!!, title ?: "", message ?: "",
            propertyId ?: "",
            publishAt?.toMillisSeconds(0L) ?: 0L,
            unpublishAt?.toMillisSeconds(Long.MAX_VALUE) ?: Long.MAX_VALUE,
            "standard")
}

fun User.toWAWUser(): WAWUser {
    return WAWUser(id ?: "", email ?: "", accessKeys?.map {
        SuccessKey(it.id ?: "", it.path ?: "")
    } ?: emptyList(), failedAccessKeys?.map { ErrorKey(it.errorMessage ?: "", it.path ?: "") }
            ?: emptyList(), deviceToken ?: "")
}

fun Property.toWorkspaceMetadata(): WorkspaceMetadata {
    return WorkspaceMetadata(
            mapAbout(this),
            mapAddress(this),
            mapMedia(this),
            mapContactInfo(contact),
            WorkspaceMetadata.DeskInfo(capacity?.seats ?: 0, (capacity?.seats
                    ?: 0) - (capacity?.dropIns ?: 0), capacity?.dropIns ?: 0),
            WorkspaceMetadata.TimeSlotInfo(openingHours?.map { if (it.start != null && it.end != null) "${it.start}-${it.end}" else "0:00-23:59" }
                    ?: emptyList()),
            mapFacilityInfo(facilities),
            mapCapacityInfo(capacity)
    )
}

private fun mapAbout(property: Property): WorkspaceMetadata.About {
    return WorkspaceMetadata.About(property.name ?: "", property.desc ?: "", property.goodToKnow
            ?: "")
}

private fun mapAddress(property: Property): WorkspaceMetadata.Address {
    return WorkspaceMetadata.Address(property.address ?: "", property.coordinates?.lat
            ?: 59.3293f, property.coordinates?.lon ?: 18.0686f)
}

private fun mapMedia(property: Property): WorkspaceMetadata.Media {
    return WorkspaceMetadata.Media(property.iconUrl ?: "", property.imageUrl ?: "")
}

private fun mapContactInfo(contact: Contact?): WorkspaceMetadata.ContactInfo {
    return WorkspaceMetadata.ContactInfo(contact?.name ?: "", contact?.title ?: "", contact?.email
            ?: "", contact?.tel ?: "", contact?.imageUrl ?: "")
}

private fun mapFacilityInfo(facilities: Facilities?): WorkspaceMetadata.FacilityInfo {
    if (facilities == null) {
        return WorkspaceMetadata.FacilityInfo(emptyList())
    }
    val list = mutableListOf<WorkspaceMetadata.Facility>()

    facilities.wifi?.let {
        list.add(WorkspaceMetadata.WiFi(it.info ?: "", it.username ?: "", it.password ?: ""))
    }

    facilities.printers?.let {
        list.add(WorkspaceMetadata.Printer(it))
    }

    facilities.coffee?.let {
        list.add(WorkspaceMetadata.Coffee(it))
    }

    facilities.conference?.let {
        list.add(WorkspaceMetadata.Conference(it))
    }

    return WorkspaceMetadata.FacilityInfo(list)
}

private fun mapCapacityInfo(capacity: Capacity?): WorkspaceMetadata.CapacityInfo {
    if (capacity == null) {
        return WorkspaceMetadata.CapacityInfo(0, 0, 0)
    }
    return WorkspaceMetadata.CapacityInfo(capacity.seats, capacity.phoneRooms, capacity.conferenceRooms)
}

fun Day.toCalendarDay(parentId: String, timeSlotInfo: WorkspaceMetadata.TimeSlotInfo): CalendarDay {
    return CalendarDay("$parentId/$id", Utils.parseDate(id
            ?: throw IllegalStateException("id cannot be null")), slots?.toTimeSlots("$parentId/$id", timeSlotInfo)
            ?: emptyList())
}

fun List<Slot>.toTimeSlots(parentId: String, timeSlotInfo: WorkspaceMetadata.TimeSlotInfo): List<TimeSlot> {
    val paths = parentId.split("/")
    val day = paths[paths.lastIndex]
    val times: Array<Array<Long>> = Array(timeSlotInfo.spans.size) { timeSlotIndex ->
        Array(2) { index ->
            val spanPaths = timeSlotInfo.spans[timeSlotIndex].split("-")

            Utils.parseDateTime(day + "T" + spanPaths[index]).time
        }
    }
    return this.mapIndexed { index, it -> it.toTimeSlot("$parentId/$index/${this.size}", times[if (index > times.lastIndex) times.lastIndex else index][0], times[if (index > times.lastIndex) times.lastIndex else index][1]) }
}

fun Slot.toTimeSlot(id: String, start: Long, end: Long): TimeSlot {
    return TimeSlot(id, status?.toStatus() ?: Status.NA, ids?.toList() ?: emptyList(), start, end)
}

fun Key.toCalendar(): Calendar {
    val paths = timeSlotId.split("/")
    val date = SimpleDateFormat("yyyyMMdd").parse(paths[1])
    val cal = Calendar.getInstance()
    cal.time = date
    return cal
}

fun String.toStatus(): Status {
    return when (this.toLowerCase()) {
        "available" -> Status.OPEN
        "closed" -> Status.CLOSED_HOUR
        "holiday" -> Status.CLOSED_HOLIDAY
        else -> Status.NA
    }
}


fun Result<Boolean>.unwrap(): Result.UnwrappedBoolean {
    return when (this) {
        is Result.Success -> {
            if (this.data) {
                Result.UnwrappedBoolean.TRUE

            } else {
                Result.UnwrappedBoolean.FALSE
            }
        }
        is Result.Error -> Result.UnwrappedBoolean.ERROR
    }
}

fun Date.toWeekday(): String {
    val calendar = Calendar.getInstance()
    calendar.time = this
    when (calendar.get(Calendar.DAY_OF_WEEK)) {
        Calendar.MONDAY -> return "Mån"
        Calendar.TUESDAY -> return "Tis"
        Calendar.WEDNESDAY -> return "Ons"
        Calendar.THURSDAY -> return "Tors"
        Calendar.FRIDAY -> return "Fre"
        Calendar.SATURDAY -> return "Lör"
        Calendar.SUNDAY -> return "Sön"
        else -> throw IllegalStateException()
    }
}

fun Date.toHour(): String {
    return SimpleDateFormat("H").format(this)
}

fun Date.toDayMonth(): String {
    return SimpleDateFormat("dd/MM").format(this)
}

fun Date.toDayMonthHour(): String {
    return SimpleDateFormat("EEE d MMM 'kl.' H", Locale("sv", "SE")).format(this).replaceFirst(".", "")
}

fun CalendarItem.Decoration.toLegend(): String {
    return when (this) {
        CalendarItem.Decoration.NONE -> "N"
        CalendarItem.Decoration.OPEN -> "O"
        CalendarItem.Decoration.CLOSED -> "C"
        CalendarItem.Decoration.FULL -> "F"
        CalendarItem.Decoration.HOLIDAY -> "H"
        CalendarItem.Decoration.SELECTED -> "S"
        CalendarItem.Decoration.RESERVED -> "R"
    }
}

fun String.toMillisSeconds(default: Long): Long {
    var date: Date?
    try {
        val sdf = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
        date = sdf.parse(this)
    } catch (ex: ParseException) {
        date = null
    }

    return date?.time ?: default
}

fun Long.toDateTimeString(): String {
    if (this == 0L) {
        return ""
    }

    var date = Date(this)

    val sdf = SimpleDateFormat("dd MMMM yyyy, HH:mm", Locale("sv", "SE"))
    return sdf.format(date)
}
