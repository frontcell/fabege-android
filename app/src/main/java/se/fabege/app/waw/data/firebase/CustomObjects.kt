package se.fabege.app.waw.data.firebase

data class Property(val address: String? = null, val capacity: Capacity? = null, val contact: Contact? = null, val coordinates: Coordinates? = null, val desc: String? = null, val facilities: Facilities? = null, val goodToKnow: String? = null, val iconUrl: String? = null, val imageUrl: String? = null, val openingHours: MutableList<Hour>? = null, val name: String? = null, val visible: Boolean? = null)

data class Capacity(val conferenceRooms: Int = 0, val dropIns: Int = 0, val phoneRooms: Int = 0, val seats: Int = 0)

data class Contact(val email: String? = null, val imageUrl: String? = null, val name: String? = null, val tel: String? = null, val title: String? = null)

data class Coordinates(val lat: Float? = null, val lon: Float? = null)

data class Facilities(val coffee: String? = null, val conference: String? = null, val printers: String? = null, val wifi: WiFi? = null)

data class WiFi(val info: String? = null, val password: String? = null, val username: String? = null)

data class Hour(val end: String? = null, val start: String? = null)

data class Slot(val status: String? = null, val ids: MutableList<String>? = null)

data class User(var id: String? = null, val email: String? = null, val firstname: String? = null, val lastname: String? = null, val parakeyUserId: String? = null, val accessKeys: MutableList<AccessKey>? = null, val failedAccessKeys: MutableList<FailedAccessKey>? = null, val deviceToken: String? = null)

data class UserTransaction(val email: String? = null, val firstname: String? = null, val lastname: String? = null, val parakeyUserId: String? = null, val accessKeys: MutableList<AccessKey>? = null, val failedAccessKeys: MutableList<FailedAccessKey>? = null, var deviceToken: String? = null)

data class Day(var id: String? = null, val slots: MutableList<Slot>? = null)

data class DayTransaction(val slots: MutableList<Slot>? = null)

data class AccessKey(val id: String? = null, val path: String? = null)

data class FailedAccessKey(val errorMessage: String? = null, val path: String? = null)

data class FaqSection(val name: String? = null, val questions: MutableList<FaqQuestion>? = null)

data class FaqQuestion(val title: String? = null, val text: String? = null)

data class AppConfig(val minAllowedVersion: Int = 0)

data class Alert(var id: String? = null, val title: String? = null, val message: String? = null, val propertyId: String? = null, val publishAt: String? = null, val unpublishAt: String? = null, val type: String? = null)