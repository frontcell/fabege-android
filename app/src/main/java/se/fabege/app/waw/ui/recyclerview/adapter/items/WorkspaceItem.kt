package se.fabege.app.waw.ui.recyclerview.adapter.items

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import se.fabege.app.R
import se.fabege.app.waw.ui.recyclerview.adapter.Item

data class WorkspaceItem(val id: String, val viewModel: ViewModel) : Item {

    data class ViewModel(val name: String, val address: String, val iconUrl: String, val showBadge: Boolean, val badgeCount: Int, val showAlert: Boolean)

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val nameTextView: TextView = itemView.findViewById(R.id.nameTextView)
        val addressTextView: TextView = itemView.findViewById(R.id.addressTextView)
        val iconImageView: ImageView = itemView.findViewById(R.id.iconImageView)
        val badgeImageView: ImageView = itemView.findViewById(R.id.badgeImageView)
        val badgeCountTextView: TextView = itemView.findViewById(R.id.badgeCountTextView)
        val alertImageView: ImageView = itemView.findViewById(R.id.alertImageView)
    }
}
