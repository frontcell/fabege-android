package se.fabege.app.waw.ui.dialog

import android.app.Dialog
import android.graphics.Color
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v7.app.AlertDialog
import android.text.Spannable
import android.text.SpannableString
import android.view.LayoutInflater
import android.view.View
import android.widget.TextView
import se.fabege.app.R
import se.fabege.app.waw.data.ReservationConfirmationMessage
import se.fabege.app.waw.ui.widget.ColorUnderlineSpan
import java.lang.IllegalStateException

class ReservationConfirmationMessageDialog : DialogFragment() {
    lateinit var reservationConfirmationMessage: ReservationConfirmationMessage

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return activity?.let {
            val builder = AlertDialog.Builder(it)
            val view = LayoutInflater.from(it).inflate(R.layout.waw_dialog_reservation_confirmation, null)

            val closeImageButton = view.findViewById<View>(R.id.closeImageButton)
            closeImageButton.setOnClickListener {
                dialog.dismiss()
            }

            val icon = view.findViewById<View>(R.id.imageView12)
            val positive = view.findViewById<View>(R.id.positiveFrameLayout)
            val positiveTextView = view.findViewById<TextView>(R.id.positiveTextView)
            val positiveText = positiveTextView.text

            val positiveSpannable = SpannableString(positiveText)
            positiveSpannable.setSpan(ColorUnderlineSpan(Color.parseColor("#8A2082"), 0, positiveText.length), 0, positiveText.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
            positiveTextView.text = positiveSpannable

            positive.setOnClickListener {
                reservationConfirmationMessage.clickListener.onAddToCalendarButtonClicked()
                dialog.dismiss()
            }

            icon.visibility = if (reservationConfirmationMessage.added.isNotEmpty()) View.VISIBLE else View.GONE
            positive.visibility = icon.visibility

            val messageTextView = view.findViewById<TextView>(R.id.messageTextView)
            messageTextView.text = reservationConfirmationMessage.message


            builder.setView(view)
            builder.create()
        } ?: throw IllegalStateException("Activity cannot be null")
    }

    interface ClickListener {
        fun onAddToCalendarButtonClicked()
    }
}