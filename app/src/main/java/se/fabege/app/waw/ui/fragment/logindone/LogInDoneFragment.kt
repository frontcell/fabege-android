package se.fabege.app.waw.ui.fragment.logindone

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import se.fabege.app.R
import se.fabege.app.waw.ui.fragment.ToolbarFragment

class LogInDoneFragment : ToolbarFragment() {
    private var callback: Events? = null

    init {
        xmlLayoutId = R.layout.waw_fragment_login_done
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        callback = context as? Events
        if (callback == null) {
            throw ClassCastException("$context must implement LogInDoneFragment.Events")
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = super.onCreateView(inflater, container, savedInstanceState)

        val button = view?.findViewById<View>(R.id.button4)
        button?.setOnClickListener {
            callback?.onLoginDoneButtonClick()
        }
        return view
    }

    interface Events {
        fun onLoginDoneButtonClick()
    }
}