package se.fabege.app.waw.ui.recyclerview.adapter.delegates

import android.content.Context
import android.graphics.Color
import android.os.Build
import android.text.Html
import android.text.util.Linkify
import android.view.LayoutInflater
import android.view.ViewGroup
import se.fabege.app.waw.misc.toDateTimeString
import se.fabege.app.waw.ui.recyclerview.adapter.Item
import se.fabege.app.waw.ui.recyclerview.adapter.ListAdapterDelegate
import se.fabege.app.waw.ui.recyclerview.adapter.items.ExpandedAlertItem


class ExpandedAlertAdapterDelegate(private val context: Context, private val clickListener: ClickListener) : ListAdapterDelegate<ExpandedAlertItem, Item, ExpandedAlertItem.ViewHolder>() {
    override fun isForViewType(item: Item, items: List<Item>, pos: Int): Boolean {
        return item is ExpandedAlertItem
    }

    override fun onCreateViewHolder(parent: ViewGroup): ExpandedAlertItem.ViewHolder {
        return ExpandedAlertItem.ViewHolder(LayoutInflater.from(context).inflate(se.fabege.app.R.layout.waw_rv_item_alert_expanded, parent, false))
    }

    override fun onBindViewHolder(item: ExpandedAlertItem, viewHolder: ExpandedAlertItem.ViewHolder) {
        viewHolder.collapseButton.setOnClickListener {
            clickListener.onExpandedAlertClicked(item.id)
        }

        viewHolder.title.text = item.viewModel.alert.title

        viewHolder.message.setLinkTextColor(Color.WHITE)

        viewHolder.message.text = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) Html.fromHtml(item.viewModel.alert.message, Html.FROM_HTML_MODE_COMPACT) else Html.fromHtml(item.viewModel.alert.message)
        Linkify.addLinks(viewHolder.message, Linkify.ALL)

        viewHolder.date.text = item.viewModel.alert.publishAt.toDateTimeString()
    }


    interface ClickListener {
        fun onExpandedAlertClicked(id: String)
    }
}