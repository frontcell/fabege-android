package se.fabege.app.waw.domain.usecase

import se.fabege.app.waw.data.Result
import se.fabege.app.waw.domain.model.WAWUser
import se.fabege.app.waw.domain.repository.DataRepository
import se.fabege.app.waw.misc.exhaustive

class CleanWAWUserUseCaseImpl(private val dataRepository: DataRepository) : CleanWAWUserUseCase() {

    override suspend fun invoke(user: WAWUser): Result<Unit> {
        val result = dataRepository.cleanUser(user.id)
        return when (result) {
            is Result.Success -> result
            is Result.Error -> result
        }.exhaustive
    }
}