package se.fabege.app.waw.domain.usecase

class IsLogInWithEmailLinkViaApiUseCaseImpl : IsLogInWithEmailLinkUseCase() {

    override operator fun invoke(emailLink: String): Boolean {
        return emailLink.startsWith("https://fabege-b28bd.firebaseapp.com/?token=")
    }
}