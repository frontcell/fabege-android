package se.fabege.app.waw.ui.recyclerview.adapter.items

import android.support.v7.widget.RecyclerView
import android.view.View
import se.fabege.app.R
import se.fabege.app.waw.domain.model.WorkspaceMetadata
import se.fabege.app.waw.ui.recyclerview.adapter.Item
import se.fabege.app.waw.ui.widget.FacilityView

data class WorkspaceMetaDataFacilityItem(val viewModel: ViewModel) : Item {

    data class ViewModel(val facilityInfo: WorkspaceMetadata.FacilityInfo)

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val facilityView = itemView.findViewById<FacilityView>(R.id.facilityView)
    }

    fun isContentSame(other: WorkspaceMetaDataFacilityItem): Boolean {
        return this.viewModel.facilityInfo.facilities.size == other.viewModel.facilityInfo.facilities.size
    }
}