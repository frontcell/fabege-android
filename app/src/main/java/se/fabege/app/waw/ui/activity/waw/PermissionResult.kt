package se.fabege.app.waw.ui.activity.waw

import android.content.pm.PackageManager

class PermissionResult(grantResults: IntArray) {
    enum class Result {
        GRANTED, DENIED, CANCELED
    }

    var result: Result

    init {
        result = if (grantResults.isEmpty()) {
            Result.CANCELED
        } else {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                Result.GRANTED
            } else {
                Result.DENIED
            }
        }
    }
}


