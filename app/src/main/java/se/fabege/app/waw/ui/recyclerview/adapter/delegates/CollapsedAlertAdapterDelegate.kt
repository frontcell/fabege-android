package se.fabege.app.waw.ui.recyclerview.adapter.delegates

import android.animation.ValueAnimator
import android.content.Context
import android.support.constraint.ConstraintLayout
import android.support.constraint.ConstraintSet
import android.transition.ChangeBounds
import android.transition.Transition
import android.transition.TransitionManager
import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.animation.LinearInterpolator
import se.fabege.app.R
import se.fabege.app.waw.ui.recyclerview.adapter.Item
import se.fabege.app.waw.ui.recyclerview.adapter.ListAdapterDelegate
import se.fabege.app.waw.ui.recyclerview.adapter.items.CollapsedAlertItem
import android.os.Handler


class CollapsedAlertAdapterDelegate(private val context: Context, private val clickListener: ClickListener) : ListAdapterDelegate<CollapsedAlertItem, Item, CollapsedAlertItem.ViewHolder>() {
    override fun isForViewType(item: Item, items: List<Item>, pos: Int): Boolean {
        return item is CollapsedAlertItem
    }

    override fun onCreateViewHolder(parent: ViewGroup): CollapsedAlertItem.ViewHolder {
        return CollapsedAlertItem.ViewHolder(LayoutInflater.from(context).inflate(se.fabege.app.R.layout.waw_rv_item_alert_collapsed, parent, false))
    }

    override fun onBindViewHolder(itemCollapsed: CollapsedAlertItem, viewHolder: CollapsedAlertItem.ViewHolder) {
        viewHolder.itemView.setOnClickListener {
            clickListener.onCollapsedAlertClicked(itemCollapsed.id)
        }

        viewHolder.title.text = itemCollapsed.viewModel.alert.title
    }


    interface ClickListener {
        fun onCollapsedAlertClicked(id: String)
    }
}