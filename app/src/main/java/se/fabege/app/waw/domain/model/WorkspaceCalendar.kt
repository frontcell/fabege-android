package se.fabege.app.waw.domain.model

import java.util.*

data class WorkspaceCalendar(val id: String, val calendarDays: List<CalendarDay>, val deskInfo: WorkspaceMetadata.DeskInfo, val timeSlotInfo: WorkspaceMetadata.TimeSlotInfo)

data class CalendarDay(val id: String, val date: Date, val timeSlots: List<TimeSlot>)

data class TimeSlot(val id: String, val status: Status, val reservations: List<String>, val start: Long, val end: Long)

data class Reservation(val id: String, val timeSlot: TimeSlot)

enum class  Status {
    OPEN, CLOSED_HOUR, CLOSED_HOLIDAY, FULL, NA
}

