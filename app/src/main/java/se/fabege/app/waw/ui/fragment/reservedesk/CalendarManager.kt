package se.fabege.app.waw.ui.fragment.reservedesk

import se.fabege.app.R
import se.fabege.app.base.GlobalState
import se.fabege.app.waw.domain.model.*
import se.fabege.app.waw.misc.Utils
import se.fabege.app.waw.ui.recyclerview.adapter.items.CalendarFooterItem
import se.fabege.app.waw.ui.recyclerview.adapter.items.CalendarHeaderItem
import se.fabege.app.waw.ui.recyclerview.adapter.items.CalendarItem
import java.lang.IllegalStateException
import java.util.*

class CalendarManager(private val wawUser: WAWUser?, private val workspaceCalendar: WorkspaceCalendar?, pendingAddedReservations: List<Reservation> = emptyList(), pendingRemovedReservations: List<Reservation> = emptyList()) {

    private lateinit var start: Array<Array<String>>

    private var refTimeSlot: MutableMap<String, TimeSlot>
    private var refStatus: MutableMap<String, Status>
    private var refReservations: MutableMap<String, Boolean>

    private var decorations: MutableMap<String, CalendarItem.Decoration>


    init {
        workspaceCalendar?.let {
            start = Array(workspaceCalendar.calendarDays.size)
            { i ->
                Array(workspaceCalendar.calendarDays[i].timeSlots.size)
                { j -> workspaceCalendar.calendarDays[i].timeSlots[j].status.name }
            }
        }

        refTimeSlot = HashMap()
        refStatus = HashMap()
        refReservations = HashMap()
        workspaceCalendar?.let {
            for (calendarDay in it.calendarDays) {
                for (timeSlot in calendarDay.timeSlots) {
                    refTimeSlot[timeSlot.id] = timeSlot
                    refStatus[timeSlot.id] = timeSlot.status
                    refReservations[timeSlot.id] = timeSlot.reservations.contains(wawUser?.id)
                }
            }
        }

        decorations = HashMap()
        for (status in refStatus) {
            decorations[status.key] = when (status.value) {
                Status.OPEN -> CalendarItem.Decoration.OPEN
                Status.CLOSED_HOUR -> CalendarItem.Decoration.CLOSED
                Status.CLOSED_HOLIDAY -> CalendarItem.Decoration.HOLIDAY
                Status.FULL -> CalendarItem.Decoration.FULL
                Status.NA -> CalendarItem.Decoration.NONE
            }
        }

        // Overwrite full timeslots
        val now = Utils.getNowInMillis()
        val endOfDay = Utils.getEndOfDayInMillis()
        val earlier = workspaceCalendar?.deskInfo?.earlier ?: 0
        val sameDay = workspaceCalendar?.deskInfo?.sameDay ?: 0
        for (timeSlot in refTimeSlot) {
            if (timeSlot.value.start > endOfDay) {
                if (timeSlot.value.reservations.size >= earlier) {
                    decorations[timeSlot.key] = CalendarItem.Decoration.FULL
                }
            } else if (timeSlot.value.end <= endOfDay) {
                if (timeSlot.value.reservations.size >= earlier + sameDay) {
                    decorations[timeSlot.key] = CalendarItem.Decoration.FULL
                }
            }
        }


        for (reservation in refReservations) {
            if (reservation.value) {
                decorations[reservation.key] = CalendarItem.Decoration.RESERVED
            }
        }


        // Overwrite due timeslots
        for (timeSlot in refTimeSlot) {
            if (timeSlot.value.end < now) {
                decorations[timeSlot.key] = CalendarItem.Decoration.NONE
            }
        }

        // Restore transient states
        for (reservation in pendingAddedReservations) {
            if (decorations[reservation.id] == CalendarItem.Decoration.OPEN) {
                onRowDataClicked(reservation.id)
            }
        }
        for (reservation in pendingRemovedReservations) {
            if (decorations[reservation.id] == CalendarItem.Decoration.RESERVED) {
                onRowDataClicked(reservation.id)
            }
        }
    }

    private fun prettyPrint(array: Array<Array<String>>) {
        for (arr in array) {
            for (value in arr) {
                print("[$value]")
            }
            println()
        }
    }


    fun onRowDataClicked(id: String) {
        val startStatus = refStatus[id]
        val currentDecoration = decorations[id]
        val previouslyReserved = refReservations[id]

        currentDecoration?.let {
            when (it) {
                CalendarItem.Decoration.OPEN -> {
                    decorations[id] = if (previouslyReserved == true) CalendarItem.Decoration.RESERVED else CalendarItem.Decoration.SELECTED
                }
                CalendarItem.Decoration.SELECTED -> decorations[id] = CalendarItem.Decoration.OPEN
                CalendarItem.Decoration.RESERVED -> decorations[id] = CalendarItem.Decoration.OPEN
            }
        }
    }

    fun createCalendarHeaderItem(): CalendarHeaderItem {
        if (workspaceCalendar == null) {
            return CalendarHeaderItem(CalendarHeaderItem.ViewModel(emptyList()))
        }
        return CalendarHeaderItem(CalendarHeaderItem.ViewModel(workspaceCalendar.timeSlotInfo.spans.map { span ->
            val times = span.split("-")

            val start = times[0].split(":")
            val end = times[1].split(":")

            val startHour = if (start[0] == "00") start[0] else start[0].removePrefix("0")
            val startMin = if (start[1] == "00") "" else ":" + start[1]

            val endHour = if (end[0] == "00") end[0] else end[0].removePrefix("0")
            val endMin = if (end[1] == "00") "" else ":" + end[1]

            "$startHour$startMin-$endHour$endMin"
        }))
    }

    fun createCalendarItem(): CalendarItem {
        if (workspaceCalendar == null) {
            return CalendarItem(CalendarItem.ViewModel(emptyList()))
        }

        return CalendarItem(CalendarItem.ViewModel(createRows()))
    }

    fun createCalendarFooterItem(): CalendarFooterItem {
        if (workspaceCalendar == null) {
            return CalendarFooterItem(CalendarFooterItem.ViewModel(false, false, getButtonText()))
        }

        return CalendarFooterItem(CalendarFooterItem.ViewModel(getReservationsNow().isNotEmpty(), isDirty(), getButtonText()))
    }

    fun isDirty(): Boolean {
        return hasReservationsBeenAdded() || hasReservationsBeenRemoved()
    }

    fun hasReservations(): Boolean {
        for (reservation in refReservations) {
            if (reservation.value) {
                return true
            }
        }
        return false
    }

    fun getReservationsNow(): List<Reservation> {
        val all = refReservations.filter { entry -> entry.value }.map { Reservation(it.key, refTimeSlot[it.key]!!) }

        val now = Utils.getNowInMillis()
        val endOfDay = Utils.getEndOfDayInMillis()

        //        for (reservation in all) {
//            println("workspaceId:${workspaceCalendar?.id} now:$now keyEndTime: ${reservation.timeSlot.end} endOfDay:$endOfDay ${reservation.timeSlot.end >= now && reservation.timeSlot.end <= endOfDay}")
//        }

        val filtered = all.filter { reservation -> reservation.timeSlot.end >= now && reservation.timeSlot.end <= endOfDay }

        return filtered
    }

    fun getReservationsNowAndAfter(): List<Reservation> {
        val all = refReservations.filter { entry -> entry.value }.map { Reservation(it.key, refTimeSlot[it.key]!!) }

        val now = Utils.getNowInMillis()

        val filtered = all.filter { reservation -> reservation.timeSlot.end >= now }

        return filtered
    }


    private class Id(slotId: String) {
        var workspaceId: String
        var daysId: String
        var slotIndex: Int

        init {
            val paths = slotId.split("/")
            workspaceId = paths[0]
            daysId = paths[1]
            slotIndex = paths[2].toInt()
        }
    }

    private class TimeSpan(span: String) {
        var start: String
        var end: String

        init {
            val paths = span.split("-")
            start = paths[0]
            end = paths[1]
        }
    }

    fun getKeysNowAndAfter(): List<Key> {
        val now = Utils.getNowInMillis()

        workspaceCalendar?.let { calendar ->
            wawUser?.let { user ->
                val keys = user.keys.filter { key ->
                    if (key.timeSlotId.isEmpty()) {
                        return@filter false
                    }
                    val id = Id(key.timeSlotId)
                    val timeSpan = TimeSpan(calendar.timeSlotInfo.spans[id.slotIndex])

                    // E.g. 20190119T09:00
                    val keyEndTime = Utils.parseDateTime(id.daysId + "T" + timeSpan.end).time

                    id.workspaceId == calendar.id && keyEndTime >= now
                }
                return keys
            }
        }
        return emptyList()
    }

    fun getKeysNow(): List<Key> {
        val now = Utils.getNowInMillis()
        val endOfDay = Utils.getEndOfDayInMillis()

        workspaceCalendar?.let { calendar ->
            wawUser?.let { user ->
                val keys = user.keys.filter { key ->
                    if (key.timeSlotId.isEmpty()) {
                        return@filter false
                    }
                    val id = Id(key.timeSlotId)
                    val timeSpan = TimeSpan(calendar.timeSlotInfo.spans[id.slotIndex])

                    // E.g. 20190119T09:00
                    val keyEndTime = Utils.parseDateTime(id.daysId + "T" + timeSpan.end).time
//                    println("xxxyz workspaceId:${id.workspaceId} calendarId:${calendar.id} now:$now keyEndTime: $keyEndTime endOfDay:$endOfDay ${id.workspaceId == calendar.id && keyEndTime >= now && keyEndTime <= endOfDay}")
                    id.workspaceId == calendar.id && keyEndTime >= now && keyEndTime <= endOfDay
                }
                return keys
            }
        }
        return emptyList()
    }

    private fun hasReservationsBeenAdded(): Boolean {
        for (decoration in decorations) {
            if (decoration.value == CalendarItem.Decoration.SELECTED && !refReservations[decoration.key]!!) {
                return true
            }
        }
        return false
    }

    fun getAddedReservations(): List<Reservation> {
        val result = mutableListOf<Reservation>()

        for (decoration in decorations) {
            if (decoration.value == CalendarItem.Decoration.SELECTED && !refReservations[decoration.key]!!) {
                result.add(Reservation(refTimeSlot[decoration.key]!!.id, refTimeSlot[decoration.key]!!))
            }
        }
        return result
    }


    private fun hasReservationsBeenRemoved(): Boolean {
        for (reservation in refReservations) {
            if (reservation.value) {
                val decoration = decorations[reservation.key]!!
                if (decoration == CalendarItem.Decoration.OPEN) {
                    return true
                }
            }
        }
        return false
    }

    fun getRemovedReservations(): List<Reservation> {
        val result = mutableListOf<Reservation>()

        for (reservation in refReservations) {
            if (reservation.value) {
                val decoration = decorations[reservation.key]!!
                if (decoration == CalendarItem.Decoration.OPEN) {
                    result.add(Reservation(refTimeSlot[reservation.key]!!.id, refTimeSlot[reservation.key]!!))
                }
            }
        }
        return result
    }

    fun rollback() {
        for (reservation in getRemovedReservations()) {
            decorations[reservation.id] = CalendarItem.Decoration.RESERVED
        }
    }

    private fun getButtonText(): String {
        return if (isDirty() && hasReservationsBeenRemoved()) {
            GlobalState.getContext().getString(R.string.waw_change_reservation)
        } else {
            GlobalState.getContext().getString(R.string.waw_confirm_reservation)
        }
    }

    private fun createRows(): List<CalendarItem.Row> {
        if (workspaceCalendar == null) {
            throw IllegalStateException("workspaceCalendar cannot be null")
        }
        return workspaceCalendar.calendarDays.map { CalendarItem.Row(it.date, createRowDatas(it.timeSlots)) }
    }

    private fun createRowDatas(timeSlots: List<TimeSlot>): List<CalendarItem.RowData> {
        return timeSlots.map { CalendarItem.RowData(it.id, decorate(it)) }
    }

    private fun decorate(timeSlot: TimeSlot): CalendarItem.Decoration {
        return decorations[timeSlot.id] ?: CalendarItem.Decoration.NONE
    }
}