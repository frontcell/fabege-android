package se.fabege.app.waw.ui

import android.content.Intent
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import se.fabege.app.waw.data.AppNotEligibleMessage
import se.fabege.app.waw.data.GenericErrorMessage
import se.fabege.app.waw.data.Result
import se.fabege.app.waw.data.error.DataRepositoryException
import se.fabege.app.waw.data.error.IsAppEligibleException
import se.fabege.app.waw.data.error.WAWUserMissingException
import se.fabege.app.waw.device.Device
import se.fabege.app.waw.domain.model.Alert
import se.fabege.app.waw.domain.model.WAWUser
import se.fabege.app.waw.domain.repository.DataRepository
import se.fabege.app.waw.misc.Injection
import se.fabege.app.waw.ui.dialog.AppNotEligibleMessageDialog

open class BasePresenterImpl(private val baseView: BaseView<*>, protected val device: Device) : BasePresenter, AppNotEligibleMessageDialog.ClickListener, DataRepository.UserCallback, DataRepository.AppConfigCallback, DataRepository.AlertsCallback {

    protected val parentJob = Job()
    protected val dispatcherProvider = Injection.provideCoroutinesDispatcherProvider()
    protected val scope = CoroutineScope(dispatcherProvider.ui + parentJob)

    private val takeOff = Injection.provideTakeOffUseCase()
    private val isAppEligable = Injection.provideIsAppEligibleUseCase()
    private val getWAWUser = Injection.provideGetWAWUserUseCase()
    private val getAlerts = Injection.provideGetAlertsUseCase()
    private val subscribeWAWUser = Injection.provideSubscripbeWAWUserUseCase()
    private val subscribeAppConfig = Injection.provideSubscribeAppConfigUseCase()
    private val subscribeAlerts = Injection.provideSubscribeAlertsUserCase()
    private val logoutUser = Injection.provideLogoutUserUseCase()
    private val unsubscribe = Injection.provideUnsubscribeUseCase()

    lateinit var wawUser: WAWUser
    private var firstTime: Boolean = true
    private var takeOffSuccessful = false
    private var skipOnUserDataChanged = true
    private var skipOnAppConfigChanged = true
    private var skipOnAlertsChanged = true;

    override fun start(intent: Intent?) {
        onStart()
    }

    override fun resume(intent: Intent?) {
        onPreResume()
        if (firstTime || !takeOffSuccessful) {
            firstTime = false
            baseView.showBusyDialog()
        }
        scope.launch(dispatcherProvider.computation) {
            val result = takeOff()
            withContext(dispatcherProvider.ui) {
                baseView.dismissBusyDialog()
            }
            when (result) {
                is Result.Success -> {
                    takeOffSuccessful = true
                    wawUser = result.data
                    startSubscriptions()
                    withContext(dispatcherProvider.ui) {
                        onResume()
                    }
                }
                is Result.Error -> {
                    takeOffSuccessful = false
                    when (result.exception) {
                        is WAWUserMissingException -> {
                            logoutUser()
                            withContext(dispatcherProvider.ui) {
                                baseView.exit()
                            }
                        }
                        is IsAppEligibleException -> {
                            withContext(dispatcherProvider.ui) {
                                baseView.showMessage(AppNotEligibleMessage(this@BasePresenterImpl))
                            }
                        }
                        is DataRepositoryException -> {
                            withContext(dispatcherProvider.ui) {
                                baseView.showMessage(GenericErrorMessage())
                            }
                        }
                    }
                }
            }
        }
    }

    private fun startSubscriptions() {
        skipOnUserDataChanged = true
        skipOnAppConfigChanged = true
        subscribeWAWUser(wawUser.id, this)
        subscribeAppConfig(this)
        subscribeAlerts(this)
    }

    override fun stop() {
        unsubscribe()
        onStop()
    }

    override fun destroy() {
        onDestroy()
    }


    open fun onStart() {}

    open fun onPreResume() {}

    open fun onResume() {}

    open fun onStop() {}

    open fun onDestroy() {}

    open fun onUserDataChanged(wawUser: WAWUser) {}

    open fun onAlertsChanged(alerts: List<Alert>) {}

    override fun onUserDataChanged() {
        if (skipOnUserDataChanged) {
            skipOnUserDataChanged = false
        } else {
            scope.launch(dispatcherProvider.computation) {
                val result = getWAWUser()
                when (result) {
                    is Result.Success -> {
                        onUserDataChanged(result.data)
                    }
                    is Result.Error -> {
                        if (result.exception is WAWUserMissingException) {
                            logoutUser()
                            withContext(dispatcherProvider.ui) {
                                baseView.exit()
                            }
                        }
                    }
                }
            }
        }
    }

    override fun onAppConfigChanged() {
        if (skipOnAppConfigChanged) {
            skipOnAppConfigChanged = false
        } else {
            scope.launch(dispatcherProvider.computation) {
                val result = isAppEligable()
                when (result) {
                    is Result.Error -> {
                        if (result.exception is IsAppEligibleException) {
                            withContext(dispatcherProvider.ui) {
                                baseView.showMessage(AppNotEligibleMessage(this@BasePresenterImpl))
                            }
                        }
                    }
                }
            }
        }
    }

    override fun onAlertsChanged() {
        if (skipOnAlertsChanged) {
            skipOnAlertsChanged = false
        } else {
            scope.launch(dispatcherProvider.computation) {
                val result = getAlerts()
                when (result) {
                    is Result.Success -> {
                        onAlertsChanged(result.data)
                    }
                }
            }
        }
    }

    override fun onPositiveAppNotEligibleButtonClicked() {
        device.openFabegeGooglePlayPage()
    }

    override fun onNegativeAppNotEligibleButtonClicked() {
        baseView.exit()
    }
}