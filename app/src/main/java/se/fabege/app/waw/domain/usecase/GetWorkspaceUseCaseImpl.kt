package se.fabege.app.waw.domain.usecase

import se.fabege.app.waw.data.Result
import se.fabege.app.waw.domain.model.Workspace
import se.fabege.app.waw.domain.repository.DataRepository
import se.fabege.app.waw.misc.exhaustive

class GetWorkspaceUseCaseImpl(private val dataRepository: DataRepository) : GetWorkspaceUseCase() {

    override suspend fun invoke(id: String): Result<Workspace> {
        val result = dataRepository.getWorkSpace(id)

        return when (result) {
            is Result.Success -> result
            is Result.Error -> result
        }.exhaustive
    }
}