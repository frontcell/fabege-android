package se.fabege.app.waw.domain.usecase

import se.fabege.app.waw.data.Result
import se.fabege.app.waw.domain.model.AuthUser
import se.fabege.app.waw.domain.repository.AuthRepository
import se.fabege.app.waw.misc.exhaustive

class LogInUserWithEmailLinkUseCaseImpl(private val authRepository: AuthRepository): LogInUserWithEmailLinkUseCase() {

    override suspend operator fun invoke(emailLink: String): Result<AuthUser> {
        val result = authRepository.loginWithEmailLink(emailLink)
        return when(result) {
            is Result.Success -> result
            is Result.Error -> result
        } .exhaustive
    }
}