package se.fabege.app.waw.ui.fragment.onboarding

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import se.fabege.app.R

class OnboardingPage1 : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.waw_fragment_onboarding_page_1, container, false)
        return view
    }
}

class OnboardingPage2 : Fragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.waw_fragment_onboarding_page_2, container, false)
        return view
    }
}

class OnboardingPage3 : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.waw_fragment_onboarding_page_3, container, false)
        return view
    }
}

class OnboardingPage4 : Fragment() {

    private var callback: Events? = null

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        callback = parentFragment as? Events
        if (callback == null) {
            throw ClassCastException("$parentFragment must implement OnboardingPage4.Events")
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.waw_fragment_onboarding_page_4, container, false)
        val button = view?.findViewById<View>(R.id.button2)
        button?.setOnClickListener {
            callback?.onDoneButtonClick()
        }
        return view
    }

    interface Events {
        fun onDoneButtonClick()
    }
}