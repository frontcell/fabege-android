package se.fabege.app.waw.domain.usecase

import se.fabege.app.waw.domain.repository.DataRepository

class SubscribeAlertsUseCaseImpl(private val dataRepository: DataRepository) : SubscribeAlertsUseCase() {
    override fun invoke(callback: DataRepository.AlertsCallback) {
        dataRepository.subscribeAlerts(callback)
    }
}