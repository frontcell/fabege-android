package se.fabege.app.waw.ui.recyclerview.adapter.items

import android.support.v7.widget.RecyclerView
import android.view.View
import se.fabege.app.R
import se.fabege.app.waw.ui.recyclerview.adapter.Item
import se.fabege.app.waw.ui.widget.CalendarView
import java.util.*

data class CalendarItem(val viewModel: ViewModel) : Item {

    data class ViewModel(val rows: List<Row>)

    data class Row(val date: Date, val data: List<RowData>)

    data class RowData(val id: String, val decoration: Decoration)

    enum class Decoration {
        NONE, OPEN, CLOSED, FULL, HOLIDAY, SELECTED, RESERVED
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val calendarView: CalendarView = itemView.findViewById(R.id.calendarView)
    }
}