package se.fabege.app.waw.domain.model

import se.fabege.app.waw.domain.repository.KeyValueRepository

interface KeyValueRepositoryFactory {
    fun get(name: String) : KeyValueRepository
}