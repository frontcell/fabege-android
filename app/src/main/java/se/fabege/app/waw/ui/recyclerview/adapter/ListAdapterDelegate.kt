package se.fabege.app.waw.ui.recyclerview.adapter

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup

abstract class ListAdapterDelegate<I : T, T, VH : RecyclerView.ViewHolder> : AdapterDelegate<List<T>>() {

    override fun isForViewType(items: List<T>, pos: Int): Boolean {
        return isForViewType(items[pos], items, pos)
    }

    override fun onBindViewHolder(items: List<T>, pos: Int, viewHolder: RecyclerView.ViewHolder) {
        onBindViewHolder(items[pos] as I, viewHolder as VH)
    }


    protected abstract fun isForViewType(item: T, items: List<T>, pos: Int): Boolean

    abstract override fun onCreateViewHolder(parent: ViewGroup): VH

    protected abstract fun onBindViewHolder(item: I, viewHolder: VH)
}