package se.fabege.app.waw.ui.recyclerview.adapter.delegates

import android.content.Context
import android.os.Build
import android.text.Html
import android.text.util.Linkify
import android.view.LayoutInflater
import android.view.ViewGroup
import se.fabege.app.R
import se.fabege.app.waw.ui.recyclerview.adapter.Item
import se.fabege.app.waw.ui.recyclerview.adapter.ListAdapterDelegate
import se.fabege.app.waw.ui.recyclerview.adapter.items.WorkspaceMetaDataExtraItem

class WorkspaceMetaDataExtraAdapterDelegate(private val context: Context) : ListAdapterDelegate<WorkspaceMetaDataExtraItem, Item, WorkspaceMetaDataExtraItem.ViewHolder>() {
    override fun isForViewType(item: Item, items: List<Item>, pos: Int): Boolean {
        return item is WorkspaceMetaDataExtraItem
    }

    override fun onCreateViewHolder(parent: ViewGroup): WorkspaceMetaDataExtraItem.ViewHolder {
        return WorkspaceMetaDataExtraItem.ViewHolder(LayoutInflater.from(context).inflate(R.layout.waw_rv_item_workspace_metadata_extra, parent, false))
    }

    override fun onBindViewHolder(item: WorkspaceMetaDataExtraItem, viewHolder: WorkspaceMetaDataExtraItem.ViewHolder) {

        viewHolder.extraTextView.text = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) Html.fromHtml(item.viewModel.extra, Html.FROM_HTML_MODE_COMPACT) else Html.fromHtml(item.viewModel.extra)

        Linkify.addLinks(viewHolder.extraTextView, Linkify.ALL)
    }
}