package se.fabege.app.waw.ui.recyclerview.adapter.delegates

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import se.fabege.app.R
import se.fabege.app.waw.ui.recyclerview.adapter.Item
import se.fabege.app.waw.ui.recyclerview.adapter.ListAdapterDelegate
import se.fabege.app.waw.ui.recyclerview.adapter.items.WorkspaceMetaDataFacilityItem

class WorkspaceMetaDataFacilityAdapterDelegate(private val context: Context) : ListAdapterDelegate<WorkspaceMetaDataFacilityItem, Item, WorkspaceMetaDataFacilityItem.ViewHolder>() {
    override fun isForViewType(item: Item, items: List<Item>, pos: Int): Boolean {
        return item is WorkspaceMetaDataFacilityItem
    }

    override fun onCreateViewHolder(parent: ViewGroup): WorkspaceMetaDataFacilityItem.ViewHolder {
        return WorkspaceMetaDataFacilityItem.ViewHolder(LayoutInflater.from(context).inflate(R.layout.waw_rv_item_workspace_metadata_facility, parent, false))
    }

    override fun onBindViewHolder(item: WorkspaceMetaDataFacilityItem, viewHolder: WorkspaceMetaDataFacilityItem.ViewHolder) {
        viewHolder.facilityView.update(item.viewModel.facilityInfo)
    }

}