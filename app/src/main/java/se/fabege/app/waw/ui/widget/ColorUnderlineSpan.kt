package se.fabege.app.waw.ui.widget

import android.graphics.Canvas
import android.graphics.Paint
import android.text.style.LineBackgroundSpan

class ColorUnderlineSpan(underlineColor: Int, private val underlineStart: Int, private val underlineEnd: Int) : LineBackgroundSpan {

    val paint = Paint()

    init {
        paint.color = underlineColor
        paint.strokeWidth = 4.0f
        paint.style = Paint.Style.FILL_AND_STROKE
    }

    override fun drawBackground(c: Canvas?, p: Paint?, left: Int, right: Int, top: Int, baseline: Int, bottom: Int, text: CharSequence?, start: Int, end: Int, lnum: Int) {
        if (!(underlineStart < underlineEnd)) {
            throw Error("underlineEnd should be greater than underlineStart")
        }

        if (underlineStart > end || underlineEnd < start) {
            return
        }

        var offsetX = 0

        if (underlineStart > start) {
            offsetX = p?.measureText(text?.subSequence(start, underlineStart).toString())?.toInt() ?: 0
        }

        val length: Int = p?.measureText(text?.subSequence(Math.max(start, underlineStart), Math.min(end, underlineEnd)).toString())?.toInt()
                ?: 0

        c?.drawLine(offsetX.toFloat(), baseline + 4.0f, (length + offsetX).toFloat(), baseline + 4.0f, paint)
    }
}
