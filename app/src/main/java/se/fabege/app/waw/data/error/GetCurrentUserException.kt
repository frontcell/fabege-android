package se.fabege.app.waw.data.error

import java.lang.RuntimeException

class GetCurrentUserException : RuntimeException()