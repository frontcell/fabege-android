package se.fabege.app.waw.ui.recyclerview.adapter.delegates

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import se.fabege.app.R
import se.fabege.app.waw.ui.recyclerview.adapter.Item
import se.fabege.app.waw.ui.recyclerview.adapter.ListAdapterDelegate
import se.fabege.app.waw.ui.recyclerview.adapter.items.WorkspaceSummaryItem

class WorkspaceSummaryAdapterDelegate(private val context: Context) : ListAdapterDelegate<WorkspaceSummaryItem, Item, WorkspaceSummaryItem.ViewHolder>() {
    override fun isForViewType(item: Item, items: List<Item>, pos: Int): Boolean {
        return item is WorkspaceSummaryItem
    }

    override fun onCreateViewHolder(parent: ViewGroup): WorkspaceSummaryItem.ViewHolder {
        return WorkspaceSummaryItem.ViewHolder(LayoutInflater.from(context).inflate(R.layout.waw_rv_item_workspace_summary, parent, false))
    }

    override fun onBindViewHolder(item: WorkspaceSummaryItem, viewHolder: WorkspaceSummaryItem.ViewHolder) {
        viewHolder.summaryTextView.text = item.viewModel.summary
    }
}