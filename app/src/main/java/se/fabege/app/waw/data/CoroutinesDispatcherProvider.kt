package se.fabege.app.waw.data

import kotlinx.coroutines.CoroutineDispatcher

data class CoroutinesDispatcherProvider(
        val ui: CoroutineDispatcher,
        val computation: CoroutineDispatcher,
        val network: CoroutineDispatcher
)