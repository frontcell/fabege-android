package se.fabege.app.waw.ui.fragment.logininstallparakey

import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.text.Spannable
import android.text.SpannableString
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import se.fabege.app.R
import se.fabege.app.waw.device.Device
import se.fabege.app.waw.misc.Injection
import se.fabege.app.waw.ui.fragment.ToolbarFragment
import se.fabege.app.waw.ui.widget.ColorUnderlineSpan

class LogInInstallParakeyFragment : ToolbarFragment() {
    private var callback: Events? = null

    private lateinit var device: Device

    private lateinit var parakeyLogo: ImageView
    private lateinit var installParakeyTextView: TextView

    init {
        xmlLayoutId = R.layout.waw_fragment_login_install_parakey
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        callback = context as? Events
        if (callback == null) {
            throw ClassCastException("$context must implement LogInInstallParakeyFragment.Events")
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        device = Injection.provideDevice(activity!!)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = super.onCreateView(inflater, container, savedInstanceState)

        parakeyLogo = view?.findViewById(R.id.imageView5)!!
        parakeyLogo.setOnClickListener {
            device.openParakeyGooglePlayPage()
        }

        installParakeyTextView = view?.findViewById(R.id.installParaykeyTextView)!!
        val text = installParakeyTextView.text
        val textSpannable = SpannableString(text)
        textSpannable.setSpan(ColorUnderlineSpan(Color.parseColor("#8A2082"), 0, text.length), 0, text.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        installParakeyTextView.text = textSpannable
        installParakeyTextView?.setOnClickListener {
            device.openParakeyGooglePlayPage()
        }
        return view
    }

    override fun onResume() {
        super.onResume()
        if (device.isParakeyAppInstalled()) {
            callback?.onParaykeyAppInstalled()
        }
    }

    interface Events {
        fun onParaykeyAppInstalled()
    }
}