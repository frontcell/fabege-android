package se.fabege.app.waw.ui.fragment.reservedesk

import android.animation.LayoutTransition
import android.content.Context
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.LinearSnapHelper
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import se.fabege.app.R
import se.fabege.app.fragment.AlertsAdapter
import se.fabege.app.waw.domain.model.Alert
import se.fabege.app.waw.misc.Injection
import se.fabege.app.waw.ui.fragment.ToolbarFragment
import se.fabege.app.waw.ui.recyclerview.adapter.AdapterDelegateManager
import se.fabege.app.waw.ui.recyclerview.adapter.Item
import se.fabege.app.waw.ui.recyclerview.adapter.delegates.*
import se.fabege.app.waw.ui.recyclerview.adapter.items.CollapsedAlertItem
import se.fabege.app.waw.ui.recyclerview.adapter.items.EmptyAlertItem
import se.fabege.app.waw.ui.recyclerview.adapter.items.ExpandedAlertItem

class ReserveDeskFragment : ToolbarFragment(), ReserveDeskContract.View {
    companion object {
        fun newInstance(workspaceId: String, deskId: String): ReserveDeskFragment {
            val args = Bundle()
            args.putString("workspaceId", workspaceId)
            args.putString("deskId", deskId)
            val fragment = ReserveDeskFragment()
            fragment.arguments = args
            return fragment
        }
    }

    private var reserveDeskEventHandler: Events? = null
    private lateinit var presenter: ReserveDeskContract.Presenter
    private lateinit var adapter: ReserveDeskAdapter

    private lateinit var alertsContainer: View
    private lateinit var alertsRecyclerView: RecyclerView
    private lateinit var alertsAdapter: AlertsAdapter
    private lateinit var alertIcon: ImageView

    init {
        xmlLayoutId = R.layout.waw_fragment_reserve_desk
    }

    override fun setPresenter(presenter: ReserveDeskContract.Presenter) {
        this.presenter = presenter
        presenter.start(activity?.intent)
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        reserveDeskEventHandler = context as? Events
        if (reserveDeskEventHandler == null) {
            throw ClassCastException("$context must implement ReserveDeskFragment.Events")
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        ReserveDeskPresenter(this, arguments?.getString("workspaceId")
                ?: throw IllegalStateException("workspaceId cannot be null"), Injection.provideDevice(activity!!))
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = super.onCreateView(inflater, container, savedInstanceState)

        toolbarFaqButton?.setOnClickListener {
            presenter.onFaqButtonClicked()
        }


        val ll = view!!.findViewById<LinearLayout>(R.id.parentLinearLayout)
        ll.layoutTransition.enableTransitionType(LayoutTransition.CHANGING)
        val recyclerView = view.findViewById<RecyclerView>(R.id.recyclerView)
        recyclerView.layoutManager = LinearLayoutManager(context)

        val adapterDelegateManager = AdapterDelegateManager<List<Item>>()
        with(adapterDelegateManager) {
            addDelegate(CalendarHeaderAdapterDelegate(context!!))
            addDelegate(CalendarAdapterDelegate(context!!, presenter))
            addDelegate(CalendarFooterAdapterDelegate(context!!, presenter))
            addDelegate(WorkspaceMetaDataAboutAdapterDelegate(context!!))
            addDelegate(WorkspaceMetaDataFacilityAdapterDelegate(context!!))
            addDelegate(WorkspaceMetaDataCapacityAdapterDelegate(context!!))
            addDelegate(WorkspaceMetaDataExtraAdapterDelegate(context!!))
            addDelegate(WorkspaceMetaDataAddressAdapterDelegate(context!!, presenter))
            addDelegate(WorkspaceMetaDataContactAdapterDelegate(context!!, presenter))
            adapterDelegateManager.addDelegate(EmptyAlertAdapterDelegate(context!!))
            adapterDelegateManager.addDelegate(CollapsedAlertAdapterDelegate(context!!, presenter))
            adapterDelegateManager.addDelegate(ExpandedAlertAdapterDelegate(context!!, presenter))
        }

        adapter = ReserveDeskAdapter(adapterDelegateManager, emptyList())
        recyclerView.adapter = adapter

        alertsContainer = view.findViewById(R.id.constraintLayout)
        alertIcon = view.findViewById<ImageView>(R.id.alertIcon)

        alertsRecyclerView = view.findViewById<RecyclerView>(R.id.alertsRecyclerView)
        alertsRecyclerView.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)

        alertsRecyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {

            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                    alertIcon.visibility = View.VISIBLE
                } else {
                    alertIcon.visibility = View.INVISIBLE
                }
            }
        })

        val snapHelper = LinearSnapHelper()
        snapHelper.attachToRecyclerView(alertsRecyclerView)

        alertsAdapter = AlertsAdapter(adapterDelegateManager, listOf(EmptyAlertItem()))
        alertsRecyclerView.adapter = alertsAdapter

        return view
    }

    override fun onResume() {
        super.onResume()
        presenter.resume(activity?.intent)
    }

    override fun onStop() {
        super.onStop()
        presenter.stop()
    }

    override fun onBackPressed(): Boolean {
        return presenter.onBackPressed()
    }

    override fun goBack() {
        baseEventHandler?.forceBackPressed()
    }

    override fun setToolbarTitle(title: String) {
        toolbarTitleTextView?.text = title
    }

    override fun setToolbarAddress(address: String) {
        toolbarAddressTextView?.text = address
        toolbarAddressImageView?.visibility = View.VISIBLE
        toolbarAddressTextView?.setOnClickListener {
            presenter.onToolbarAddressClicked()
        }
    }

    override fun showWorkspace(items: List<Item>) {
        adapter.updateItems(items)
    }

    override fun showFaq() {
        reserveDeskEventHandler?.showFaq()
    }

    override fun showCollapsedAlerts(scrollPos: Int, alerts: List<Alert>) {
        alertsContainer.visibility = if (alerts.isEmpty()) View.GONE else View.VISIBLE
        alertIcon.visibility = if (alerts.isEmpty()) View.GONE else View.VISIBLE
        alertsAdapter.updateItems(alerts.map { alert -> CollapsedAlertItem(alert.id, CollapsedAlertItem.ViewModel(alert)) })
        if (scrollPos != -1) {
            alertsRecyclerView.scrollToPosition(scrollPos)
        }
    }

    override fun showExpandedAlerts(scrollPos: Int, alerts: List<Alert>) {
        alertsContainer.visibility = if (alerts.isEmpty()) View.GONE else View.VISIBLE
        alertIcon.visibility = if (alerts.isEmpty()) View.GONE else View.VISIBLE
        alertsAdapter.updateItems(alerts.map { alert -> ExpandedAlertItem(alert.id, ExpandedAlertItem.ViewModel(alert)) })
        if (scrollPos != -1) {
            alertsRecyclerView.scrollToPosition(scrollPos)
        }
    }

    interface Events {
        fun showFaq()
    }
}