package se.fabege.app.waw.ui.recyclerview.adapter.items

import android.support.v7.widget.RecyclerView
import android.view.View
import se.fabege.app.R
import se.fabege.app.waw.ui.recyclerview.adapter.Item
import se.fabege.app.waw.ui.widget.CalendarHeaderView

class CalendarHeaderItem(val viewModel: ViewModel) : Item {

    data class ViewModel(val spans: List<String>)

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val calendarHeaderView = itemView.findViewById<CalendarHeaderView>(R.id.calendarHeaderView)
    }


    fun isContentSame(other: CalendarHeaderItem): Boolean {
        if (this.viewModel.spans.size != other.viewModel.spans.size) {
            return false
        }

        for ((index, item) in this.viewModel.spans.withIndex()) {
            if (item != other.viewModel.spans[index]) {
                return false
            }
        }

        return true
    }
}