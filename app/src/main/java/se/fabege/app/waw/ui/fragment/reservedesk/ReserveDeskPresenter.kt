package se.fabege.app.waw.ui.fragment.reservedesk

import kotlinx.coroutines.*
import se.fabege.app.R
import se.fabege.app.base.GlobalState
import se.fabege.app.waw.data.*
import se.fabege.app.waw.device.Device
import se.fabege.app.waw.domain.model.Alert
import se.fabege.app.waw.domain.model.Reservation
import se.fabege.app.waw.domain.model.WAWUser
import se.fabege.app.waw.domain.model.Workspace
import se.fabege.app.waw.misc.Injection
import se.fabege.app.waw.misc.Utils
import se.fabege.app.waw.ui.BasePresenterImpl
import se.fabege.app.waw.ui.activity.waw.WAWActivity
import se.fabege.app.waw.ui.recyclerview.adapter.Item

class ReserveDeskPresenter(private val view: ReserveDeskContract.View, private val workspaceId: String, device: Device) : BasePresenterImpl(view, device), ReserveDeskContract.Presenter {

    private val getWorkspace = Injection.provideGetWorkspaceUseCase()
    private val subscribeWorkspaceCalendar = Injection.provideSubscribeWorkspaceCalendarUseCase()
    private val reserveDesk = Injection.provideReserveDeskUseCase()
    private val getAlerts = Injection.provideGetAlertsUseCase()

    private val keyValueRepository = Injection.provideDefaultKeyValueRepository()

    private var alerts: List<Alert> = emptyList()

    private lateinit var workspace: Workspace
    private var calendarManager: CalendarManager? = null

    private var validationJob: Job? = null
    private lateinit var successfulAddedReservations: List<Reservation>
    private lateinit var successfulRemovedReservations: List<Reservation>

    init {
        view.setPresenter(this)
    }

    override fun onResume() {
        subscribeWorkspaceCalendar(workspaceId, this)

        scope.launch(dispatcherProvider.computation) {
            val alertsResult = getAlerts()
            withContext(dispatcherProvider.ui) {
                when (alertsResult) {
                    is Result.Success -> {
                        showAlerts(null, alertsResult.data)
                    }
                }
            }

        }
    }

    override fun onBackPressed(): Boolean {
        calendarManager?.let {
            if (it.isDirty()) {
                view.showMessage(AbortReservationMessage(this))
                return true
            }
        }
        return false
    }

    override fun onToolbarAddressClicked() {
        onMapClicked()
    }

    override fun onFaqButtonClicked() {
        device.trackPageView("WAW_FAQ")
        view.showFaq()
    }


    override fun onDataChanged() {
        scope.launch(dispatcherProvider.computation) {
            val result = getWorkspace(workspaceId)
            if (result is Result.Success) {
                workspace = result.data
                updateCalendarManager()
                withContext(dispatcherProvider.ui) {
                    view.setToolbarTitle(result.data.metaData.about.name)
                    view.setToolbarAddress(result.data.metaData.address.address)
                    if (!validationInProgress()) {
                        view.showWorkspace(createItems())
                    }
                }
            } else {
                withContext(dispatcherProvider.ui) {
                    view.showMessage(GenericErrorMessage())
                }
            }
        }
    }

    override fun onAlertsChanged(alerts: List<Alert>) {
        scope.launch(dispatcherProvider.ui) {
            showAlerts(null, alerts)
        }
    }

    override fun onRowDataClicked(ids: List<String>) {
        calendarManager?.let {
            for (id in ids) {
                it.onRowDataClicked(id)
            }
            view.showWorkspace(createItems())
        }
    }

    override fun onKeyButtonClicked() {
        calendarManager?.let {
            if (it.hasReservations()) {
                if (device.isParakeyAppInstalled()) {
                    device.trackEvent("WAW_Open_door")
                    device.openParakeyApp(wawUser.email, wawUser.generatePassword(), WAWActivity.OPEN_PARAKEY_APP_REQUEST_CODE)
                } else {
                    view.showMessage(TextMessage(GlobalState.getContext().getString(R.string.waw_dialog_generic_error_title), GlobalState.getContext().getString(R.string.waw_dialog_parakey_app_not_found_message)))
                }
            }
        }
    }

    override fun onReserveButtonClicked() {
        calendarManager?.let {
            val toRemove = it.getRemovedReservations()
            if (toRemove.isNotEmpty()) {
                view.showMessage(CancelReservationConfirmationMessage(toRemove, this))
            } else reserveDesk(it.getAddedReservations(), it.getRemovedReservations())
        }
    }

    override fun onMapClicked() {
        if (device.isGoogleMapAppInstalled()) {
            device.openGoogleMapApp(workspace.metaData.address.latitude, workspace.metaData.address.longitude)
        } else {
            view.showMessage(TextMessage(GlobalState.getContext().getString(R.string.waw_dialog_generic_error_title), GlobalState.getContext().getString(R.string.waw_dialog_google_maps_app_not_found_message)))
        }
    }

    override fun onEmailClicked() {
        if (device.canSendEmail()) {
            device.sendEmail(workspace.metaData.contactInfo.email)
        } else {
            view.showMessage(TextMessage(GlobalState.getContext().getString(R.string.waw_dialog_generic_error_title), GlobalState.getContext().getString(R.string.waw_dialog_email_app_not_found_message)))
        }
    }

    override fun onPhoneNumberClicked() {
        if (device.canCallPhoneNumber()) {
            device.callPhoneNumber(workspace.metaData.contactInfo.phoneNumber)
        } else {
            view.showMessage(TextMessage(GlobalState.getContext().getString(R.string.waw_dialog_generic_error_title), GlobalState.getContext().getString(R.string.waw_dialog_phone_app_not_found_message)))
        }
    }

    override fun onPositiveAbortReservationButtonClicked() {
        // Do a back press
        view.goBack()
    }

    override fun onPositiveCancelReservationButtonClicked() {
        calendarManager?.let {
            reserveDesk(it.getAddedReservations(), it.getRemovedReservations())
        }
    }

    override fun onNegativeAbortReservationButtonClicked() {
        // Do nothing
    }

    override fun onNegativeCancelReservationButtonClicked() {
        calendarManager?.let {
            it.rollback()
            view.showWorkspace(createItems())
        }
    }

    override fun onAddToCalendarButtonClicked() {
        if (device.isCalendarPermissionsGranted()) {
            onCalendarPermissionsGranted()
        } else {
            device.requestCalendarPermissions(this)
        }
    }

    override fun onCalendarPermissionsGranted() {
        addCalendarEvents()
    }

    private fun addCalendarEvents() {
        device.addCalendarEvents(successfulAddedReservations.map {
            se.fabege.app.waw.device.CalendarManager.Event(it.id, "WAW ${workspace.metaData.about.name}", "", workspace.metaData.address.address, it.timeSlot.start, it.timeSlot.end)
        })
    }

    private fun deleteCalendarEvents() {
        device.deleteCalendarEvents(successfulRemovedReservations.map { se.fabege.app.waw.device.CalendarManager.Event(it.id, "", "", "", 0, 0) })
    }

    override fun onCalendarPermissionDenied() {
    }

    private fun updateCalendarManager() {
        calendarManager = if (calendarManager == null) {
            CalendarManager(wawUser, workspace.calendar)
        } else {
            CalendarManager(wawUser, workspace.calendar, calendarManager!!.getAddedReservations(), calendarManager!!.getRemovedReservations())
        }
    }

    private fun createItems(): List<Item> {
        calendarManager?.let {
            return listOf(it.createCalendarHeaderItem(), it.createCalendarItem(),
                    it.createCalendarFooterItem(), Mapper.createWorkspaceMetaDataAboutItem(workspace),
                    Mapper.createWorkspaceMetaDataFacilityItem(workspace),
                    Mapper.createWorkspaceMetaDataCapacityItem(workspace),
                    Mapper.createWorkspaceMetaDataExtraItem(workspace),
                    Mapper.createWorkspaceMetaDataAddressItem(workspace),
                    Mapper.createWorkspaceMetaDataContactItem(workspace))
        }
        return emptyList()
    }

    private fun reserveDesk(toAdd: List<Reservation>, toRemove: List<Reservation>) {
        view.showBusyDialog()
        device.trackEvent("WAW_Booking_start")
        scope.launch(dispatcherProvider.computation) {
            val result = reserveDesk(wawUser, toAdd, toRemove)
            withContext(dispatcherProvider.ui) {
                when (result) {
                    is Result.Success -> {
                        successfulAddedReservations = toAdd
                        successfulRemovedReservations = toRemove
                        if (successfulRemovedReservations.isNotEmpty() && device.isCalendarPermissionsGranted()) {
                            deleteCalendarEvents()
                        }
                        if (successfulAddedReservations.isNotEmpty()) {
                            validateReservation()
                        } else {
                            device.trackEvent("WAW_Booking_success")
                            view.showMessage(ReservationConfirmationMessage(toAdd, toRemove, this@ReserveDeskPresenter))
                        }
                    }
                    is Result.Error -> {
                        device.trackEvent("WAW_Booking_fail")
                        view.showMessage(GenericErrorMessage())
                    }
                }
            }
        }
    }

    override fun onUserDataChanged(wawUser: WAWUser) {
        if (!validationInProgress()) {
            return
        } else {
            if (wawUser.validateReservations(successfulAddedReservations)) {
                validationJob?.cancel()

                scope.launch(dispatcherProvider.ui) {
                    device.trackEvent("WAW_Booking_success")
                    view.showMessage(ReservationConfirmationMessage(successfulAddedReservations, successfulRemovedReservations, this@ReserveDeskPresenter))
                    view.showWorkspace(createItems())
                }
            }
        }
    }

    private fun validationInProgress(): Boolean {
        if (validationJob == null) {
            return false
        }

        validationJob?.let {
            return it.isActive
        }

        return false
    }


    private fun validateReservation() {
        validationJob = GlobalScope.launch(dispatcherProvider.computation) {
            try {
                withTimeout(15000) {
                    repeat(1000) { i ->
                        delay(1000)
                    }
                }
            } catch (e: TimeoutCancellationException) {
                withContext(dispatcherProvider.ui) {
                    device.trackEvent("WAW_Booking_fail")
                    view.showMessage(GenericErrorMessage())
                }
                validationJob?.cancel()
            }
        }
    }

    override fun onCollapsedAlertClicked(id: String) {
        keyValueRepository.putBoolean("showExpandedAlerts", true)
        showAlerts(id, alerts)
    }

    override fun onExpandedAlertClicked(id: String) {
        keyValueRepository.putBoolean("showExpandedAlerts", false)
        showAlerts(id, alerts)
    }

    private fun showAlerts(id: String?, alerts: List<Alert>) {
        this.alerts = alerts.filter { alert -> alert.workspaceId == workspaceId && Utils.getNowInMillis() >= alert.publishAt && Utils.getNowInMillis() < alert.unpublishAt }

        if (keyValueRepository.getBoolean("showExpandedAlerts", true)) {
            view.showExpandedAlerts(getIndexOfId(id), this.alerts)
        } else {
            view.showCollapsedAlerts(getIndexOfId(id), this.alerts)
        }
    }

    private fun getIndexOfId(id: String?): Int {
        if (id == null) {
            return -1
        }

        for ((index, alert) in alerts.withIndex()) {
            if (id == alert.id) {
                return index
            }
        }
        return -1
    }
}