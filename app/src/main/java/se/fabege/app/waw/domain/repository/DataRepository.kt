package se.fabege.app.waw.domain.repository

import se.fabege.app.waw.data.Result
import se.fabege.app.waw.domain.model.*

interface DataRepository {
    suspend fun getUser(userId: String): Result<WAWUser>
    suspend fun getUsers(email: String): Result<List<WAWUser>>
    suspend fun getWorkspaces(): Result<List<Workspace>>
    suspend fun getWorkSpace(workspaceId: String): Result<Workspace>
    suspend fun handleReservations(userId: String, toAdd: List<Reservation>, toCancel: List<Reservation>): Result<Unit>
    suspend fun getFaq(): Result<Faq>
    suspend fun cleanUser(userId: String): Result<Unit>
    suspend fun getAppConfig(): Result<AppConfig>
    suspend fun getDeviceRegistrationToken(): Result<String>
    suspend fun setDeviceRegistrationToken(userId: String, token: String): Result<Unit>
    suspend fun getAlerts(): Result<List<Alert>>

    fun subscribeUser(userId: String, callback: UserCallback)
    fun subscribeAppConfig(callback: AppConfigCallback)
    fun subscribeWorkspaceCalendar(workspaceId: String, callback: Callback)
    fun subscribeAlerts(callback: AlertsCallback)
    fun subscribeWorkspacesMetadata(callback: MetadataCallback)
    fun unsubscribe()


    interface UserCallback {
        fun onUserDataChanged()
    }

    interface AppConfigCallback {
        fun onAppConfigChanged()
    }

    interface Callback {
        fun onDataChanged()
    }

    interface AlertsCallback {
        fun onAlertsChanged()
    }

    interface MetadataCallback {
        fun onMetadataChanged()
    }
}