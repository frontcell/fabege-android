package se.fabege.app.waw.ui.recyclerview.adapter.items

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import se.fabege.app.R
import se.fabege.app.waw.domain.model.WorkspaceMetadata
import se.fabege.app.waw.ui.recyclerview.adapter.Item

data class WorkspaceMetaDataContactItem(val viewModel: ViewModel) : Item {

    data class ViewModel(val contactInfo: WorkspaceMetadata.ContactInfo)

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val contactImageView = itemView.findViewById<ImageView>(R.id.contactImageView)
        val nameTextView = itemView.findViewById<TextView>(R.id.nameTextView)
        val titleTextView = itemView.findViewById<TextView>(R.id.titleTextView)
        val emailTextView = itemView.findViewById<TextView>(R.id.emailTextView)
        val phoneNumberTextView = itemView.findViewById<TextView>(R.id.phoneNumberTextView)
    }

    fun isContentSame(other: WorkspaceMetaDataContactItem): Boolean {
        return this.viewModel.contactInfo.name == other.viewModel.contactInfo.name &&
                this.viewModel.contactInfo.email == other.viewModel.contactInfo.email &&
                this.viewModel.contactInfo.title == other.viewModel.contactInfo.title &&
                this.viewModel.contactInfo.phoneNumber == other.viewModel.contactInfo.phoneNumber &&
                this.viewModel.contactInfo.imageUrl == other.viewModel.contactInfo.imageUrl
    }
}