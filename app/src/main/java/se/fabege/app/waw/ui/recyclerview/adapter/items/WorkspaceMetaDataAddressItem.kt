package se.fabege.app.waw.ui.recyclerview.adapter.items

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.TextView
import com.google.android.gms.maps.MapView
import se.fabege.app.R
import se.fabege.app.waw.ui.recyclerview.adapter.Item

data class WorkspaceMetaDataAddressItem(val viewModel: ViewModel) : Item {

    data class ViewModel(val address: String, val latitude: Float, val longitude: Float)

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val addressTextView = itemView.findViewById<TextView>(R.id.addressTextView)
        val mapView = itemView.findViewById<MapView>(R.id.mapView)
    }

    fun isContentSame(other: WorkspaceMetaDataAddressItem): Boolean {
        return this.viewModel.address == other.viewModel.address && this.viewModel.latitude == other.viewModel.latitude && this.viewModel.longitude == other.viewModel.longitude
    }
}