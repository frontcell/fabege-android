package se.fabege.app.waw.ui.fragment.onboarding

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter

class OnboardingAdapter(fm: FragmentManager?) : FragmentPagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {
        return OnboardingPageFactory.newInstance(position)
    }

    override fun getCount(): Int {
        return OnboardingPageFactory.count()
    }
}