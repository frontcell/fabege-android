package se.fabege.app.waw.domain.repository

interface KeyValueRepository {
    fun putBoolean(key: String, value: Boolean)
    fun getBoolean(key: String, defaultValue: Boolean = false): Boolean

    fun putString(key: String, value: String)
    fun getString(key:String): String?

    fun putLong(key: String, value: Long)
    fun getLong(key: String) : Long
}