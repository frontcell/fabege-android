package se.fabege.app.waw.ui.activity.redirect

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import se.fabege.app.waw.ui.activity.waw.WAWActivity

class RedirectActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        redirect(intent)
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        redirect(intent)
    }

    private fun redirect(intent: Intent?) {
        intent?.setClass(this, WAWActivity::class.java)
        startActivity(intent)
        finish()
    }
}