package se.fabege.app.waw.ui.recyclerview.adapter.items

import android.support.v7.widget.RecyclerView
import android.view.View
import se.fabege.app.waw.ui.recyclerview.adapter.Item

class EmptyAlertItem : Item {

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    }
}