package se.fabege.app.waw.data.datasource

import com.google.android.gms.tasks.Task
import com.google.firebase.auth.ActionCodeSettings
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import se.fabege.app.waw.data.Result
import se.fabege.app.waw.data.error.GetCurrentUserException
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine

object FirebaseAuthRemoteDataSource {
    init {
    }

    fun isSignedIn(): Result<Boolean> {

        val user = FirebaseAuth.getInstance().currentUser
        return if (user != null) {
            Result.Success(true)
        } else {
            Result.Success(false)
        }
    }

    fun getCurrentUser(): Result<FirebaseUser> {

        val user = FirebaseAuth.getInstance().currentUser
        return if (user != null) {
            Result.Success(user)
        } else {
            Result.Error(GetCurrentUserException())
        }
    }

    fun sendSignInLinkToEmail(email: String): Task<Void> {
        val actionCodeSettings = ActionCodeSettings.newBuilder()
                .setUrl("https://fabege.se/waw")
                .setHandleCodeInApp(true)
                .setIOSBundleId("se.fabege.fabege")
                .setAndroidPackageName("se.fabege.app", false, null)
                .build()
        return FirebaseAuth.getInstance().sendSignInLinkToEmail(email, actionCodeSettings)
    }

    fun isSignInWithEmailLink(emailLink: String): Result<Boolean> {
        return if (FirebaseAuth.getInstance().isSignInWithEmailLink(emailLink)) {
            Result.Success(true)
        } else {
            Result.Success(false)
        }
    }

    fun signInWithEmailLink(email: String, emailLink: String): Task<AuthResult> {
        return FirebaseAuth.getInstance().signInWithEmailLink(email, emailLink)
    }

    fun signInWithCustomToken(token: String): Task<AuthResult> {
        return FirebaseAuth.getInstance().signInWithCustomToken(token)
    }


    suspend fun signOut(): Result<Boolean> = suspendCoroutine { cont ->

        val callback = object: FirebaseAuth.AuthStateListener {
            override fun onAuthStateChanged(p0: FirebaseAuth) {

                    p0.removeAuthStateListener(this)
                    cont.resume(Result.Success(p0.currentUser == null))
            }
        }
        FirebaseAuth.getInstance().addAuthStateListener(callback)
        FirebaseAuth.getInstance().signOut()
    }
}