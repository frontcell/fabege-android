package se.fabege.app.waw.ui.dialog

import android.app.Dialog
import android.graphics.Color
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v7.app.AlertDialog
import android.text.Spannable
import android.text.SpannableString
import android.view.LayoutInflater
import android.view.View
import android.widget.TextView
import se.fabege.app.R
import se.fabege.app.waw.data.AbortReservationMessage
import se.fabege.app.waw.ui.widget.ColorUnderlineSpan

class AbortReservationMessageDialog : DialogFragment() {
    lateinit var abortReservationMessage: AbortReservationMessage

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return activity?.let {
            val builder = AlertDialog.Builder(it)
            val view = LayoutInflater.from(it).inflate(R.layout.waw_dialog_abort_reservation, null)
            val positive = view.findViewById<View>(R.id.positiveFrameLayout)
            val negative = view.findViewById<View>(R.id.negativeFrameLayout)
            val positiveTextView = view.findViewById<TextView>(R.id.positiveTextView)
            val negativeTextView = view.findViewById<TextView>(R.id.negativeTextView)
            val positiveText = positiveTextView.text
            val negativeText = negativeTextView.text


            val positiveSpannable = SpannableString(positiveText)
            positiveSpannable.setSpan(ColorUnderlineSpan(Color.parseColor("#8A2082"), 0, positiveText.length), 0, positiveText.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
            positiveTextView.text = positiveSpannable

            val negativeSpannable = SpannableString(negativeText)
            negativeSpannable.setSpan(ColorUnderlineSpan(Color.parseColor("#8A2082"), 0, negativeText.length), 0, negativeText.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
            negativeTextView.text = negativeSpannable
            positive.setOnClickListener {
                abortReservationMessage.clickListener.onPositiveAbortReservationButtonClicked()
                dialog.dismiss()
            }
            negative.setOnClickListener {
                abortReservationMessage.clickListener.onNegativeAbortReservationButtonClicked()
                dialog.dismiss()
            }
            builder.setView(view)
            builder.create()
        } ?: throw IllegalStateException("Activity cannot be null")
    }

    interface ClickListener {
        fun onPositiveAbortReservationButtonClicked()
        fun onNegativeAbortReservationButtonClicked()
    }
}