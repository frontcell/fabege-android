package se.fabege.app.waw.domain.usecase

import se.fabege.app.waw.data.Result
import se.fabege.app.waw.domain.model.AuthUser
import se.fabege.app.waw.domain.repository.AuthRepository
import se.fabege.app.waw.misc.exhaustive

class LogInUserWithEmailLinkViaApiUseCaseImpl(private val authRepository: AuthRepository) : LogInUserWithEmailLinkUseCase() {

    override suspend operator fun invoke(emailLink: String): Result<AuthUser> {
        val result = authRepository.loginWithCustomToken(extractToken(emailLink))
        return when (result) {
            is Result.Success -> result
            is Result.Error -> result
        }.exhaustive
    }

    private fun extractToken(emailLink: String): String {
        val prefix = "https://fabege-b28bd.firebaseapp.com/?token="
        return emailLink.substring(prefix.length)
    }
}