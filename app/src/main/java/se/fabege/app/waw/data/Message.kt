package se.fabege.app.waw.data

import se.fabege.app.R
import se.fabege.app.base.GlobalState
import se.fabege.app.waw.device.CalendarManager
import se.fabege.app.waw.domain.model.Reservation
import se.fabege.app.waw.misc.toDayMonthHour
import se.fabege.app.waw.misc.toHour
import se.fabege.app.waw.ui.dialog.*
import java.lang.StringBuilder
import java.util.*

abstract class Message

open class TextMessage(val title: String, val message: String) : Message()

class EmailNotEligibleMessage : Message()

class AbortReservationMessage(val clickListener: AbortReservationMessageDialog.ClickListener) : Message()

class CancelReservationConfirmationMessage(var canceled: List<Reservation>, val clickListener: CancelReservationConfirmationMessageDialog.ClickListener) : Message() {
    val message: String

    init {
        val builder = StringBuilder()
//        canceled = canceled.sortedBy { it.timeSlot.start }
//        for (reservation in canceled) {
//            builder.append(Date(reservation.timeSlot.start).toDayMonthHour())
//            builder.append("-")
//            builder.append(Date(reservation.timeSlot.end).toHour())
//            builder.append("\n")
//        }
        message = builder.toString()
    }
}

class ReservationConfirmationMessage(var added: List<Reservation>, var canceled: List<Reservation>, val clickListener: ReservationConfirmationMessageDialog.ClickListener) : Message() {
    val message: String

    init {
        added = added.sortedBy { it.timeSlot.start }
        val builder = StringBuilder()
//        if (added.isNotEmpty()) {
//            builder.append("Bokat\n")
//        }
        for (reservation in added) {
            builder.append(Date(reservation.timeSlot.start).toDayMonthHour())
            builder.append("-")
            builder.append(Date(reservation.timeSlot.end).toHour())
            builder.append("\n")
        }

        if (added.isEmpty() && canceled.isNotEmpty()) {
            builder.append("Bokningen är borttagen")
            builder.append("\n")
        }
//        if (canceled.isNotEmpty()) {
//            builder.append("\n Avbokat\n")
//        }
//        canceled = canceled.sortedBy { it.timeSlot.start }
//        for (reservation in canceled) {
//            builder.append(Date(reservation.timeSlot.start).toDayMonthHour())
//            builder.append("-")
//            builder.append(Date(reservation.timeSlot.end).toHour())
//            builder.append("\n")
//        }
        message = builder.toString()
    }
}

class DoorUnlockedMessage : Message()

class SelectCalendarMessage(val calendars: CalendarManager.Calendars, val clickListener: SelectCalendarMessageDialog.ClickListener) : Message()

class AppNotEligibleMessage(val clickListener: AppNotEligibleMessageDialog.ClickListener): Message()

class GenericErrorMessage : TextMessage(GlobalState.getContext().getString(R.string.waw_dialog_generic_error_title), GlobalState.getContext().getString(R.string.waw_dialog_generic_error_message))