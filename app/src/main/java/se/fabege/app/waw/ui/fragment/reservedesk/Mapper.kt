package se.fabege.app.waw.ui.fragment.reservedesk

import se.fabege.app.waw.domain.model.Workspace
import se.fabege.app.waw.ui.recyclerview.adapter.items.*

class Mapper {
    companion object {

        fun createWorkspaceMetaDataAboutItem(workspace: Workspace): WorkspaceMetaDataAboutItem {
            return WorkspaceMetaDataAboutItem(WorkspaceMetaDataAboutItem.ViewModel(workspace.metaData.about.name, workspace.metaData.media.imageUrl, workspace.metaData.about.description))
        }

        fun createWorkspaceMetaDataFacilityItem(workspace: Workspace): WorkspaceMetaDataFacilityItem {
            return WorkspaceMetaDataFacilityItem(WorkspaceMetaDataFacilityItem.ViewModel(workspace.metaData.facilityInfo))
        }

        fun createWorkspaceMetaDataCapacityItem(workspace: Workspace): WorkspaceMetaDataCapacityItem {
            return WorkspaceMetaDataCapacityItem(WorkspaceMetaDataCapacityItem.ViewModel(workspace.metaData.capacityInfo))
        }

        fun createWorkspaceMetaDataExtraItem(workspace: Workspace): WorkspaceMetaDataExtraItem {
            return WorkspaceMetaDataExtraItem(WorkspaceMetaDataExtraItem.ViewModel(workspace.metaData.about.extra))
        }

        fun createWorkspaceMetaDataAddressItem(workspace: Workspace): WorkspaceMetaDataAddressItem {
            return WorkspaceMetaDataAddressItem(WorkspaceMetaDataAddressItem.ViewModel(workspace.metaData.address.address, workspace.metaData.address.latitude, workspace.metaData.address.longitude))
        }

        fun createWorkspaceMetaDataContactItem(workspace: Workspace): WorkspaceMetaDataContactItem {
            return WorkspaceMetaDataContactItem(WorkspaceMetaDataContactItem.ViewModel(workspace.metaData.contactInfo))
        }
    }
}