package se.fabege.app.waw.domain.usecase

import se.fabege.app.waw.data.Result
import se.fabege.app.waw.domain.model.AuthUser
import se.fabege.app.waw.domain.repository.AuthRepository

class GetUserUseCaseImpl(private val authRepository: AuthRepository) : GetUserUseCase() {

    override operator fun invoke(): Result<AuthUser> {
        return authRepository.getUser()
    }
}