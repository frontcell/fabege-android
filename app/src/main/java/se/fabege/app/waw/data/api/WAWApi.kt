package se.fabege.app.waw.data.api

import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface WAWApi {
    @GET("authentificate")
    fun authentificate(@Query("uid") uid: String?): Deferred<Response<Unit>>

    companion object {
        const val ENDPOINT = "https://us-central1-fabege-b28bd.cloudfunctions.net/"
    }
}


