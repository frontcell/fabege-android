package se.fabege.app.waw.domain.usecase

import se.fabege.app.waw.data.Result
import se.fabege.app.waw.data.error.DataRepositoryException
import se.fabege.app.waw.data.error.WAWUserMissingException
import se.fabege.app.waw.domain.model.WAWUser
import se.fabege.app.waw.domain.repository.AuthRepository
import se.fabege.app.waw.domain.repository.CacheRepository
import se.fabege.app.waw.domain.repository.DataRepository
import se.fabege.app.waw.misc.Injection
import se.fabege.app.waw.misc.exhaustive

class GetWAWUserWithEmailUseCaseImpl(private val dataRepository: DataRepository) : GetWAWUserWithEmailUseCase() {
    override suspend fun invoke(email: String): Result<WAWUser> {

        val result = dataRepository.getUsers(email)
        return when (result) {
            is Result.Success -> {
                when (result.data.size) {
                    0 -> Result.Error(WAWUserMissingException())
                    1 -> {
                        Injection.provideCacheRepository().putWAWUser(result.data.first())
                        Result.Success(result.data.first())
                    }
                    else -> Result.Error(DataRepositoryException())
                }
            }
            is Result.Error -> result

        }.exhaustive
    }
}