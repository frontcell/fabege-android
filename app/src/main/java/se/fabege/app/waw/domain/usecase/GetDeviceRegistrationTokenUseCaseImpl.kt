package se.fabege.app.waw.domain.usecase

import se.fabege.app.waw.data.Result
import se.fabege.app.waw.domain.repository.DataRepository
import se.fabege.app.waw.misc.exhaustive

class GetDeviceRegistrationTokenUseCaseImpl(private val dataRepository: DataRepository) : GetDeviceRegistrationTokenUseCase() {
    override suspend fun invoke(): Result<String> {
        val result = dataRepository.getDeviceRegistrationToken()
        return when (result) {
            is Result.Success -> result
            is Result.Error -> result
        }.exhaustive
    }
}