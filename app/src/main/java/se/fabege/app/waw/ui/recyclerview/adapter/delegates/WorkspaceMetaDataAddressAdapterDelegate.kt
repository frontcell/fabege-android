package se.fabege.app.waw.ui.recyclerview.adapter.delegates

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MapStyleOptions
import com.google.android.gms.maps.model.MarkerOptions
import se.fabege.app.R
import se.fabege.app.waw.ui.recyclerview.adapter.Item
import se.fabege.app.waw.ui.recyclerview.adapter.ListAdapterDelegate
import se.fabege.app.waw.ui.recyclerview.adapter.items.WorkspaceMetaDataAddressItem

class WorkspaceMetaDataAddressAdapterDelegate(private val context: Context, private val clickListener: ClickListener) : ListAdapterDelegate<WorkspaceMetaDataAddressItem, Item, WorkspaceMetaDataAddressItem.ViewHolder>() {
    override fun isForViewType(item: Item, items: List<Item>, pos: Int): Boolean {
        return item is WorkspaceMetaDataAddressItem
    }

    override fun onCreateViewHolder(parent: ViewGroup): WorkspaceMetaDataAddressItem.ViewHolder {
        return WorkspaceMetaDataAddressItem.ViewHolder(LayoutInflater.from(context).inflate(R.layout.waw_rv_item_workspace_metadata_address, parent, false))
    }

    override fun onBindViewHolder(item: WorkspaceMetaDataAddressItem, viewHolder: WorkspaceMetaDataAddressItem.ViewHolder) {
        viewHolder.addressTextView.text = item.viewModel.address
        viewHolder.mapView.onCreate(null)
        viewHolder.mapView.getMapAsync { map ->
            map.uiSettings.isMapToolbarEnabled = false
            map.setOnMapClickListener {
                clickListener.onMapClicked()
            }
            map.setMapStyle(MapStyleOptions.loadRawResourceStyle(context, R.raw.style_json))
            val address = LatLng(item.viewModel.latitude.toDouble(), item.viewModel.longitude.toDouble())
            map.addMarker(MarkerOptions().position(address).icon(BitmapDescriptorFactory.fromResource(R.drawable.waw_icon_location_purple)))
            map.moveCamera(CameraUpdateFactory.newLatLngZoom(address, 15f))
        }
    }

    interface ClickListener {
        fun onMapClicked()
    }
}