package se.fabege.app.waw.domain.usecase

import se.fabege.app.waw.domain.repository.AuthRepository
import se.fabege.app.waw.data.Result
import se.fabege.app.waw.misc.exhaustive

class IsLogInWithEmailLinkUseCaseImpl(private val authRepository: AuthRepository) : IsLogInWithEmailLinkUseCase() {

    override operator fun invoke(emailLink: String): Boolean {
        val result = authRepository.isEmailLink(emailLink)

        return when (result) {
            is Result.Success -> result.data
            is Result.Error -> false
        }.exhaustive
    }
}