package se.fabege.app.waw.ui.recyclerview.adapter.delegates

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import se.fabege.app.R
import se.fabege.app.waw.ui.recyclerview.adapter.Item
import se.fabege.app.waw.ui.recyclerview.adapter.ListAdapterDelegate
import se.fabege.app.waw.ui.recyclerview.adapter.items.EmptyAlertItem

class EmptyAlertAdapterDelegate(private val context: Context) : ListAdapterDelegate<EmptyAlertItem, Item, EmptyAlertItem.ViewHolder>() {
    override fun isForViewType(item: Item, items: List<Item>, pos: Int): Boolean {
        return item is EmptyAlertItem
    }

    override fun onCreateViewHolder(parent: ViewGroup): EmptyAlertItem.ViewHolder {
        return EmptyAlertItem.ViewHolder(LayoutInflater.from(context).inflate(R.layout.waw_rv_item_empty_alert, parent, false))
    }

    override fun onBindViewHolder(item: EmptyAlertItem, viewHolder: EmptyAlertItem.ViewHolder) {
    }
}