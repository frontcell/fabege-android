package se.fabege.app.waw.ui.recyclerview.adapter

abstract class ListAdapter<T : List<Any>>(items: T):BaseAdapter<T>(items) {
    override fun getItemCount(): Int {
        return items.size
    }
}