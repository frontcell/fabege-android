package se.fabege.app.waw.ui.dialog

import android.app.Dialog
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v7.app.AlertDialog
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import se.fabege.app.R
import se.fabege.app.waw.data.SelectCalendarMessage

class SelectCalendarMessageDialog : DialogFragment() {
    lateinit var selectCalendarMessage: SelectCalendarMessage

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return activity?.let {
            val builder = AlertDialog.Builder(it)
            val view = LayoutInflater.from(it).inflate(R.layout.waw_dialog_select_calendar, null)
            val closeImageButton = view.findViewById<View>(R.id.closeImageButton)
            closeImageButton.setOnClickListener {
                dialog.dismiss()
            }
            val container = view.findViewById<LinearLayout>(R.id.calendarContainer)
            for ((index, calendar) in selectCalendarMessage.calendars.calendarDisplayNames.withIndex()) {
                val calendarItemView = createCalendarItemView(container)
                calendarItemView.text = calendar
                calendarItemView.tag = selectCalendarMessage.calendars.getCalendarId(index)
                calendarItemView.setOnClickListener {
                    selectCalendarMessage.clickListener.onCalendarClicked(it.tag as Long)
                    dismiss()
                }
                container.addView(calendarItemView)
            }
            builder.setView(view)
            builder.create()
        } ?: throw IllegalStateException("Activity cannot be null")
    }

    private fun createCalendarItemView(container: LinearLayout): TextView {
        val textView = LayoutInflater.from(context).inflate(R.layout.waw_dialog_select_calendar_item, container, false) as TextView
        return textView
    }

    interface ClickListener {
        fun onCalendarClicked(calendarId: Long)
    }
}