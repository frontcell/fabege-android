package se.fabege.app.waw.domain.usecase

import se.fabege.app.waw.data.Result
import se.fabege.app.waw.domain.repository.DataRepository
import se.fabege.app.waw.misc.exhaustive

class SetDeviceRegistrationTokenUseCaseImpl(private val dataRepository: DataRepository) : SetDeviceRegistrationTokenUseCase() {
    override suspend fun invoke(userId: String, token: String): Result<Unit> {
        val result = dataRepository.setDeviceRegistrationToken(userId, token)
        return when (result) {
            is Result.Success -> result
            is Result.Error -> result
        }.exhaustive
    }
}