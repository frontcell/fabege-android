package se.fabege.app.waw.domain.usecase

import se.fabege.app.waw.data.Result
import se.fabege.app.waw.data.error.DataRepositoryException
import se.fabege.app.waw.data.error.DocumentNotFoundException
import se.fabege.app.waw.data.error.WAWUserMissingException
import se.fabege.app.waw.domain.model.WAWUser
import se.fabege.app.waw.domain.repository.AuthRepository
import se.fabege.app.waw.domain.repository.CacheRepository
import se.fabege.app.waw.domain.repository.DataRepository
import se.fabege.app.waw.misc.Injection
import se.fabege.app.waw.misc.exhaustive

class GetWAWUserUseCaseImpl(private val authRepository: AuthRepository, private val dataRepository: DataRepository) : GetWAWUserUseCase() {
    override suspend fun invoke(): Result<WAWUser> {
        val result = authRepository.getUser()

        return when (result) {
            is Result.Success -> {
                val result2 = dataRepository.getUser(result.data.uid)
                return when (result2) {
                    is Result.Success -> {
                        Injection.provideCacheRepository().putWAWUser(result2.data)
                        Result.Success(result2.data)
                    }
                    is Result.Error -> {
                        when (result2.exception) {
                            is DocumentNotFoundException -> Result.Error(WAWUserMissingException())
                            else -> Result.Error(DataRepositoryException())
                        }
                    }
                }.exhaustive

            }
            is Result.Error -> result
        }.exhaustive
    }
}