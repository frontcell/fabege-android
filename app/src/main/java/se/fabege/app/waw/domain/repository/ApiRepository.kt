package se.fabege.app.waw.domain.repository

import se.fabege.app.waw.data.Result

interface ApiRepository {
    suspend fun sendEmailLink(uid: String): Result<Unit>
}