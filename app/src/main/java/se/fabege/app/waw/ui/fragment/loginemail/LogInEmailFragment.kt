package se.fabege.app.waw.ui.fragment.loginemail

import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.FrameLayout
import android.widget.ImageView
import se.fabege.app.R
import se.fabege.app.waw.misc.Utils
import se.fabege.app.waw.ui.fragment.ToolbarFragment


class LogInEmailFragment : ToolbarFragment(), TextWatcher {

    private var callback: Events? = null

    private var editText: EditText? = null
    private var emailImageView: ImageView? = null
    private var sendButton: FrameLayout? = null
    private var noiseImageView: ImageView? = null
    private var sendEnabled = false

    init {
        xmlLayoutId = R.layout.waw_fragment_login_email
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        callback = context as? Events
        if (callback == null) {
            throw ClassCastException("$context must implement LogInEmailFragment.Events")
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = super.onCreateView(inflater, container, savedInstanceState)
        editText = view?.findViewById(R.id.emailEditText)
        editText?.addTextChangedListener(this)
        emailImageView = view?.findViewById(R.id.emailImageView)
        sendButton = view?.findViewById(R.id.button2)
        noiseImageView = view?.findViewById(R.id.noiseImageView)
        sendButton?.setOnClickListener {
            if (sendEnabled) {
                callback?.onSendButtonClick(editText?.text.toString())
            }
        }

        disableSendButton()
        return view

    }

    override fun afterTextChanged(s: Editable?) {
    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
    }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        if (isValidEmail(s.toString())) {
            emailImageView?.setImageResource(R.drawable.waw_icon_email_black)
            enableSendButton()
        } else {
            emailImageView?.setImageResource(R.drawable.waw_icon_email_gray)
            disableSendButton()
        }
    }

    private fun disableSendButton() {
        sendEnabled = false
        noiseImageView?.visibility = View.INVISIBLE
        sendButton?.elevation = 0f
        sendButton?.setBackgroundColor(Color.parseColor("#E6E1DC"))
    }

    private fun enableSendButton() {
        sendEnabled = true
        noiseImageView?.visibility = View.VISIBLE
        sendButton?.elevation = Utils.dpToPx(10).toFloat()
        sendButton?.setBackgroundColor(Color.parseColor("#8A2082"))

    }

    fun isValidEmail(email: String): Boolean {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }

    interface Events {
        fun onSendButtonClick(email: String)
    }
}