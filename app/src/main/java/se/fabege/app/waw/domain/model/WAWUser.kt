package se.fabege.app.waw.domain.model

data class WAWUser(val id: String, val email: String, val keys: List<SuccessKey>, val error: List<ErrorKey>, val deviceRegistrationToken: String) {

    fun generatePassword(): String {
//        return id + "_321_fabege"
        return email + "_321_fabege"
    }

    fun validateReservations(reservations: List<Reservation>): Boolean {
        for (reservation in reservations) {
            if (!inKeys(reservation.id)) {
                return false
            }
        }
        return true
    }

    private fun inKeys(id: String): Boolean {
        for (key in keys) {
            if (id.contains(key.timeSlotId)) {
                return true
            }
        }
        return false
    }
}