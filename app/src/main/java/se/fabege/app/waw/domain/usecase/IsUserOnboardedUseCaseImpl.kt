package se.fabege.app.waw.domain.usecase

import se.fabege.app.waw.data.Result
import se.fabege.app.waw.domain.model.KeyValueRepositoryFactory

class IsUserOnboardedUseCaseImpl(private val getUser: GetUserUseCase, private val keyValueRepositoryFactory: KeyValueRepositoryFactory) : IsUserOnboardedUseCase() {

    override operator fun invoke(): Boolean {
        val result = getUser()
        return if (result is Result.Success) {
            val keyValueRepository = keyValueRepositoryFactory.get(result.data.uid)
            keyValueRepository.getBoolean("onboarded")
        } else {
            false
        }
    }
}