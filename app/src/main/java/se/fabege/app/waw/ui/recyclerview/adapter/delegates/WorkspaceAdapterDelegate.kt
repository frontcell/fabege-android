package se.fabege.app.waw.ui.recyclerview.adapter.delegates

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.squareup.picasso.Picasso
import se.fabege.app.R
import se.fabege.app.waw.ui.recyclerview.adapter.Item
import se.fabege.app.waw.ui.recyclerview.adapter.ListAdapterDelegate
import se.fabege.app.waw.ui.recyclerview.adapter.items.WorkspaceItem

class WorkspaceAdapterDelegate(private val context: Context, private val clickListener: ClickListener) : ListAdapterDelegate<WorkspaceItem, Item, WorkspaceItem.ViewHolder>() {
    override fun isForViewType(item: Item, items: List<Item>, pos: Int): Boolean {
        return item is WorkspaceItem
    }

    override fun onCreateViewHolder(parent: ViewGroup): WorkspaceItem.ViewHolder {
        return WorkspaceItem.ViewHolder(LayoutInflater.from(context).inflate(R.layout.waw_rv_item_workspace, parent, false))
    }

    override fun onBindViewHolder(item: WorkspaceItem, viewHolder: WorkspaceItem.ViewHolder) {
        viewHolder.itemView.setOnClickListener {
            clickListener.onWorkspaceClicked(item.id)
        }
        viewHolder.nameTextView.text = item.viewModel.name
        viewHolder.addressTextView.text = item.viewModel.address
        if (item.viewModel.iconUrl.isNotEmpty()) {
            Picasso.get().load(item.viewModel.iconUrl).into(viewHolder.iconImageView)
        }
        viewHolder.badgeImageView.visibility = if (item.viewModel.showBadge) View.VISIBLE else View.INVISIBLE
        viewHolder.badgeCountTextView.visibility = if (item.viewModel.showBadge) View.VISIBLE else View.INVISIBLE
        viewHolder.badgeCountTextView.text = item.viewModel.badgeCount.toString()
        viewHolder.alertImageView.visibility = if (item.viewModel.showAlert) View.VISIBLE else View.INVISIBLE
    }

    interface ClickListener {
        fun onWorkspaceClicked(id: String)
    }
}