package se.fabege.app.waw.ui.fragment.faq

import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import se.fabege.app.R
import se.fabege.app.waw.data.GenericErrorMessage
import se.fabege.app.waw.data.Result
import se.fabege.app.waw.domain.model.Faq
import se.fabege.app.waw.misc.Injection
import se.fabege.app.waw.ui.fragment.ToolbarFragment
import se.fabege.app.waw.ui.recyclerview.adapter.AdapterDelegateManager
import se.fabege.app.waw.ui.recyclerview.adapter.Item
import se.fabege.app.waw.ui.recyclerview.adapter.delegates.FaqSectionAdapterDelegate
import se.fabege.app.waw.ui.recyclerview.adapter.items.FaqSectionItem
import se.fabege.app.waw.ui.widget.DividerItemDecoration

class FaqFragment : ToolbarFragment() {

    private lateinit var adapter: FaqAdapter

    init {
        xmlLayoutId = R.layout.waw_fragment_faq
    }

    private val parentJob = Job()
    private val dispatcherProvider = Injection.provideCoroutinesDispatcherProvider()
    private val scope = CoroutineScope(dispatcherProvider.ui + parentJob)

    private val getFaq = Injection.provideGetFaqUseCase()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = super.onCreateView(inflater, container, savedInstanceState)


        val recyclerView = view!!.findViewById<RecyclerView>(R.id.recyclerView)
        recyclerView.layoutManager = LinearLayoutManager(context)

        val adapterDelegateManager = AdapterDelegateManager<List<Item>>()
        adapterDelegateManager.addDelegate(FaqSectionAdapterDelegate(context!!))

        adapter = FaqAdapter(adapterDelegateManager, emptyList())

        val dividerItemDecoration = DividerItemDecoration(context!!, LinearLayoutManager.VERTICAL)
        dividerItemDecoration.setDrawable(ContextCompat.getDrawable(context!!, R.drawable.waw_divider)!!)
        recyclerView.addItemDecoration(dividerItemDecoration)
        recyclerView.adapter = adapter


        return view
    }

    override fun onResume() {
        super.onResume()

        scope.launch(dispatcherProvider.computation) {
            val result = getFaq()
            withContext(dispatcherProvider.ui) {
                when (result) {
                    is Result.Success -> adapter.updateItems(createFaqItems(result.data))
                    is Result.Error -> showMessage(GenericErrorMessage())
                }
            }
        }
    }

    private fun createFaqItems(faq: Faq): List<Item> {
        return faq.faqSections.map { FaqSectionItem(FaqSectionItem.ViewModel(it)) }
    }
}