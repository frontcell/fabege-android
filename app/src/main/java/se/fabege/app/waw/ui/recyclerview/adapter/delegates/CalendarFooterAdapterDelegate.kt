package se.fabege.app.waw.ui.recyclerview.adapter.delegates

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import se.fabege.app.R
import se.fabege.app.waw.misc.Utils
import se.fabege.app.waw.ui.recyclerview.adapter.Item
import se.fabege.app.waw.ui.recyclerview.adapter.ListAdapterDelegate
import se.fabege.app.waw.ui.recyclerview.adapter.items.CalendarFooterItem

class CalendarFooterAdapterDelegate(private val context: Context, private val clickListener: ClickListener) : ListAdapterDelegate<CalendarFooterItem, Item, CalendarFooterItem.ViewHolder>() {
    override fun isForViewType(item: Item, items: List<Item>, pos: Int): Boolean {
        return item is CalendarFooterItem
    }

    override fun onCreateViewHolder(parent: ViewGroup): CalendarFooterItem.ViewHolder {
        return CalendarFooterItem.ViewHolder(LayoutInflater.from(context).inflate(R.layout.waw_rv_item_calendar_footer, parent, false))
    }

    override fun onBindViewHolder(item: CalendarFooterItem, viewHolder: CalendarFooterItem.ViewHolder) {
        if (item.viewModel.keyViewButtonEnabled) {
            viewHolder.keyView.setBackgroundColor(Color.parseColor("#8A2082"))
            viewHolder.keyView.elevation = Utils.dpToPx(10).toFloat()
            viewHolder.keyView.setOnClickListener { clickListener.onKeyButtonClicked() }
        } else {
            viewHolder.keyView.setBackgroundColor(Color.parseColor("#E6E1DC"))
            viewHolder.keyView.elevation = 0f
            viewHolder.keyView.setOnClickListener(null)

        }

        if (item.viewModel.reserveButtonEnabled) {
            viewHolder.reserveView.setBackgroundColor(Color.parseColor("#8A2082"))
            viewHolder.reserveView.elevation = Utils.dpToPx(10).toFloat()
            viewHolder.reserveView.setOnClickListener { clickListener.onReserveButtonClicked() }
        } else {
            viewHolder.reserveView.setBackgroundColor(Color.parseColor("#E6E1DC"))
            viewHolder.reserveView.elevation = 0f
            viewHolder.reserveView.setOnClickListener(null)
        }
        viewHolder.reserveViewText.text = item.viewModel.reserveButtonText

    }

    interface ClickListener {
        fun onKeyButtonClicked()
        fun onReserveButtonClicked()
    }
}

