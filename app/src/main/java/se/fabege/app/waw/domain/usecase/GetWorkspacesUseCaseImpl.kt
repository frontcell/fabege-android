package se.fabege.app.waw.domain.usecase

import se.fabege.app.waw.data.Result
import se.fabege.app.waw.domain.model.Workspace
import se.fabege.app.waw.domain.repository.DataRepository
import se.fabege.app.waw.misc.Injection
import se.fabege.app.waw.misc.exhaustive

class GetWorkspacesUseCaseImpl(private val dataRepository: DataRepository) : GetWorkspacesUseCase() {

    override suspend fun invoke(): Result<List<Workspace>> {
        val result = dataRepository.getWorkspaces()

        return when (result) {
            is Result.Success -> {
                Injection.provideCacheRepository().putWorkspaces(result.data)
                result
            }
            is Result.Error -> result
        }.exhaustive
    }
}