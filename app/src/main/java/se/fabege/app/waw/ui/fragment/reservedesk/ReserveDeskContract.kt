package se.fabege.app.waw.ui.fragment.reservedesk

import se.fabege.app.waw.device.Device
import se.fabege.app.waw.domain.model.Alert
import se.fabege.app.waw.domain.repository.DataRepository
import se.fabege.app.waw.ui.Back
import se.fabege.app.waw.ui.BasePresenter
import se.fabege.app.waw.ui.BaseView
import se.fabege.app.waw.ui.dialog.AbortReservationMessageDialog
import se.fabege.app.waw.ui.dialog.CancelReservationConfirmationMessageDialog
import se.fabege.app.waw.ui.dialog.ReservationConfirmationMessageDialog
import se.fabege.app.waw.ui.recyclerview.adapter.Item
import se.fabege.app.waw.ui.recyclerview.adapter.delegates.*
import se.fabege.app.waw.ui.recyclerview.adapter.items.CollapsedAlertItem
import se.fabege.app.waw.ui.widget.CalendarView

interface ReserveDeskContract {
    interface View : BaseView<Presenter>, Back {
        fun setToolbarTitle(title: String)
        fun setToolbarAddress(address: String)
        fun showWorkspace(items: List<Item>)
        fun showFaq()
        fun showCollapsedAlerts(scrollPos: Int, alerts: List<Alert>)
        fun showExpandedAlerts(scrollPos: Int, alerts: List<Alert>)
    }

    interface Presenter : BasePresenter, DataRepository.Callback, CalendarView.ClickListener,
            CalendarFooterAdapterDelegate.ClickListener, AbortReservationMessageDialog.ClickListener,
            ReservationConfirmationMessageDialog.ClickListener, CancelReservationConfirmationMessageDialog.ClickListener,
            Device.CalendarPermissionsCallback, WorkspaceMetaDataAddressAdapterDelegate.ClickListener,
            WorkspaceMetaDataContactAdapterDelegate.ClickListener,
            CollapsedAlertAdapterDelegate.ClickListener,
            ExpandedAlertAdapterDelegate.ClickListener{
        fun onBackPressed(): Boolean
        fun onToolbarAddressClicked()
        fun onFaqButtonClicked()
    }
}

