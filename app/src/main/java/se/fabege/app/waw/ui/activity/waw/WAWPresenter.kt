package se.fabege.app.waw.ui.activity.waw

import android.content.Intent
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import se.fabege.app.waw.data.EmailNotEligibleMessage
import se.fabege.app.waw.data.GenericErrorMessage
import se.fabege.app.waw.data.Result
import se.fabege.app.waw.data.TextMessage
import se.fabege.app.waw.device.Device
import se.fabege.app.waw.misc.Injection
import se.fabege.app.waw.misc.unwrap

class WAWPresenter(private val view: WAWContract.View, private val device: Device) : WAWContract.Presenter {

    private val parentJob = Job()
    private val dispatcherProvider = Injection.provideCoroutinesDispatcherProvider()
    private val scope = CoroutineScope(dispatcherProvider.ui + parentJob)

    private val getWAWUser = Injection.provideGetWAWUserUseCase()
    private val getWAWUserWithEmail = Injection.provideGetWAWUserWithEmailUseCase()
    private val isUserLoggedIn = Injection.provideIsUserLoggedInUseCase()
    private val isUserOnboarded = Injection.provideIsUserOnboardedUseCase()
    private val setUserOnboarded = Injection.provideSetUserOnboardedUseCase()
    private val sendEmailLinkToUser = Injection.provideSendEmailLinkToUserUserCase()
    private val isLogInWithEmailLink = Injection.provideIsLogInWithEmailLinkUseCase()
    private val logInUserWithEmailLink = Injection.provideLogInUserWithEmailLinkUseCase()
    private val isEmailEligible = Injection.provideIsEmailEligibleUseCase()

    private var coldStart: Boolean = false

    init {
        this.view.setPresenter(this)
    }

    override fun start(intent: Intent?) {
        coldStart = true
    }

    override fun resume(intent: Intent?) {
        if (coldStart) {
            resumeColdStart(intent)
            coldStart = false
        } else {
            resumeHotStart(intent)
        }
    }

    override fun stop() {
    }

    override fun destroy() {
    }

    private fun resumeColdStart(intent: Intent?) {
        if (isUserLoggedIn()) {
            if (device.isParakeyAppInstalled()) {
                onContinueFromLogInActivateParakey()
            } else {
                view.showLogInInstallParakey()
            }
            return
        }
        checkIntent(intent)
    }

    private fun resumeHotStart(intent: Intent?) {
        val userLoggedIn = isUserLoggedIn()
        if (userLoggedIn) {
            return
        }
        checkIntent(intent)
    }

    private fun checkIntent(intent: Intent?) {
        if (intentContainsLogInWithEmailLink(intent)) {
            loginUser(intent)
        } else {
            if (coldStart) {
                view.showLogInWelcome()
            }
        }
    }

    private fun intentContainsLogInWithEmailLink(intent: Intent?): Boolean {
        val data = getIntentData(intent)
        return if (data == null) {
            false
        } else {
            isLogInWithEmailLink(data)
        }
    }

    private fun getIntentData(intent: Intent?): String? {
        return if (intent != null && intent.data != null) {
            intent.data!!.toString()
        } else {
            null
        }
    }

    private fun loginUser(intent: Intent?) {
        val emailLink = getIntentData(intent)
        emailLink?.let {
            scope.launch(dispatcherProvider.computation) {
                val result = logInUserWithEmailLink(emailLink)
                withContext(dispatcherProvider.ui) {
                    when (result) {
                        is Result.Success -> {
                            val user = result.data
                            if (device.isParakeyAppInstalled()) {
                                onContinueFromLogInActivateParakey()
                            } else {
                                view.showLogInInstallParakey()
                            }
                        }
                        is Result.Error -> {
                            view.showMessage(TextMessage("Hoppsan", result.exception.toString()))
                        }
                    }
                }
            }
        }
    }

    /**
     * User clicked "continue" button
     */
    override fun onContinueFromLogInWelcome() {
        device.trackEvent("WAW_Register_start")
        view.showLogInEmail()
    }

    /**
     * User clicked "send email" button
     */
    override fun onContinueFromLogInEmail(email: String) {
        view.showBusyDialog()
        scope.launch(dispatcherProvider.computation) {
            val isEmailEligibleResult = isEmailEligible(email)
            withContext(dispatcherProvider.ui) {
                when (isEmailEligibleResult.unwrap()) {
                    Result.UnwrappedBoolean.TRUE -> {
                        device.trackEvent("WAW_Register_success")
                        withContext(dispatcherProvider.computation) {
                            val result = getWAWUserWithEmail(email)
                            if (result is Result.Success) {
                                withContext(dispatcherProvider.ui) {
                                    sendEmailLink(result.data.id)
                                }
                            } else {
                                withContext(dispatcherProvider.ui) {
                                    view.dismissBusyDialog()
                                    view.showMessage(GenericErrorMessage())
                                }
                            }
                        }
                    }
                    Result.UnwrappedBoolean.FALSE -> {
                        view.dismissBusyDialog()
                        view.showMessage(EmailNotEligibleMessage())
                    }
                    Result.UnwrappedBoolean.ERROR -> {
                        view.dismissBusyDialog()
                        view.showMessage(GenericErrorMessage())
                    }
                }
            }
        }
    }

    /**
     * User installed Paraykey app
     */
    override fun onContinueFromLogInInstallParakey() {
        scope.launch(dispatcherProvider.computation) {
            val result = getWAWUser()
            if (result is Result.Success) {
                withContext(dispatcherProvider.ui) {
                    view.showLogInActivateParakey(result.data.email, result.data.generatePassword())
                }
            } else {
                withContext(dispatcherProvider.ui) {
                    view.showMessage(GenericErrorMessage())
                }
            }
        }
    }

    private fun sendEmailLink(email: String) {
        scope.launch(dispatcherProvider.computation) {
            val sendEmailLinkToUserResult = sendEmailLinkToUser(email)
            withContext(dispatcherProvider.ui) {
                view.dismissBusyDialog()
                when (sendEmailLinkToUserResult) {
                    is Result.Success -> view.showLogInEmailSent()
                    is Result.Error -> view.showMessage(TextMessage("Hoppsan", sendEmailLinkToUserResult.exception.toString()))
                }
            }
        }
    }

    /**
     * User clicked "activate/connect" button
     */
    override fun onContinueFromLogInActivateParakey() {
        if (shouldShowOnboarding()) {
            view.showOnboarding()
            setUserOnboarded()
        } else {
            onContinueFromLogInDone()
        }
    }

    /**
     * User clicked "start" button
     */
    override fun onContinueFromLogInDone() {
        view.showSelectWorkspace()
    }

    private fun shouldShowOnboarding(): Boolean {
        return !isUserOnboarded()
    }

    /**
     * User clicked "done" button
     */
    override fun onContinueFromOnboardingDone() {
        view.showLogInDone()
    }
}