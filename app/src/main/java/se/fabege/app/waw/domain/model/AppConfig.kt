package se.fabege.app.waw.domain.model

data class AppConfig(val minAppVersion: Int)