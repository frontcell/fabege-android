package se.fabege.app.waw.ui.recyclerview.adapter.items

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import se.fabege.app.R
import se.fabege.app.waw.ui.recyclerview.adapter.Item

data class WorkspaceMetaDataAboutItem(val viewModel: ViewModel) : Item {

    data class ViewModel(val name: String, val imageUrl: String, val description: String)

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val titleTextView = itemView.findViewById<TextView>(R.id.workspaceTitleTextView)
        val imageView = itemView.findViewById<ImageView>(R.id.workspaceImageView)
        val descTextView = itemView.findViewById<TextView>(R.id.workspaceDescTextView)
    }

    fun isContentSame(other: WorkspaceMetaDataAboutItem): Boolean {
        return this.viewModel.name == other.viewModel.name && this.viewModel.imageUrl == other.viewModel.imageUrl && this.viewModel.description == other.viewModel.description
    }
}