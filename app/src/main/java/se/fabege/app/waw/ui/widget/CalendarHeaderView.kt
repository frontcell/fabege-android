package se.fabege.app.waw.ui.widget

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.FrameLayout
import android.widget.LinearLayout
import android.widget.TextView
import se.fabege.app.R
import se.fabege.app.waw.ui.recyclerview.adapter.items.CalendarHeaderItem

class CalendarHeaderView(context: Context, attrs: AttributeSet) : FrameLayout(context, attrs) {
    private var headerContainer: LinearLayout

    init {
        LayoutInflater.from(context).inflate(R.layout.waw_view_calendar_header, this, true)
        headerContainer = findViewById(R.id.linearLayout1)
    }

    fun update(viewModel: CalendarHeaderItem.ViewModel) {
        headerContainer.removeAllViews()
        for ((index, span) in viewModel.spans.withIndex()) {
            headerContainer.addView(createSpanView(span))
            if (index != viewModel.spans.lastIndex) {
                headerContainer.addView(LayoutInflater.from(context).inflate(R.layout.waw_view_calendar_space_6dp, headerContainer, false))
            }
        }
    }

    private fun createSpanView(span: String): View {
        val inflater = LayoutInflater.from(context)
        val view = inflater.inflate(R.layout.waw_view_calendar_date_span, headerContainer, false)
        (view as TextView).text = "kl $span"
        return view
    }
}