package se.fabege.app.waw.domain.usecase

import se.fabege.app.waw.data.Result
import se.fabege.app.waw.domain.model.*
import se.fabege.app.waw.domain.repository.DataRepository

abstract class GetUserUseCase {
    abstract operator fun invoke(): Result<AuthUser>
}

abstract class IsUserLoggedInUseCase {
    abstract operator fun invoke(): Boolean
}

abstract class IsUserOnboardedUseCase {
    abstract operator fun invoke(): Boolean
}

abstract class SetUserOnboardedUseCase {
    abstract operator fun invoke()
}

abstract class SendEmailLinkToUserUseCase {
    abstract suspend operator fun invoke(email: String): Result<Unit>
}

abstract class IsLogInWithEmailLinkUseCase {
    abstract operator fun invoke(emailLink: String): Boolean
}

abstract class LogInUserWithEmailLinkUseCase {
    abstract suspend operator fun invoke(emailLink: String): Result<AuthUser>
}

abstract class IsEmailEligibleUseCase {
    abstract suspend operator fun invoke(email: String): Result<Boolean>
}

abstract class LogoutUserUseCase {
    abstract suspend operator fun invoke(): Result<Boolean>
}

abstract class GetWorkspacesUseCase {
    abstract suspend operator fun invoke(): Result<List<Workspace>>
}

abstract class GetWorkspaceUseCase {
    abstract suspend operator fun invoke(id: String): Result<Workspace>
}

abstract class SubcribeWorkspaceCalendarUseCase {
    abstract operator fun invoke(workspaceId: String, callback: DataRepository.Callback)
}

abstract class SubscribeWAWUserUseCase {
    abstract operator fun invoke(userId: String, callback: DataRepository.UserCallback)
}

abstract class SubscribeAppConfigUseCase {
    abstract operator fun invoke(callback: DataRepository.AppConfigCallback)
}

abstract class SubscribeAlertsUseCase {
    abstract operator fun invoke(callback: DataRepository.AlertsCallback)
}

abstract class SubscribeWorkspacesMetadataUseCase {
    abstract operator fun invoke(callback: DataRepository.MetadataCallback)
}

abstract class UnsubscribeUseCase {
    abstract operator fun invoke()
}

abstract class GetWAWUserUseCase {
    abstract suspend operator fun invoke(): Result<WAWUser>
}

abstract class GetWAWUserWithEmailUseCase {
    abstract suspend operator fun invoke(email: String): Result<WAWUser>
}

abstract class ReserveDeskUseCase {
    abstract suspend operator fun invoke(user: WAWUser, toAdd: List<Reservation>, toRemove: List<Reservation>): Result<Unit>
}

abstract class GetFaqUseCase {
    abstract suspend operator fun invoke(): Result<Faq>
}

abstract class CleanWAWUserUseCase {
    abstract suspend operator fun invoke(user: WAWUser): Result<Unit>
}

abstract class TakeOffUseCase {
    abstract suspend operator fun invoke(): Result<WAWUser>
}

abstract class IsAppEligibleUseCase {
    abstract suspend operator fun invoke(): Result<Unit>
}

abstract class SetDeviceRegistrationTokenUseCase {
    abstract suspend operator fun invoke(userId: String, token: String): Result<Unit>
}

abstract class GetDeviceRegistrationTokenUseCase {
    abstract suspend operator fun invoke(): Result<String>
}

abstract class GetAlertsUseCase {
    abstract suspend operator fun invoke(): Result<List<Alert>>
}