package se.fabege.app.waw.ui.recyclerview.adapter.items

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.TextView
import se.fabege.app.R
import se.fabege.app.waw.domain.model.Alert
import se.fabege.app.waw.ui.recyclerview.adapter.Item

data class CollapsedAlertItem(val id: String, val viewModel: ViewModel) : Item {

    data class ViewModel(val alert: Alert)

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val title: TextView = itemView.findViewById(R.id.alertTitle)
    }
}