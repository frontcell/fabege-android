package se.fabege.app.waw.data.datasource

import com.google.android.gms.tasks.Task
import com.google.firebase.firestore.*
import se.fabege.app.waw.data.firebase.DayTransaction
import se.fabege.app.waw.data.firebase.UserTransaction
import se.fabege.app.waw.domain.repository.DataRepository
import se.fabege.app.waw.misc.Utils

class FirebaseFirestoreRemoteDataSource {

    companion object {
        private const val USER = "user"
        private const val PROPERTY = "property"
        private const val SCHEDULES = "schedules"
        private const val DAYS = "days"
        private const val FAQ = "faq"
        private const val APP_CONFIG = "appConfig"
        private const val ALERTS = "alerts"
    }

    fun getUser(id: String): Task<DocumentSnapshot> {
        val db = FirebaseFirestore.getInstance()

        return db.collection(USER).document(id).get()
    }

    fun getUsers(email: String): Task<QuerySnapshot> {
        val db = FirebaseFirestore.getInstance()

        return db.collection(USER)
                .whereEqualTo("email", email).get()
    }

    fun getProperties(): Task<QuerySnapshot> {
        val db = FirebaseFirestore.getInstance()

        return db.collection(PROPERTY).get()
    }

    fun getProperty(id: String): Task<DocumentSnapshot> {
        val db = FirebaseFirestore.getInstance()

        return db.collection(PROPERTY).document(id).get()
    }

    fun getSchedule(workspaceId: String): Task<QuerySnapshot> {
        val db = FirebaseFirestore.getInstance()

        return db.collection(SCHEDULES).document(workspaceId).collection(DAYS).get()
    }

    private enum class Action {
        ADD, REMOVE, NA
    }


    private class Id(slotId: String) {
        var workspaceId: String
        var daysId: String
        var mapId: String
        var slotIndex: Int
        var numOfSlots: Int

        init {
            val paths = slotId.split("/")
            workspaceId = paths[0]
            daysId = paths[1]
            mapId = "$workspaceId/$daysId"
            slotIndex = paths[2].toInt()
            if (paths.size > 3) {
                numOfSlots = paths[3].toInt()
            } else {
                numOfSlots = -1
            }
        }
    }

    fun applyToUserClean(userId: String): Task<Unit> {
        val db = FirebaseFirestore.getInstance()

        return db.runTransaction { transaction ->
            // read data
            val documentSnapshot = transaction.get(db.collection(USER).document(userId))
            val user = documentSnapshot.toObject(UserTransaction::class.java)

            if (user != null) {
                // write data
                val endOfYesterday = Utils.getEndOfDayInMillis() - 86400000L
                val filtered = user.accessKeys?.filter {
                    val id = Id(it.path!!)
                    Utils.parseDate(id.daysId).time >= endOfYesterday
                }
                user.accessKeys?.clear()
                user.accessKeys?.addAll(filtered ?: arrayListOf())

                user.failedAccessKeys?.clear()

                transaction.set(db.collection(USER).document(userId), user, SetOptions.merge())
            }
            Unit
        }
    }

    fun applyToUser(userId: String, deviceToken: String): Task<Unit> {
        val db = FirebaseFirestore.getInstance()

        return db.runTransaction { transaction ->
            // read data
            val documentSnapshot = transaction.get(db.collection(USER).document(userId))
            val user = documentSnapshot.toObject(UserTransaction::class.java)

            user?.deviceToken = deviceToken

            transaction.set(db.collection(USER).document(userId), user!!, SetOptions.merge())
            Unit
        }
    }

    fun applyToSchedule(userId: String, addIds: List<String>, removeIds: List<String>): Task<Unit> {
        val db = FirebaseFirestore.getInstance()

        // id path legends
        // e.g. {workspace id}/{date}/{slot index}/{num of slots}

        val map: MutableMap<String, Array<Action>> = HashMap()

        // Create map

        for (id in listOf(addIds, removeIds).flatten().map { slotId -> Id(slotId) }) {
            if (!map.containsKey(id.mapId)) {
                map[id.mapId] = Array(id.numOfSlots) { Action.NA }
            }
        }
        // Populate with data
        for (id in addIds.map { slotId -> Id(slotId) }) {
            map[id.mapId]!![id.slotIndex] = Action.ADD
        }

        for (id in removeIds.map { slotId -> Id(slotId) }) {
            map[id.mapId]!![id.slotIndex] = Action.REMOVE
        }

        // debug print
//        for (entry in map) {
//            print("key:" + entry.key + "values:")
//            for (value in entry.value) {
//                print("$value ")
//            }
//            println()
//        }

        return db.runTransaction { transaction ->
            // read data
            val snapshots = map.keys.map { mapId ->
                val paths = mapId.split("/")
                Pair(mapId, transaction.get(db.collection(SCHEDULES).document(paths[0]).collection(DAYS).document(paths[1])))
            }

            // write data
            for ((mapId, document) in snapshots) {

                val day = document.toObject(DayTransaction::class.java)

                for ((index, action) in map[mapId]!!.withIndex()) {
                    when (action) {
                        Action.ADD -> day?.slots?.get(index)?.ids?.add(userId)
                        Action.REMOVE -> day?.slots?.get(index)?.ids?.remove(userId)
                    }
                }
                val paths = mapId.split("/")
                transaction.set(db.collection(SCHEDULES).document(paths[0]).collection(DAYS).document(paths[1]), day!!, SetOptions.merge())
            }
        }
    }

    fun subscribeOnSchedule(id: String, callback: DataRepository.Callback): ListenerRegistration {
        val db = FirebaseFirestore.getInstance()

        val query = db.collection(SCHEDULES).document(id).collection(DAYS)

        return query.addSnapshotListener { snapshot, e ->
            if (e == null) {
                val source = if (snapshot != null && snapshot.metadata.hasPendingWrites()) "Local" else "Server"
                if (source == "Server") {
                    callback.onDataChanged()
                }
            }
        }
    }

    fun subscribeOnUser(id: String, callback: DataRepository.UserCallback): ListenerRegistration {
        val db = FirebaseFirestore.getInstance()

        val query = db.collection(USER).document(id)

        return query.addSnapshotListener { snapshot, e ->
            if (e == null) {
                val source = if (snapshot != null && snapshot.metadata.hasPendingWrites()) "Local" else "Server"
                if (source == "Server") {
                    callback.onUserDataChanged()
                }
            }
        }
    }

    fun subscribeOnAppConfig(callback: DataRepository.AppConfigCallback): ListenerRegistration {
        val db = FirebaseFirestore.getInstance()

        val query = db.collection(APP_CONFIG).document("android")

        return query.addSnapshotListener { snapshot, e ->
            if (e == null) {
                val source = if (snapshot != null && snapshot.metadata.hasPendingWrites()) "Local" else "Server"
                if (source == "Server") {
                    callback.onAppConfigChanged()
                }
            }
        }
    }

    fun subscribeOnAlerts(callback: DataRepository.AlertsCallback): ListenerRegistration {
        val db = FirebaseFirestore.getInstance()

        val query = db.collection(ALERTS)

        return query.addSnapshotListener { snapshot, e ->
            if (e == null) {
                val source = if (snapshot != null && snapshot.metadata.hasPendingWrites()) "Local" else "Server"
                if (source == "Server") {
                    callback.onAlertsChanged()
                }
            }
        }
    }

    fun subscribeOnWorkspacesMetadata(callback: DataRepository.MetadataCallback): ListenerRegistration {
        val db = FirebaseFirestore.getInstance()

        val query = db.collection(PROPERTY)

        return query.addSnapshotListener { snapshot, e ->
            if (e == null) {
                val source = if (snapshot != null && snapshot.metadata.hasPendingWrites()) "Local" else "Server"
                if (source == "Server") {
                    callback.onMetadataChanged()
                }
            }
        }
    }

    fun getFaqs(): Task<QuerySnapshot> {
        val db = FirebaseFirestore.getInstance()

        return db.collection(FAQ).get()
    }

    fun getAppConfig(): Task<DocumentSnapshot> {
        val db = FirebaseFirestore.getInstance()

        return db.collection(APP_CONFIG).document("android").get()
    }

    fun getAlerts(): Task<QuerySnapshot> {
        val db = FirebaseFirestore.getInstance()
        return db.collection(ALERTS).get()
    }
}