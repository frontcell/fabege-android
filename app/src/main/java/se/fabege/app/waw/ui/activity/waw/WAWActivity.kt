package se.fabege.app.waw.ui.activity.waw

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import se.fabege.app.R
import se.fabege.app.waw.data.*
import se.fabege.app.waw.device.Device
import se.fabege.app.waw.misc.Injection
import se.fabege.app.waw.ui.Back
import se.fabege.app.waw.ui.activity.BaseActivity
import se.fabege.app.waw.ui.dialog.*
import se.fabege.app.waw.ui.fragment.BaseFragment
import se.fabege.app.waw.ui.fragment.loginactivateparakey.LogInActivateParakeyFragment
import se.fabege.app.waw.ui.fragment.logindone.LogInDoneFragment
import se.fabege.app.waw.ui.fragment.loginemail.LogInEmailFragment
import se.fabege.app.waw.ui.fragment.loginemailsent.LogInEmailSentFragment
import se.fabege.app.waw.ui.fragment.logininstallparakey.LogInInstallParakeyFragment
import se.fabege.app.waw.ui.fragment.loginwelcome.LogInWelcomeFragment
import se.fabege.app.waw.ui.fragment.onboarding.OnboardingFragment
import se.fabege.app.waw.ui.fragment.reservedesk.ReserveDeskFragment
import se.fabege.app.waw.ui.fragment.selectworkspace.SelectWorkspaceFragment

class WAWActivity : BaseActivity(), WAWContract.View,
        BaseFragment.Events,
        LogInWelcomeFragment.Events,
        LogInEmailFragment.Events,
        LogInEmailSentFragment.Events,
        LogInInstallParakeyFragment.Events,
        LogInActivateParakeyFragment.Events,
        LogInDoneFragment.Events,
        OnboardingFragment.Events,
        SelectWorkspaceFragment.Events,
        ReserveDeskFragment.Events {

    companion object {
        const val ACTIVATE_PARAKEY_APP_REQUEST_CODE = 6745
        const val OPEN_PARAKEY_APP_REQUEST_CODE = 3644
        const val REQUEST_CALENDAR_PERMISSIONS_CODE = 4474
    }

    var calendarPermissionsCallback: Device.CalendarPermissionsCallback? = null

    private lateinit var presenter: WAWContract.Presenter

    override fun setPresenter(presenter: WAWContract.Presenter) {
        this.presenter = presenter
        presenter.start(intent)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.waw_activity_waw)

        WAWPresenter(this, Injection.provideDevice(this))
    }

    override fun onResume() {
        super.onResume()
        presenter.resume(intent)
    }

    override fun onNewIntent(intent: Intent) {
        super.onNewIntent(intent)
        setIntent(intent)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            ACTIVATE_PARAKEY_APP_REQUEST_CODE -> {
                onParakeyAppActivated()
            }
            OPEN_PARAKEY_APP_REQUEST_CODE -> {
                if (resultCode == Activity.RESULT_OK) {
                    showMessage(DoorUnlockedMessage())
                } else {
                    showMessage(TextMessage(getString(R.string.waw_dialog_generic_error_title), getString(R.string.waw_dialog_door_unlock_failed)))
                }
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        if (requestCode == REQUEST_CALENDAR_PERMISSIONS_CODE) {
            when (PermissionResult(grantResults).result) {
                PermissionResult.Result.GRANTED -> calendarPermissionsCallback?.onCalendarPermissionsGranted()
                else -> calendarPermissionsCallback?.onCalendarPermissionDenied()
            }
        }
    }

    override fun onBackPressed() {
        for (fragment in supportFragmentManager.fragments) {
            if (fragment is Back) {
                val handled = fragment.onBackPressed()
                if (handled) {
                    return
                }
            }
        }
        super.onBackPressed()
    }

    override fun forceBackPressed() {
        super.onBackPressed()
    }

    // LogInWelcomeFragment Events
    override fun onContinueButtonClick() {
        presenter.onContinueFromLogInWelcome()
    }

    // LogInEmailFragment Events
    override fun onSendButtonClick(email: String) {
        presenter.onContinueFromLogInEmail(email)
    }

    // LogInInstallParakeyFragment Events
    override fun onParaykeyAppInstalled() {
        presenter.onContinueFromLogInInstallParakey()
    }

    // LogInActivateParakeyFragment Events
    override fun onParakeyAppActivated() {
        presenter.onContinueFromLogInActivateParakey()
    }

    // LogInDoneFragment Events
    override fun onLoginDoneButtonClick() {
        presenter.onContinueFromLogInDone()
    }

    // OnboardingFragment Events
    override fun onOnboardingDoneButtonClick() {
        presenter.onContinueFromOnboardingDone()
    }

    // SelectWorkspaceFragment Events
    override fun showReserveDesk(workspaceId: String, deskId: String) {
        val fragment = ReserveDeskFragment.newInstance(workspaceId, deskId)
        replaceFragmentAddToBackStack(fragment)
    }

    override fun showFaq() {
        val fragment = se.fabege.app.waw.ui.fragment.faq.FaqFragment()
        replaceFragmentAddToBackStack(fragment)
    }

    override fun showLogInWelcome() {
        val fragment = LogInWelcomeFragment()
        replaceFragment(fragment)
    }

    override fun showLogInEmail() {
        val fragment = LogInEmailFragment()
        replaceFragment(fragment)
    }

    override fun showLogInEmailSent() {
        val fragment = LogInEmailSentFragment()
        replaceFragment(fragment)
    }

    override fun showLogInInstallParakey() {
        val fragment = LogInInstallParakeyFragment()
        replaceFragment(fragment)
    }

    override fun showLogInActivateParakey(email: String, password: String) {
        val fragment = LogInActivateParakeyFragment.newInstance(email, password)
        replaceFragment(fragment)
    }

    override fun showLogInDone() {
        val fragment = LogInDoneFragment()
        replaceFragment(fragment)
    }

    override fun showOnboarding() {
        val fragment = OnboardingFragment()
        replaceFragment(fragment)
    }

    override fun showSelectWorkspace() {
        val fragment = SelectWorkspaceFragment()
        replaceFragment(fragment)
    }

    override fun exit() {
        finish()
    }

    override fun showMessage(message: Message) {
        dismissBusyDialog()
        when (message) {
            is TextMessage -> showTextMessage(message)
            is EmailNotEligibleMessage -> showEmailNotEligibleMessage()
            is ReservationConfirmationMessage -> showReservationConfirmationMessage(message)
            is CancelReservationConfirmationMessage -> showCancelReservationConfirmationMessage(message)
            is AbortReservationMessage -> showAbortReservationMessage(message)
            is DoorUnlockedMessage -> showDoorUnlockedMessage()
            is SelectCalendarMessage -> showSelectCalendarMessage(message)
            is AppNotEligibleMessage -> showAppNotEligibleMessage(message)
        }
    }

    override fun showBusyDialog() {
        var dialog = supportFragmentManager.findFragmentByTag("busyDialog")
        if (dialog != null) {
            return
        }
        dialog = BusyDialog()
        dialog.isCancelable = false
        dialog.show(supportFragmentManager, "busyDialog")
    }

    override fun dismissBusyDialog() {
        val dialog = supportFragmentManager.findFragmentByTag("busyDialog")

        dialog?.let {
            it as BusyDialog
            it.dismissAllowingStateLoss()
        }
    }

    private fun showTextMessage(textMessage: TextMessage) {
        val dialog = TextMessageDialog()
        dialog.textMessage = textMessage
        dialog.isCancelable = false
        dialog.show(supportFragmentManager, "textMessageDialog")
    }

    private fun showEmailNotEligibleMessage() {
        val dialog = EmailNotEligibleMessageDialog()
        dialog.isCancelable = false
        dialog.show(supportFragmentManager, "emailNotEligibleMessageDialog")
    }

    private fun showCancelReservationConfirmationMessage(cancelReservationConfirmationMessage: CancelReservationConfirmationMessage) {
        val dialog = CancelReservationConfirmationMessageDialog()
        dialog.isCancelable = false
        dialog.cancelReservationConfirmationMessage = cancelReservationConfirmationMessage
        dialog.show(supportFragmentManager, "cancelReservationConfirmationDialog")
    }

    private fun showReservationConfirmationMessage(reservationConfirmationMessage: ReservationConfirmationMessage) {
        val dialog = ReservationConfirmationMessageDialog()
        dialog.isCancelable = false
        dialog.reservationConfirmationMessage = reservationConfirmationMessage
        dialog.show(supportFragmentManager, "reservationConfirmationDialog")
    }

    private fun showAbortReservationMessage(abortReservationMessage: AbortReservationMessage) {
        val dialog = AbortReservationMessageDialog()
        dialog.isCancelable = false
        dialog.abortReservationMessage = abortReservationMessage
        dialog.show(supportFragmentManager, "abortReservationMessageDialog")
    }

    private fun showDoorUnlockedMessage() {
        val dialog = DoorUnlockedMessageDialog()
        dialog.isCancelable = false
        dialog.show(supportFragmentManager, "doorUnlockedMessageDialog")
    }

    private fun showSelectCalendarMessage(selectCalendarMessage: SelectCalendarMessage) {
        val dialog = SelectCalendarMessageDialog()
        dialog.isCancelable = false
        dialog.selectCalendarMessage = selectCalendarMessage
        dialog.show(supportFragmentManager, "selectCalendarMessageDialog")
    }

    private fun showAppNotEligibleMessage(appNotEligibleMessage: AppNotEligibleMessage) {
        val dialog = AppNotEligibleMessageDialog()
        dialog.isCancelable = false
        dialog.appNotEligibleMessage = appNotEligibleMessage
        dialog.show(supportFragmentManager, "appNotEligibleMessageDialog")
    }

    private fun replaceFragment(fragment: Fragment) {
        val fragmentManager = supportFragmentManager
        val fragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.contentFrameLayout, fragment, "")
        fragmentTransaction.commit()
    }

    private fun replaceFragmentAddToBackStack(fragment: Fragment) {
        val fragmentManager = supportFragmentManager
        val fragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.contentFrameLayout, fragment, "")
        fragmentTransaction.addToBackStack(null)
        fragmentTransaction.commit()
    }
}