package se.fabege.app.waw.misc

import android.content.Context
import android.content.res.Resources
import android.util.TypedValue
import se.fabege.app.waw.data.Result
import java.io.IOException
import java.lang.Exception
import java.text.SimpleDateFormat
import java.util.*

class Utils {
    companion object {
        fun parseDate(date: String): Date {
            return SimpleDateFormat("yyyyMMdd").parse(date)
        }

        fun parseDateTime(dateTime: String): Date {
            return SimpleDateFormat("yyyyMMdd'T'HH:mm").parse(dateTime)
        }

        fun dpToPx(dp: Int): Int {
            return (dp * Resources.getSystem().getDisplayMetrics().density).toInt()
        }

        fun pxToDp(px: Int): Int {
            return (px / Resources.getSystem().getDisplayMetrics().density).toInt()
        }

        fun spToPx(sp: Float, context: Context): Int {
            return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, sp, context.resources.displayMetrics).toInt()
        }

        fun getNowInMillis(): Long {
            return Calendar.getInstance().time.time
        }

        fun getEndOfDayInMillis(): Long {
            val cal = Calendar.getInstance()
            with(cal) {
                set(Calendar.HOUR_OF_DAY, 23)
                set(Calendar.MINUTE, 59)
                set(Calendar.SECOND,59)
                set(Calendar.MILLISECOND, 999)
            }
            return cal.time.time
        }
    }


}

suspend fun <T : Any> safeApiCall(call: suspend () -> Result<T>, errorMessage: String): Result<T> {
    return try {
        call()
    } catch (e: Exception) {
        Result.Error(IOException(errorMessage, e))
    }
}

