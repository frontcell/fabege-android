package se.fabege.app.waw.ui.fragment.loginemailsent

import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.text.Spannable
import android.text.SpannableString
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import se.fabege.app.R
import se.fabege.app.waw.data.TextMessage
import se.fabege.app.waw.device.Device
import se.fabege.app.waw.misc.Injection
import se.fabege.app.waw.ui.fragment.ToolbarFragment
import se.fabege.app.waw.ui.widget.ColorUnderlineSpan

class LogInEmailSentFragment : ToolbarFragment() {

    private var callback: Events? = null

    private lateinit var device: Device

    private lateinit var emailLogo: ImageView
    private lateinit var openEmailTextView: TextView


    init {
        xmlLayoutId = R.layout.waw_fragment_login_email_sent
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        callback = context as? Events
        if (callback == null) {
            throw ClassCastException("$context must implement LogInEmailSentFragment.Events")
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        device = Injection.provideDevice(activity!!)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = super.onCreateView(inflater, container, savedInstanceState)
        emailLogo = view?.findViewById(R.id.imageView5)!!
        emailLogo.setOnClickListener {
            doEmail()
        }

        openEmailTextView = view?.findViewById(R.id.openEmailTextView)!!
        val text = openEmailTextView.text
        val positiveSpannable = SpannableString(text)
        positiveSpannable.setSpan(ColorUnderlineSpan(Color.parseColor("#8A2082"), 0, text.length), 0, text.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        openEmailTextView.text = positiveSpannable

        openEmailTextView.setOnClickListener {
            doEmail()
        }
        return view
    }

    private fun doEmail() {
        if (device.isEmailAppInstalled()) {
            device.openEmailApp()
        } else {
            showMessage(TextMessage(getString(R.string.waw_dialog_generic_error_title), getString(R.string.waw_dialog_email_app_not_found_message)))
        }
    }

    interface Events
}