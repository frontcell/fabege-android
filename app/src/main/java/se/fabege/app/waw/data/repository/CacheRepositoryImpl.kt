package se.fabege.app.waw.data.repository

import se.fabege.app.waw.domain.model.WAWUser
import se.fabege.app.waw.domain.model.Workspace
import se.fabege.app.waw.domain.repository.CacheRepository
import java.lang.IllegalStateException

object CacheRepositoryImpl : CacheRepository {

    private val lock = Any()
    private var lock2 = Any()
    private var workspaces: List<Workspace>? = null
    private var wawUser: WAWUser? = null

    init {

    }

    override fun hasWorkspaces(): Boolean {
        synchronized(lock) {
            return workspaces != null
        }
    }

    override fun getWorkspaces(): List<Workspace> {
        synchronized(lock) {
            workspaces?.let {
                return it
            }
            throw IllegalStateException("workspaces cannot be null!")
        }
    }

    override fun putWorkspaces(workspaces: List<Workspace>) {
        synchronized(lock) {
            this.workspaces = workspaces
        }
    }

    override fun hasWAWUser(): Boolean {
        synchronized(lock2) {
            return wawUser != null
        }
    }

    override fun getWAWUser(): WAWUser {
        synchronized(lock2) {
            wawUser?.let {
                return it
            }
            throw IllegalStateException("wawUser cannot be null!")
        }
    }

    override fun putWAWUser(wawUser: WAWUser) {
        synchronized(lock2) {
            this.wawUser = wawUser
        }
    }

    override fun clear() {
        synchronized(lock) {
            workspaces = null
        }
        synchronized(lock2) {
            wawUser = null
        }
    }
}