package se.fabege.app.waw.ui.fragment.loginwelcome

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import se.fabege.app.R
import se.fabege.app.waw.ui.fragment.ToolbarFragment

class LogInWelcomeFragment : ToolbarFragment() {

    private var callback: Events? = null

    init {
        xmlLayoutId = R.layout.waw_fragment_login_welcome
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        callback = context as? Events
        if (callback == null) {
            throw ClassCastException("$context must implement LogInWelcomeFragment.Events")
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = super.onCreateView(inflater, container, savedInstanceState)

        val button = view?.findViewById<View>(R.id.button2)
        button?.setOnClickListener { callback?.onContinueButtonClick() }
        return view
    }

    interface Events {
        fun onContinueButtonClick()
    }
}

