package se.fabege.app.waw.ui.widget

import android.content.Context
import android.os.Build
import android.text.Html
import android.text.util.Linkify
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import se.fabege.app.R
import se.fabege.app.waw.domain.model.WorkspaceMetadata

class FacilityView(context: Context, attrs: AttributeSet) : FrameLayout(context, attrs) {
    private var headerContainer: LinearLayout
    private var contentContainer: FrameLayout
    private lateinit var items: List<WorkspaceMetadata.Facility>
    private lateinit var icons: MutableList<ImageView?>
    private lateinit var selected: Array<Boolean>

    init {
        LayoutInflater.from(context).inflate(R.layout.waw_view_facility, this, true)
        headerContainer = findViewById(R.id.linearLayout1)
        contentContainer = findViewById(R.id.frameLayout1)
    }


    fun update(facilityInfo: WorkspaceMetadata.FacilityInfo) {
        items = facilityInfo.facilities
        icons = MutableList(items.size) { null }
        selected = Array(items.size) { false }

        headerContainer.removeAllViews()
        for ((index, facility) in facilityInfo.facilities.withIndex()) {

            headerContainer.addView(createIconView(index))
        }
        select(0)
        update(0)
    }

    private fun createIconView(index: Int): View {
        val inflater = LayoutInflater.from(context)
        val view = inflater.inflate(R.layout.waw_view_facility_icon, headerContainer, false)
        view.tag = index
        val imageView = view.findViewById<ImageView>(R.id.imageView)
        icons[index] = imageView
        view.setOnClickListener {
            val index = it.tag as Int
            select(index)
            update(index)
        }
        return view
    }

    private fun select(index: Int) {
        for (i in 0 until selected.size) {
            selected[i] = i == index
        }
    }

    private fun update(index: Int) {
        for ((index, icon) in icons.withIndex()) {
            icon?.let {
                it.setImageResource(getImageResId(index, selected[index]))
            }
        }
        val facility = items[index]
        when (facility) {
            is WorkspaceMetadata.WiFi -> showWifiContent(facility)
            is WorkspaceMetadata.Printer -> showPrinterContent(facility)
            is WorkspaceMetadata.Coffee -> showCoffeeContent(facility)
            is WorkspaceMetadata.Conference -> showConferenceContent(facility)
        }
    }

    private fun getImageResId(index: Int, selected: Boolean): Int {
        val facility = items[index]
        return if (selected) {
            when (facility) {
                is WorkspaceMetadata.WiFi -> R.drawable.waw_icon_wifi_purple
                is WorkspaceMetadata.Printer -> R.drawable.waw_icon_print_purple
                is WorkspaceMetadata.Coffee -> R.drawable.waw_icon_coffee_purple
                is WorkspaceMetadata.Conference -> R.drawable.waw_icon_conference_purple
                else -> 0
            }
        } else {
            when (facility) {
                is WorkspaceMetadata.WiFi -> R.drawable.waw_icon_wifi_black
                is WorkspaceMetadata.Printer -> R.drawable.waw_icon_print_black
                is WorkspaceMetadata.Coffee -> R.drawable.waw_icon_coffee_black
                is WorkspaceMetadata.Conference -> R.drawable.waw_icon_conference_black
                else -> 0
            }
        }
    }

    private fun showWifiContent(wifi: WorkspaceMetadata.WiFi) {
        val inflater = LayoutInflater.from(context)
        val view = inflater.inflate(R.layout.waw_view_facility_content_wifi, contentContainer, false)
        val contentTextView = view.findViewById<TextView>(R.id.contentTextView)
        contentTextView.text = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) Html.fromHtml(wifi.description, Html.FROM_HTML_MODE_COMPACT) else Html.fromHtml(wifi.description)
        Linkify.addLinks(contentTextView, Linkify.ALL)
        val usernameTextView = view.findViewById<TextView>(R.id.usernameTextView)
        usernameTextView.text = wifi.username
        val passwordTextView = view.findViewById<TextView>(R.id.passwordTextView)
        passwordTextView.text = wifi.password
        contentContainer.removeAllViews()
        contentContainer.addView(view)
    }

    private fun showPrinterContent(printer: WorkspaceMetadata.Printer) {
        val inflater = LayoutInflater.from(context)
        val view = inflater.inflate(R.layout.waw_view_facility_content, contentContainer, false)
        val contentTextView = view.findViewById<TextView>(R.id.contentTextView)
        contentTextView.text = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) Html.fromHtml(printer.description, Html.FROM_HTML_MODE_COMPACT) else Html.fromHtml(printer.description)
        Linkify.addLinks(contentTextView, Linkify.ALL)
        contentContainer.removeAllViews()
        contentContainer.addView(view)
    }

    private fun showCoffeeContent(coffee: WorkspaceMetadata.Coffee) {
        val inflater = LayoutInflater.from(context)
        val view = inflater.inflate(R.layout.waw_view_facility_content, contentContainer, false)
        val contentTextView = view.findViewById<TextView>(R.id.contentTextView)
        contentTextView.text = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) Html.fromHtml(coffee.description, Html.FROM_HTML_MODE_COMPACT) else Html.fromHtml(coffee.description)
        Linkify.addLinks(contentTextView, Linkify.ALL)
        contentContainer.removeAllViews()
        contentContainer.addView(view)
    }

    private fun showConferenceContent(conference: WorkspaceMetadata.Conference) {
        val inflater = LayoutInflater.from(context)
        val view = inflater.inflate(R.layout.waw_view_facility_content, contentContainer, false)
        val contentTextView = view.findViewById<TextView>(R.id.contentTextView)
        contentTextView.text = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) Html.fromHtml(conference.description, Html.FROM_HTML_MODE_COMPACT) else Html.fromHtml(conference.description)
        Linkify.addLinks(contentTextView, Linkify.ALL)
        contentContainer.removeAllViews()
        contentContainer.addView(view)
    }
}