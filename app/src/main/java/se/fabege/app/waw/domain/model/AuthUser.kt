package se.fabege.app.waw.domain.model

data class AuthUser(val uid: String)