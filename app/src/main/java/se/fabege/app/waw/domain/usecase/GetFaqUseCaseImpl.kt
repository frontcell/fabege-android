package se.fabege.app.waw.domain.usecase

import se.fabege.app.waw.data.Result
import se.fabege.app.waw.domain.model.Faq
import se.fabege.app.waw.domain.repository.DataRepository
import se.fabege.app.waw.misc.exhaustive

class GetFaqUseCaseImpl(private val dataRepository: DataRepository) : GetFaqUseCase() {

    override suspend fun invoke(): Result<Faq> {
        val result = dataRepository.getFaq()

        return when (result) {
            is Result.Success -> result
            is Result.Error -> result
        }.exhaustive
    }
}