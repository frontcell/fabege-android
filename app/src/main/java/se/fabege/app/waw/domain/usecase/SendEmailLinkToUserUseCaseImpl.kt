package se.fabege.app.waw.domain.usecase

import se.fabege.app.waw.data.Result
import se.fabege.app.waw.domain.repository.AuthRepository
import se.fabege.app.waw.misc.exhaustive

class SendEmailLinkToUserUseCaseImpl(private val authRepository: AuthRepository): SendEmailLinkToUserUseCase() {

    override suspend operator fun invoke(email: String): Result<Unit> {
        val result = authRepository.sendEmailLink(email)
        return when (result) {
            is Result.Success -> result
            is Result.Error -> result
        }.exhaustive
    }
}