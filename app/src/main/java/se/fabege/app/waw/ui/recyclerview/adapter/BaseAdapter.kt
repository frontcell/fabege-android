package se.fabege.app.waw.ui.recyclerview.adapter

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup

abstract class BaseAdapter<T>(protected var items: T) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    protected var manager: AdapterDelegateManager<T> = AdapterDelegateManager()


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return manager.onCreateViewHolder(parent, viewType)
    }

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        manager.onBindViewHolder(items, position, viewHolder)
    }

    override fun getItemViewType(position: Int): Int {
        return manager.getItemViewType(items, position)
    }
}