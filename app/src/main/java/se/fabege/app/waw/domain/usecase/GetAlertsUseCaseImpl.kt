package se.fabege.app.waw.domain.usecase

import se.fabege.app.waw.data.Result
import se.fabege.app.waw.domain.model.Alert
import se.fabege.app.waw.domain.repository.DataRepository
import se.fabege.app.waw.misc.exhaustive

class
GetAlertsUseCaseImpl(private val dataRepository: DataRepository) : GetAlertsUseCase() {

    override suspend fun invoke(): Result<List<Alert>> {
        val result = dataRepository.getAlerts()

        return when (result) {
            is Result.Success -> result
            is Result.Error -> result
        }.exhaustive
    }
}