package se.fabege.app.waw.ui.recyclerview.adapter.delegates

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import se.fabege.app.R
import se.fabege.app.waw.ui.recyclerview.adapter.Item
import se.fabege.app.waw.ui.recyclerview.adapter.ListAdapterDelegate
import se.fabege.app.waw.ui.recyclerview.adapter.items.CalendarHeaderItem

class CalendarHeaderAdapterDelegate(private val context: Context) : ListAdapterDelegate<CalendarHeaderItem, Item, CalendarHeaderItem.ViewHolder>() {
    override fun isForViewType(item: Item, items: List<Item>, pos: Int): Boolean {
        return item is CalendarHeaderItem
    }

    override fun onCreateViewHolder(parent: ViewGroup): CalendarHeaderItem.ViewHolder {
        return CalendarHeaderItem.ViewHolder(LayoutInflater.from(context).inflate(R.layout.waw_rv_item_calendar_header, parent, false))
    }

    override fun onBindViewHolder(item: CalendarHeaderItem, viewHolder: CalendarHeaderItem.ViewHolder) {
        viewHolder.calendarHeaderView.update(item.viewModel)
    }

}