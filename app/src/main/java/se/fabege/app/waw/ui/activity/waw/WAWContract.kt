package se.fabege.app.waw.ui.activity.waw

import se.fabege.app.waw.ui.BasePresenter
import se.fabege.app.waw.ui.BaseView

interface WAWContract {
    interface View : BaseView<Presenter> {
        fun showLogInWelcome()
        fun showLogInEmail()
        fun showLogInEmailSent()
        fun showLogInInstallParakey()
        fun showLogInActivateParakey(email:String, password: String)
        fun showOnboarding()
        fun showLogInDone()
        fun showSelectWorkspace()
    }

    interface Presenter : BasePresenter {
        fun onContinueFromLogInWelcome()
        fun onContinueFromLogInEmail(email: String)
        fun onContinueFromLogInInstallParakey()
        fun onContinueFromLogInActivateParakey()
        fun onContinueFromOnboardingDone()
        fun onContinueFromLogInDone()
    }
}