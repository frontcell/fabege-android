package se.fabege.app.waw.ui.fragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import se.fabege.app.R

abstract class ToolbarFragment : BaseFragment() {

    protected var xmlLayoutId: Int = 0
    protected var toolbarTitleTextView: TextView? = null
    protected var toolbarAddressTextView: TextView? = null
    protected var toolbarAddressImageView: ImageView? = null
    protected var toolbarFaqButton: View? = null


    private var toolbarBackButton: View? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(xmlLayoutId, container, false)

        toolbarTitleTextView = view.findViewById(R.id.titleTextView)
        toolbarAddressImageView = view.findViewById(R.id.addressImageView)
        toolbarAddressTextView = view.findViewById(R.id.addressTextView)
        toolbarBackButton = view.findViewById(R.id.backImageButton)
        toolbarBackButton?.setOnClickListener {
            activity?.onBackPressed()
        }
        toolbarFaqButton = view.findViewById(R.id.faqImageButton)
        return view
    }
}