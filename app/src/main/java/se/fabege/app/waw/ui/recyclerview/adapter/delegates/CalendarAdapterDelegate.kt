package se.fabege.app.waw.ui.recyclerview.adapter.delegates

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import se.fabege.app.R
import se.fabege.app.waw.ui.recyclerview.adapter.Item
import se.fabege.app.waw.ui.recyclerview.adapter.ListAdapterDelegate
import se.fabege.app.waw.ui.recyclerview.adapter.items.CalendarItem
import se.fabege.app.waw.ui.widget.CalendarView

class CalendarAdapterDelegate(private val context: Context, private val clickListener: CalendarView.ClickListener) : ListAdapterDelegate<CalendarItem, Item, CalendarItem.ViewHolder>() {
    override fun isForViewType(item: Item, items: List<Item>, pos: Int): Boolean {
        return item is CalendarItem
    }

    override fun onCreateViewHolder(parent: ViewGroup): CalendarItem.ViewHolder {
        return CalendarItem.ViewHolder(LayoutInflater.from(context).inflate(R.layout.waw_rv_item_calendar, parent, false))
    }

    override fun onBindViewHolder(item: CalendarItem, viewHolder: CalendarItem.ViewHolder) {
        viewHolder.calendarView.update(item.viewModel)
        viewHolder.calendarView.setClickListener(clickListener)
    }

}