package se.fabege.app.waw.device;

import android.content.AsyncQueryHandler;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.provider.CalendarContract;
import android.support.v4.util.Pair;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;

public class CalendarManager extends AsyncQueryHandler {

    private static final int ADD_EVENT = 0;
    private static final int ADD_REMINDER = 1;
    private static final int GET_CALENDARS = 2;
    private static final int DELETE_EVENT = 3;

    public interface Callback {
        void onGetCalendarsComplete(Calendars result);

        void onAddEventComplete(String id, long eventId);

        void onDeleteEventComplete();

        void onAddReminderComplete(long remainderId);
    }

    private static final String[] PROJECTION = new String[]{
            CalendarContract.Calendars._ID,
            CalendarContract.Calendars.CALENDAR_DISPLAY_NAME
    };


    private WeakReference<Callback> callbackWeakReference;

    public CalendarManager(ContentResolver cr, Callback callback) {
        super(cr);
        this.callbackWeakReference = new WeakReference<>(callback);
    }

    public void getCalendarsAsync() {
        startQuery(GET_CALENDARS, null, CalendarContract.Calendars.CONTENT_URI, PROJECTION, null, null, null);
    }

    public void addEventAsync(long calendarId, Event event) {

        startInsert(ADD_EVENT, event.getId(), CalendarContract.Events.CONTENT_URI, prepareAddEventValues(calendarId, event));
    }

    public void deleteEventAsync(long eventId) {
        startDelete(DELETE_EVENT, null, ContentUris.withAppendedId(CalendarContract.Events.CONTENT_URI, eventId), null, null);
    }

    public void addReminderAsync(long eventId) {
        startInsert(ADD_REMINDER, null, CalendarContract.Reminders.CONTENT_URI, prepareAddReminderValues(eventId));
    }


    private ContentValues prepareAddEventValues(long calendarId, Event event) {
        ContentValues values = new ContentValues();
        values.put(CalendarContract.Events.CALENDAR_ID, calendarId);
        values.put(CalendarContract.Events.TITLE, event.getTitle());
        values.put(CalendarContract.Events.DESCRIPTION, event.getDescription());
        values.put(CalendarContract.Events.EVENT_LOCATION, event.getLocation());
        values.put(CalendarContract.Events.DTSTART, event.getBeginTimeInMillis());
        values.put(CalendarContract.Events.DTEND, event.getEndTimeInMillis());
        values.put(CalendarContract.Events.AVAILABILITY, CalendarContract.Events.AVAILABILITY_BUSY);
        values.put(CalendarContract.Events.EVENT_TIMEZONE, TimeZone.getDefault().getID());
        values.put(CalendarContract.Events.HAS_ALARM, false);

        return values;
    }

    private ContentValues prepareAddReminderValues(long eventId) {
        ContentValues values = new ContentValues();
        values.put(CalendarContract.Reminders.MINUTES, 60);
        values.put(CalendarContract.Reminders.EVENT_ID, eventId);
        values.put(CalendarContract.Reminders.METHOD, CalendarContract.Reminders.METHOD_ALERT);

        return values;
    }

    @Override
    protected void onQueryComplete(int token, Object cookie, Cursor cursor) {
        switch (token) {
            case GET_CALENDARS:
                Callback callback = callbackWeakReference.get();
                if (callback != null) {
                    callback.onGetCalendarsComplete(new Calendars(cursor));
                }
                break;
            default:
                break;
        }
    }

    @Override
    protected void onInsertComplete(int token, Object cookie, Uri uri) {
        Callback callback = callbackWeakReference.get();
        if (callback != null) {
            switch (token) {
                case ADD_EVENT:
                    long eventId = Long.parseLong(uri.getLastPathSegment());
                    callback.onAddEventComplete((String) cookie, eventId);
                    break;
                case ADD_REMINDER:
                    long reminderId = Long.parseLong(uri.getLastPathSegment());
                    callback.onAddReminderComplete(reminderId);
                    break;
                default:
                    break;
            }
        }
    }

    @Override
    protected void onDeleteComplete(int token, Object cookie, int result) {
        Callback callback = callbackWeakReference.get();
        if (callback != null) {
            switch (token) {
                case DELETE_EVENT:
                    callback.onDeleteEventComplete();
                    break;
                default:
                    break;
            }
        }
    }

    public static class Calendars {
        List<Pair<Long, String>> pairList = new ArrayList<>();

        public Calendars(Cursor cursor) {
            while (cursor.moveToNext()) {
                pairList.add(new Pair<>(cursor.getLong(cursor.getColumnIndexOrThrow(CalendarContract.Calendars._ID)), cursor.getString(cursor.getColumnIndexOrThrow(CalendarContract.Calendars.CALENDAR_DISPLAY_NAME))));
            }
            cursor.close();
        }

        public String[] getCalendarDisplayNames() {
            String[] result = new String[pairList.size()];
            for (int i = 0; i < pairList.size(); i++) {
                Pair<Long, String> pair = pairList.get(i);
                result[i] = pair.second;
            }

            return result;
        }

        public long getCalendarId(int index) {
            return pairList.get(index).first;
        }
    }

    public static class Event {

        public Event(String id, String title, String description, String location, long beginTimeInMillis, long endTimeInMillis) {
            this.id = id;
            this.title = title;
            this.description = description;
            this.location = location;
            this.beginTimeInMillis = beginTimeInMillis;
            this.endTimeInMillis = endTimeInMillis;
        }

        private String id;
        private String title;
        private String description;
        private String location;
        private long beginTimeInMillis;
        private long endTimeInMillis;

        public String getId() {
            return this.id;
        }

        public String getTitle() {
            return this.title;
        }

        public String getDescription() {
            return description;
        }

        public String getLocation() {
            return location;
        }

        public long getBeginTimeInMillis() {
            return beginTimeInMillis;
        }

        public long getEndTimeInMillis() {
            return endTimeInMillis;
        }
    }
}

