package se.fabege.app.waw.ui.widget

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.util.AttributeSet
import android.view.View


class CalendarBackgroundView(context: Context, attrs: AttributeSet) : View(context, attrs) {
    private var xStep: Float = 0f
    private var paint: Paint = Paint()

    init {
        paint.color = Color.parseColor("#E6E1DC")
        paint.strokeWidth = 3f
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        xStep = (Math.sin(Math.toRadians(66.0)) * (h / 2.0) * Math.cos(Math.toRadians(66.0))).toFloat()
    }

    override fun onDraw(canvas: Canvas) {
        val startY = 0f
        val stopY = height.toFloat()
        for (x in -width / 2..width * 3 / 2 step 30) {
            canvas.drawLine(x + xStep, startY, x - xStep, stopY, paint)
        }
    }
}

