package se.fabege.app.waw.domain.usecase

import se.fabege.app.BuildConfig
import se.fabege.app.waw.data.Result
import se.fabege.app.waw.data.error.DataRepositoryException
import se.fabege.app.waw.data.error.IsAppEligibleException
import se.fabege.app.waw.domain.repository.DataRepository

class IsAppEligibleUseCaseImpl(private val dataRepository: DataRepository) : IsAppEligibleUseCase() {

    override suspend fun invoke(): Result<Unit> {
        val result = dataRepository.getAppConfig()

        return when (result) {
            is Result.Success -> {
                if (BuildConfig.VERSION_CODE >= result.data.minAppVersion) {
                    Result.Success(Unit)
                } else {
                    Result.Error(IsAppEligibleException())
                }
            }
            is Result.Error -> {
                Result.Error(DataRepositoryException())
            }
        }
    }
}