package se.fabege.app.waw.domain.usecase

import android.util.Log
import se.fabege.app.waw.data.Result
import se.fabege.app.waw.domain.model.WAWUser
import se.fabege.app.waw.misc.Injection

class TakeOffUseCaseImpl : TakeOffUseCase() {

    private val isAppEligible = Injection.provideIsAppEligibleUseCase()
    private val getWAWUser = Injection.provideGetWAWUserUseCase()
    private val cleanUser = Injection.provideCleanWAWUserUseCase()
    private val getDeviceRegistrationToken = Injection.provideGetDeviceRegistrationTokenUseCase()
    private val setDeviceRegistrationToken = Injection.provideSetDeviceRegistrationTokenUseCase()

    override suspend fun invoke(): Result<WAWUser> {
        val wawUser = getWAWUser()

        if (wawUser is Result.Success) {
            val cleanUser = cleanUser(wawUser.data)
            val deviceRegistrationToken = getDeviceRegistrationToken()
            if (deviceRegistrationToken is Result.Success) {
                Log.d("TakeOff", "deviceRegistrationToken: " + deviceRegistrationToken.data)
                if (wawUser.data.deviceRegistrationToken != deviceRegistrationToken.data) {
                    setDeviceRegistrationToken(wawUser.data.id, deviceRegistrationToken.data)
                }
            }
        } else {
            return wawUser
        }

        val isAppEligible = isAppEligible()

        return if (isAppEligible is Result.Success) {
            wawUser
        } else {
            isAppEligible as Result.Error
        }
    }
}