package se.fabege.app.waw.domain.usecase

import se.fabege.app.waw.domain.repository.DataRepository

class SubscribeWorkspaceCalendarUseCaseImpl(private val dataRepository: DataRepository): SubcribeWorkspaceCalendarUseCase() {

    override fun invoke(workspaceId: String, callback: DataRepository.Callback) {
        dataRepository.subscribeWorkspaceCalendar(workspaceId, callback)
    }
}