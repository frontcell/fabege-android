package se.fabege.app.waw.ui.recyclerview.adapter.delegates

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import se.fabege.app.R
import se.fabege.app.waw.ui.recyclerview.adapter.Item
import se.fabege.app.waw.ui.recyclerview.adapter.ListAdapterDelegate
import se.fabege.app.waw.ui.recyclerview.adapter.items.WorkspaceMetaDataCapacityItem

class WorkspaceMetaDataCapacityAdapterDelegate(private val context: Context) : ListAdapterDelegate<WorkspaceMetaDataCapacityItem, Item, WorkspaceMetaDataCapacityItem.ViewHolder>() {
    override fun isForViewType(item: Item, items: List<Item>, pos: Int): Boolean {
        return item is WorkspaceMetaDataCapacityItem
    }

    override fun onCreateViewHolder(parent: ViewGroup): WorkspaceMetaDataCapacityItem.ViewHolder {
        return WorkspaceMetaDataCapacityItem.ViewHolder(LayoutInflater.from(context).inflate(R.layout.waw_rv_item_workspace_metadata_capacity, parent, false))
    }

    override fun onBindViewHolder(item: WorkspaceMetaDataCapacityItem, viewHolder: WorkspaceMetaDataCapacityItem.ViewHolder) {
        viewHolder.pieChartView.setValues(listOf(item.viewModel.capacityInfo.conferenceRooms, item.viewModel.capacityInfo.desks))
        viewHolder.desksTextView.text = context.getString(R.string.waw_metadata_desks, item.viewModel.capacityInfo.desks)
        viewHolder.conferenceRoomsTextView.text = context.getString(R.string.waw_metadata_conferenceRooms, item.viewModel.capacityInfo.conferenceRooms)
        viewHolder.phoneRoomsTextView.text = context.getString(R.string.waw_metadata_phoneRooms, item.viewModel.capacityInfo.phoneRooms)
    }
}