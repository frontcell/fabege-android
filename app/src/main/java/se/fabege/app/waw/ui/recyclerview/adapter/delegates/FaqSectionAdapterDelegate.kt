package se.fabege.app.waw.ui.recyclerview.adapter.delegates

import android.content.Context
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.LinearLayout
import se.fabege.app.R
import se.fabege.app.waw.domain.model.FaqQuestion
import se.fabege.app.waw.ui.recyclerview.adapter.Item
import se.fabege.app.waw.ui.recyclerview.adapter.ListAdapterDelegate
import se.fabege.app.waw.ui.recyclerview.adapter.items.FaqSectionItem
import se.fabege.app.waw.ui.widget.FaqQuestionView

class FaqSectionAdapterDelegate(private val context: Context) : ListAdapterDelegate<FaqSectionItem, Item, FaqSectionItem.ViewHolder>() {
    override fun isForViewType(item: Item, items: List<Item>, pos: Int): Boolean {
        return item is FaqSectionItem
    }

    override fun onCreateViewHolder(parent: ViewGroup): FaqSectionItem.ViewHolder {
        return FaqSectionItem.ViewHolder(LayoutInflater.from(context).inflate(R.layout.waw_rv_item_faq_section, parent, false))
    }

    override fun onBindViewHolder(item: FaqSectionItem, viewHolder: FaqSectionItem.ViewHolder) {
        viewHolder.sectionTitleTextView.text = item.viewModel.faqSection.name

        for (question in item.viewModel.faqSection.questions) {
            viewHolder.sectionContainer.addView(createFaqQuestionView(question), createLayoutParams())
        }

        viewHolder.sectionContainer.visibility = View.GONE

        viewHolder.itemView.setOnClickListener {
            if (viewHolder.sectionContainer.visibility == View.VISIBLE) {
                viewHolder.sectionContainer.visibility = View.GONE
                viewHolder.arrowImageView.setImageResource(R.drawable.waw_icon_down)
            } else {
                viewHolder.sectionContainer.visibility = View.VISIBLE
                viewHolder.arrowImageView.setImageResource(R.drawable.waw_icon_up)
                val params = viewHolder.arrowImageView.layoutParams as FrameLayout.LayoutParams

                params.gravity = Gravity.RIGHT or Gravity.BOTTOM
                viewHolder.arrowImageView.layoutParams = params
            }
        }
    }

    private fun createFaqQuestionView(question: FaqQuestion): View {
        val view = FaqQuestionView(context)
        view.update(question)
        return view
    }

    private fun createLayoutParams(): LinearLayout.LayoutParams {
        val params = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
        return params
    }

}