package se.fabege.app.waw.ui.recyclerview.adapter.items

import android.view.View
import android.widget.TextView
import se.fabege.app.R
import se.fabege.app.waw.ui.recyclerview.adapter.Item

data class CalendarFooterItem(val viewModel: ViewModel) : Item {

    data class ViewModel(val keyViewButtonEnabled: Boolean, val reserveButtonEnabled: Boolean, val reserveButtonText: String)

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val keyView = itemView.findViewById<View>(R.id.keyFrameLayout)
        val reserveView = itemView.findViewById<View>(R.id.reserveFrameLayout)
        val reserveViewText = itemView.findViewById<TextView>(R.id.reserveTextView)
    }

    fun isContentSame(other: CalendarFooterItem) : Boolean {
        return this.viewModel.keyViewButtonEnabled == other.viewModel.keyViewButtonEnabled && this.viewModel.reserveButtonEnabled ==  other.viewModel.reserveButtonEnabled && this.viewModel.reserveButtonText == other.viewModel.reserveButtonText
    }
}