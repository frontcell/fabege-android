package se.fabege.app.waw.domain.usecase

import se.fabege.app.waw.data.Result
import se.fabege.app.waw.domain.model.WAWUser
import se.fabege.app.waw.domain.repository.DataRepository
import se.fabege.app.waw.misc.exhaustive

class IsEmailEligibleUseCaseImpl(private val dataRepository: DataRepository) : IsEmailEligibleUseCase() {

    override suspend operator fun invoke(email: String): Result<Boolean> {
        val result = dataRepository.getUsers(email)

        return when (result) {
            is Result.Success -> dataContainsEmail(email, result.data)
            is Result.Error -> result
        }.exhaustive
    }

    private fun dataContainsEmail(email: String, data: List<WAWUser>): Result<Boolean> {
        for (user in data) {
            if (email.toLowerCase() == user.email.toLowerCase()) {
                return Result.Success(true)
            }
        }
        return Result.Success(false)
    }
}