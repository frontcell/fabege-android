package se.fabege.app.waw.domain.model

data class WorkspaceMetadata(val about: About, val address: Address, val media: Media,
                             val contactInfo: ContactInfo, val deskInfo: DeskInfo,
                             val timeSlotInfo: TimeSlotInfo, val facilityInfo: FacilityInfo,
                             val capacityInfo: CapacityInfo) {
    data class About(val name: String, val description: String, val extra: String)

    data class Address(val address: String, val latitude: Float, val longitude: Float)

    data class FacilityInfo(val facilities: List<Facility>)

    data class Coffee(val description: String) : Facility

    data class Conference(val description: String) : Facility

    data class Printer(val description: String) : Facility

    data class WiFi(val description: String, val username: String, val password: String) : Facility

    data class Media(val iconUrl: String, val imageUrl: String)

    data class TimeSlotInfo(val spans: List<String>)

    data class DeskInfo(val total: Int, val earlier: Int, val sameDay: Int)

    data class CapacityInfo(val desks: Int, val phoneRooms: Int, val conferenceRooms: Int)

    data class ContactInfo(val name: String, val title: String, val email: String, val phoneNumber: String, val imageUrl: String)

    data class AlertsInfo(val alerts: List<Alert>)

    interface Facility
}


