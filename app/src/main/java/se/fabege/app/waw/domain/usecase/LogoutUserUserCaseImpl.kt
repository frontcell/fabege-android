package se.fabege.app.waw.domain.usecase

import se.fabege.app.waw.data.Result
import se.fabege.app.waw.domain.repository.AuthRepository
import se.fabege.app.waw.misc.exhaustive

class LogoutUserUserCaseImpl(private val authRepository: AuthRepository): LogoutUserUseCase() {

    override suspend fun invoke(): Result<Boolean> {
        val result = authRepository.logout()
        return when(result) {
            is Result.Success -> result
            is Result.Error -> result
        }.exhaustive
    }
}