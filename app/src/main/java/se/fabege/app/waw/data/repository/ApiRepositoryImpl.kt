package se.fabege.app.waw.data.repository

import se.fabege.app.waw.data.Result
import se.fabege.app.waw.data.datasource.WAWApiRemoteDataSource
import se.fabege.app.waw.data.error.SendSignInLinkToEmailException
import se.fabege.app.waw.domain.repository.ApiRepository
import se.fabege.app.waw.domain.repository.KeyValueRepository

class ApiRepositoryImpl(private val api: WAWApiRemoteDataSource, private val keyValueRepository: KeyValueRepository) : ApiRepository {

    override suspend fun sendEmailLink(uid: String): Result<Unit> {
        keyValueRepository.putString("uid", uid)
        val result = api.sendSignInLinkToUid(uid)
        return when(result) {
            is Result.Success -> result
            is Result.Error -> Result.Error(SendSignInLinkToEmailException())
        }
    }
}