package se.fabege.app.waw.domain.model

data class Faq(val faqSections: List<FaqSection>)

data class FaqSection(val name: String, val questions: List<FaqQuestion>)

data class FaqQuestion(val question: String, val answer: String)