package se.fabege.app.waw.ui.recyclerview.adapter

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup

class AdapterDelegateManager<T> {

    private val delegates: MutableMap<Int, AdapterDelegate<T>> = HashMap()

    fun addDelegate(delegate: AdapterDelegate<T>): AdapterDelegateManager<T> {
        var viewType = delegates.size
        while (delegates[viewType] != null) {
            viewType += 1
        }
        return addDelegate(viewType, delegate)
    }

    fun getItemViewType(items: T, pos: Int): Int {
        for (entry in delegates.entries) {
            if (entry.value.isForViewType(items, pos)) {
                return entry.key
            }
        }
        throw NullPointerException("No matching AdapterDelegate found")
    }

    fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return getDelegateForViewType(viewType).onCreateViewHolder(parent)
    }

    fun onBindViewHolder(items: T, pos: Int, viewHolder: RecyclerView.ViewHolder) {
        getDelegateForViewType(viewHolder.itemViewType).onBindViewHolder(items, pos, viewHolder)
    }

    fun getDelegateForViewType(viewType: Int): AdapterDelegate<T> {
        val delegate = delegates.get(viewType)
        if (delegate != null) {
            return delegate
        } else {
            throw NullPointerException("No key= $viewType present")
        }
    }

    private fun addDelegate(viewType: Int, delegate: AdapterDelegate<T>): AdapterDelegateManager<T> {
        delegates[viewType] = delegate
        return this
    }

}