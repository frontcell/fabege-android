package se.fabege.app.waw.domain.usecase

import se.fabege.app.waw.domain.repository.DataRepository

class SubscribeWAWUserUseCaseImpl(private val dataRepository: DataRepository) : SubscribeWAWUserUseCase() {

    override fun invoke(userId: String, callback: DataRepository.UserCallback) {
        dataRepository.subscribeUser(userId, callback)
    }
}