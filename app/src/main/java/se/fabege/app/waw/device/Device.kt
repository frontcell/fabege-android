package se.fabege.app.waw.device

interface Device {
    fun openEmailApp()
    fun openParakeyGooglePlayPage()
    fun openFabegeGooglePlayPage()
    fun callPhoneNumber(phoneNumber: String)
    fun sendEmail(email: String)
    fun openParakeyApp(email: String, password: String, requestCode: Int)
    fun openGoogleMapApp(latitude: Float, longitude: Float)
    fun requestCalendarPermissions(callback: CalendarPermissionsCallback)
    fun addCalendarEvents(events: List<CalendarManager.Event>)
    fun deleteCalendarEvents(events: List<CalendarManager.Event>)

    fun isCalendarPermissionsGranted(): Boolean
    fun isGoogleMapAppInstalled(): Boolean
    fun isEmailAppInstalled(): Boolean
    fun isParakeyAppInstalled(): Boolean
    fun canCallPhoneNumber(): Boolean
    fun canSendEmail(): Boolean

    fun trackPageView(name: String)
    fun trackEvent(name: String)

    interface CalendarPermissionsCallback {
        fun onCalendarPermissionsGranted()
        fun onCalendarPermissionDenied()
    }
}