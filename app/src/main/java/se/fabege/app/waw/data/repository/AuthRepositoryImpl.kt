package se.fabege.app.waw.data.repository;

import android.util.Log
import com.google.firebase.auth.FirebaseUser
import kotlinx.coroutines.tasks.await
import se.fabege.app.waw.data.datasource.FirebaseAuthRemoteDataSource
import se.fabege.app.waw.data.Result
import se.fabege.app.waw.data.error.SendSignInLinkToEmailException
import se.fabege.app.waw.data.error.SignInWithEmailLinkException
import se.fabege.app.waw.domain.model.AuthUser
import se.fabege.app.waw.domain.repository.AuthRepository
import se.fabege.app.waw.domain.repository.KeyValueRepository

class AuthRepositoryImpl(private val authRemote: FirebaseAuthRemoteDataSource, private val keyValueRepository: KeyValueRepository) : AuthRepository {

    companion object {
        private val TAG = "Fabege WAW " + AuthRepositoryImpl::class.java.simpleName
    }

    override fun isLoggedIn(): Result<Boolean> {
        return authRemote.isSignedIn()
    }

    override fun getUser(): Result<AuthUser> {
        val result = authRemote.getCurrentUser()
        return when (result) {
            is Result.Success -> Result.Success(createAuthUserFromFirebaseUser(result.data))
            is Result.Error -> result
        }
    }

    override fun isEmailLink(emailLink: String): Result<Boolean> {
        return authRemote.isSignInWithEmailLink(emailLink)
    }

    override suspend fun sendEmailLink(email: String): Result<Unit> {
        keyValueRepository.putString("email", email)
        val task = authRemote.sendSignInLinkToEmail(email)
        try {
            task.await()
        } finally {
            return if (task.isSuccessful) {
                Result.Success(Unit)
            } else {
                Log.e(TAG, task.exception.toString())
                Result.Error(SendSignInLinkToEmailException())
            }
        }
    }

    override suspend fun loginWithEmailLink(emailLink: String): Result<AuthUser> {
        val email = keyValueRepository.getString("email")
        if (email.isNullOrEmpty()) {
            return Result.Error(SignInWithEmailLinkException())
        }

        val task = authRemote.signInWithEmailLink(keyValueRepository.getString("email")
                ?: "", emailLink)
        try {
            task.await()
        } finally {
            return if (task.isSuccessful) {
                val firebaseUser = task.result!!.user!!
                Result.Success(createAuthUserFromFirebaseUser(firebaseUser))
            } else {
                Log.e(TAG, task.exception.toString())
                Result.Error(SignInWithEmailLinkException())
            }
        }
    }

    override suspend fun loginWithCustomToken(token: String): Result<AuthUser> {
        val uid = keyValueRepository.getString("uid")
        if (uid.isNullOrEmpty()) {
            return Result.Error(SignInWithEmailLinkException())
        }

        val task = authRemote.signInWithCustomToken(token)
        try {
            task.await()
        } finally {
            return if (task.isSuccessful) {
                val firebaseUser = task.result!!.user!!
                if (uid != firebaseUser.uid) {
                    logout()
                    Result.Error(SignInWithEmailLinkException())
                } else {
                    Result.Success(createAuthUserFromFirebaseUser(firebaseUser))
                }
            } else {
                Log.e(TAG, task.exception.toString())
                Result.Error(SignInWithEmailLinkException())
            }
        }
    }


    override suspend fun logout(): Result<Boolean> {
        return authRemote.signOut()
    }

    private fun createAuthUserFromFirebaseUser(firebaseUser: FirebaseUser): AuthUser {
        return AuthUser(firebaseUser.uid)
    }

}
