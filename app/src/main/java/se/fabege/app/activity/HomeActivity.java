package se.fabege.app.activity;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import se.fabege.api.dto.work.WorkResponse;
import se.fabege.app.R;
import se.fabege.app.analytics.AnalyticsImpl;
import se.fabege.app.analytics.AnalyticsUtils;
import se.fabege.app.base.BaseActivity;
import se.fabege.app.base.BaseFragment;
import se.fabege.app.base.FabegeApplication;
import se.fabege.app.fragment.FaqFragment;
import se.fabege.app.fragment.HomeFragment;
import se.fabege.app.fragment.InterstitialFragment;
import se.fabege.app.fragment.WebViewFragment;
import se.fabege.app.fragment.WorkFragment;
import se.fabege.app.fragment.register.RegisterFragment;
import se.fabege.app.fragment.select.SelectFragment;
import se.fabege.app.waw.ui.activity.waw.WAWActivity;
import se.fabege.provider.database.table.StructureTable;
import se.fabege.provider.model.Structure;
import se.fabege.provider.provider.FabegeProvider;


public class HomeActivity extends BaseActivity implements HomeFragment.IHomeFragment,
        WorkFragment.IWorkFragment, RegisterFragment.IRegisterFragment, SelectFragment.ISelectFragment,
        BaseFragment.IFabegeBaseFragment, FaqFragment.IFaqFragment, InterstitialFragment.IInterstitialFragment {

    private Structure mActiveStructure;
    private WorkResponse mOngoingWork;
    private final static int REGISTER_LOADER = 1003;
    private boolean startSelect;

    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        final FragmentManager manager = getSupportFragmentManager();
        Cursor c = getContentResolver().query(FabegeProvider.STRUCTURE_CONTENT_URI, null, StructureTable.IS_FAVOURITE + " = ?", new String[]{"1"}, null);
        int count = c.getCount();
        c.close();

        if (savedInstanceState == null) {
            startSelect = count == 0;
        }

        pushFragment(InterstitialFragment.Companion.newInstance());

        manager.addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {
                int backStackEntryCount = manager.getBackStackEntryCount();
                ActionBar supportActionBar = getSupportActionBar();
                if (backStackEntryCount > 1) {
                    supportActionBar.setDisplayHomeAsUpEnabled(true);
                } else {
                    supportActionBar.setDisplayHomeAsUpEnabled(false);
                }
            }
        });

        hideToolbar();
    }

    @Override
    public void onBackPressed() {
        final FragmentManager manager = getSupportFragmentManager();
        BaseFragment f = (BaseFragment) manager.findFragmentById(R.id.container);
        if (!f.consumeBackPress()) {
            int backStackEntryCount = manager.getBackStackEntryCount();
            if (backStackEntryCount == 1) {
                finish();
            } else {
                super.onBackPressed();
            }
        }
    }

    @Override
    public void onSearchClicked() {
        Log.d(FabegeApplication.TAG, "onSearchClicked");
//        AnalyticsImpl.getInstance().trackEvent(AnalyticsUtils.makePageViewEvent("Search", getActiveStructure()));
        pushFragment(SelectFragment.newInstance(true));
    }

    @Override
    public void onSubmitWorkClicked(Structure structure) {
        mActiveStructure = structure;
        Log.d(FabegeApplication.TAG, "onSubmitWorkClicked");
        AnalyticsImpl.getInstance().trackEvent(AnalyticsUtils.makePageViewEvent("Service Request", getActiveStructure()));
        pushFragment(RegisterFragment.newInstance());
    }

    @Override
    public void onViewWorkClicked() {
        Log.d(FabegeApplication.TAG, "onViewWorkClicked");
        AnalyticsImpl.getInstance().trackEvent(AnalyticsUtils.makePageViewEvent("Ongoing Work", getActiveStructure()));
        if (mOngoingWork != null && mActiveStructure != null) {
            pushFragment(WorkFragment.newInstance());
        }
    }

    @Override
    public void onReadMoreClicked(String url) {
        Log.d(FabegeApplication.TAG, "onReadMoreClicked");
        AnalyticsImpl.getInstance().trackEvent(AnalyticsUtils.makePageViewEvent("Read more", getActiveStructure()));
        pushFragment(WebViewFragment.newInstance(url));
    }

    @Override
    public void onFaqClicked(Structure structure) {
        Log.d(FabegeApplication.TAG, "onFaqClicked");
        mActiveStructure = structure;
//        pushFragment(FaqFragment.newInstance());
        setTitle(R.string.button_faq);
        Log.d(FabegeApplication.TAG, "onShowFaqItem");
        pushFragment(WebViewFragment.newInstance("https://www.fabege.se/vara-kunder/vanliga-fragor"));
        AnalyticsImpl.getInstance().trackEvent(AnalyticsUtils.makePageViewEvent("FAQ", getActiveStructure(), null));
    }

    @Override
    public void onHomeClicked() {
        if (startSelect) {
            startSelect = false;
            pushFragment(SelectFragment.newInstance());

        } else {
            pushFragment(HomeFragment.newInstance());
        }
    }

    @Override
    public void onWAWClicked() {
        startActivity(new Intent(this, WAWActivity.class));
    }

    @Override
    public void setActiveStructure(Structure structure) {
        mActiveStructure = structure;
    }

    @Override
    public void setOngoingWork(WorkResponse response) {
        mOngoingWork = response;
    }

    @Override
    public void onSubmitComplete() {
        Log.d(FabegeApplication.TAG, "onSubmitComplete");

        popBackStack();
    }

    @Override
    public void onChangeStructure() {
        Log.d(FabegeApplication.TAG, "onChangeStructure");
        pushFragment(SelectFragment.newInstance(true, mActiveStructure.getRowId()));
    }

    @Override
    public WorkResponse getOngoingWork() {
        return mOngoingWork;
    }

    @Override
    public Structure getActiveStructure() {
        return mActiveStructure;
    }

    @Override
    public void onSelectionDone(boolean wasSearchMode) {

        Log.d(FabegeApplication.TAG, "onSelectionDone");
        FragmentManager supportFragmentManager = getSupportFragmentManager();
        if (wasSearchMode) {
            popBackStack();
        } else {
            FragmentTransaction transaction = supportFragmentManager.beginTransaction();
            transaction.replace(R.id.container, HomeFragment.newInstance());
            transaction.commit();
        }
    }

    @Override
    public void onStructureSelected(Structure structure) {
        Log.d(FabegeApplication.TAG, "onStructureSelected");
        mActiveStructure = structure;
        popBackStack();
    }

    @Override
    public void onShowFaqItem(FaqFragment.FaqItem faqItem, String url) {

        Log.d(FabegeApplication.TAG, "onShowFaqItem");
        pushFragment(WebViewFragment.newInstance(url));
        AnalyticsImpl.getInstance().trackEvent(AnalyticsUtils.makePageViewEvent("FAQ", getActiveStructure(), faqItem));
    }

    public void showToolbar() {
        toolbar.setVisibility(View.VISIBLE);
    }

    public void hideToolbar() {
        toolbar.setVisibility(View.GONE);
    }
}
