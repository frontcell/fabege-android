package se.fabege.app.activity;

import android.content.Intent;
import android.os.Bundle;

import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.answers.Answers;

import io.fabric.sdk.android.Fabric;
import se.fabege.app.R;
import se.fabege.app.base.BaseActivity;
import se.fabege.app.base.BaseFragment;
import se.fabege.app.fragment.SplashFragment;
import se.fabege.provider.model.Structure;

/**
 * Created by Grindah on 2015-03-29.
 */
public class SplashActivity extends BaseActivity implements SplashFragment.ISplashFragment, BaseFragment.IFabegeBaseFragment {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        Fabric.with(this, new Answers());
        setContentView(R.layout.activity_splash);
    }

    @Override
    public void onLoadComplete() {
        Intent intent = new Intent(this, HomeActivity.class);
        startActivity(intent);
        finish();
    }


    @Override
    public Structure getActiveStructure() {
        return null;
    }
}
