package se.fabege.app.storage;

import android.app.IntentService;
import android.content.ContentProviderOperation;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Intent;
import android.content.OperationApplicationException;
import android.database.Cursor;
import android.os.RemoteException;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import org.simpleframework.xml.core.Persister;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

import se.fabege.api.Api;
import se.fabege.api.TokenFactory;
import se.fabege.api.dto.Structure;
import se.fabege.api.dto.StructureResponse;
import se.fabege.api.dto.params.StructureQueryParams;
import se.fabege.api.dto.session.SessionKey;
import se.fabege.app.base.FabegeApplication;
import se.fabege.provider.database.table.StructureTable;
import se.fabege.provider.provider.FabegeProvider;

/**
 * Created by Grindah on 2015-04-08.
 */
public class UpdateService extends IntentService {
    public static final String ACTION_UPDATE_DB = "UPDATE_DATABASE";
    public static final String ACTION_DB_UPDATED = "DB_UPDATED";


    public UpdateService() {
        super("Fabege Update Service");
    }


    @Override
    protected void onHandleIntent(Intent intent) {
        long timestamp = System.currentTimeMillis();
        if (intent.getAction().equals(ACTION_UPDATE_DB)) {
            loadStructures();
        }


        Log.d(FabegeApplication.TAG, intent.getAction() + " processed in " + (System.currentTimeMillis() - timestamp) + "ms");
    }


    private SessionKey createSession() {
        final TokenFactory.Token token = TokenFactory.generateToken();

        final SessionKey session = Api.getInstance().login(token.getUserName(), token.getTimestamp(), token.getHash());

        return session;
    }

    private void  loadStructures() {

        Log.d(FabegeApplication.TAG, "loadStructures");

        long timestamp = System.currentTimeMillis();
        StructureQueryParams q = new StructureQueryParams();
        StructureResponse response = null;
        try {

            final SessionKey session = createSession(); // network call
            final StringWriter serializedXml = new StringWriter();
            Log.d(FabegeApplication.TAG, "Session created: " + session);

            if (session != null) {
                new Persister().write(q, serializedXml);

                response = Api.getInstance().getStructures("Fabege_GetStructures", session.mKey, serializedXml.toString());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        Log.d(FabegeApplication.TAG, "loadStructures took " + (System.currentTimeMillis() - timestamp));
        if (response != null) {
            updateStructuresTable(response.structureList);
        }
    }

    private void updateStructuresTable(List<Structure> structures) {
        ArrayList<ContentProviderOperation> operations = new ArrayList<>();
        String[] defaultProjection = new String[]{StructureTable._ID};
        final String updateTag = String.valueOf(System.currentTimeMillis());

        final String where = StructureTable.SOURCE + " = ?";
        ContentResolver contentResolver = getContentResolver();
        ContentValues values = new ContentValues();
        for (Structure s : structures) {
            String[] whereArgs = new String[]{String.valueOf(s.sourceId)};

            final Cursor c = contentResolver.query(FabegeProvider.STRUCTURE_CONTENT_URI, defaultProjection, where, whereArgs, null);
            int count = c.getCount();
            c.close();
            ContentProviderOperation operation = null;
            if (count == 0) { // insert
                Log.d(FabegeApplication.TAG, "INSERT " + s.name);
                values = toContextValues(s, values, updateTag);
                values.put(StructureTable.IS_FAVOURITE, 0);
                operation = ContentProviderOperation.newInsert(FabegeProvider.STRUCTURE_CONTENT_URI)
                        .withValues(values)
                        .build();
            } else { // update
                Log.d(FabegeApplication.TAG, "UPDATE " + s.name);
                operation = ContentProviderOperation.newUpdate(FabegeProvider.STRUCTURE_CONTENT_URI)
                        .withSelection(where, whereArgs)
                        .withValues(toContextValues(s, values, updateTag))
                        .build();
            }
            operations.add(operation);
        }

        ContentProviderOperation deleteOperation =
                ContentProviderOperation.newDelete(FabegeProvider.STRUCTURE_CONTENT_URI)
                        .withSelection(StructureTable.UPDATED + " != ?", new String[]{updateTag})
                        .build();

        operations.add(deleteOperation);

        try {
            contentResolver.applyBatch(FabegeProvider.AUTHORITY, operations);
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (OperationApplicationException e) {
            e.printStackTrace();
        }

        Intent broadcast = new Intent(ACTION_DB_UPDATED);
        LocalBroadcastManager.getInstance(this).sendBroadcast(broadcast);
    }


    private ContentValues toContextValues(Structure s, ContentValues recycle, String updateTag) {
        ContentValues values = recycle;
        if (values == null) {
            values = new ContentValues();
        } else {
            values.clear();
        }

        values.put(StructureTable.DESCRIPTION, s.description);
        values.put(StructureTable.DESTINATION_ID, s.destinationId);
        values.put(StructureTable.LATITUDE, s.latitude);
        values.put(StructureTable.LONGITUDE, s.longitude);
        values.put(StructureTable.NAME, s.name);
        values.put(StructureTable.PROPERTY_ID, s.propertyId);
        values.put(StructureTable.SOURCE, s.sourceId);
        values.put(StructureTable.UPDATED, updateTag);
        return values;
    }

}

