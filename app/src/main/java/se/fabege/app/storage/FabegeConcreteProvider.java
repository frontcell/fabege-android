package se.fabege.app.storage;

import android.content.ContentProviderOperation;
import android.content.ContentProviderResult;
import android.content.OperationApplicationException;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

import se.fabege.provider.provider.FabegeProvider;

/**
 * Created by Grindah on 2015-04-17.
 */
public class FabegeConcreteProvider extends FabegeProvider {
    @Override
    public ContentProviderResult[] applyBatch(ArrayList<ContentProviderOperation> operations)
            throws OperationApplicationException {

        final SQLiteDatabase db = mDatabase.getWritableDatabase();
        final int numOperations = operations.size();
        final ContentProviderResult[] results = new ContentProviderResult[numOperations];
        db.beginTransaction();
        try {
            for (int i = 0; i < numOperations; i++) {
                results[i] = operations.get(i).apply(this, results, i);
            }
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }
        return results;
    }
}
