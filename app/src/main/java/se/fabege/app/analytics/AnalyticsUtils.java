package se.fabege.app.analytics;

import java.util.HashMap;
import java.util.Map;

import se.fabege.app.fragment.FaqFragment;
import se.fabege.provider.model.Structure;

/**
 * Created by Vu Phan on 2017-02-16.
 */

public class AnalyticsUtils {

    // EVENT NAMES.
    private static final String PAGE_VIEW = "Page View";
    private static final String ADD_PROPERTY ="Add Property";
    private static final String SERVICE_REQUEST_SUCCESS = "Service Request Success";
    private static final String SERVICE_REQUEST_FAIL = "Service Request Fail";

    // ATTRIBUTES.
    private static final String PAGE = "Page";
    private static final String PROPERTY_ADDRESS = "Property Address";
    private static final String PROPERTY_NAME = "Property Name";
    private static final String FAQ_TYPE = "Faq Type";
    private static final String IMAGE = "Image";

    private AnalyticsUtils() {

    }

    public static Event makePageViewEvent(String page, Structure structure) {
        return makePageViewEvent(page, structure, null);
    }

    public static Event makePageViewEvent(String page, Structure structure, FaqFragment.FaqItem faqItem) {
        Map<String, String> attributes = new HashMap<>();
        attributes.put(PAGE, page);
        attributes.put(PROPERTY_ADDRESS, structure.getName());
        attributes.put(PROPERTY_NAME, structure.getDescription());
        if (faqItem != null) {
            attributes.put(FAQ_TYPE, faqItem.toString().toLowerCase());
        }


        return new Event(PAGE_VIEW, attributes);
    }



    public static Event makeAddPropertyEvent(Structure structure) {
        Map<String, String> attributes = new HashMap<>();
        attributes.put(PROPERTY_ADDRESS, structure.getName());
        attributes.put(PROPERTY_NAME, structure.getDescription());

        return new Event(ADD_PROPERTY, attributes);
    }

    public static Event makeServiceRequestSuccessEvent(Structure structure, boolean bitmap) {
        Map<String, String> attributes = new HashMap<>();
        attributes.put(PROPERTY_ADDRESS, structure.getName());
        attributes.put(PROPERTY_NAME, structure.getDescription());
        attributes.put(IMAGE, bitmap? "1" : "0");

        return new Event(SERVICE_REQUEST_SUCCESS, attributes);
    }

    public static Event makeServiceRequestFailEvent(Structure structure, boolean bitmap) {
        Map<String, String> attributes = new HashMap<>();
        attributes.put(PROPERTY_ADDRESS, structure.getName());
        attributes.put(PROPERTY_NAME, structure.getDescription());
        attributes.put(IMAGE, bitmap? "1" : "0");

        return new Event(SERVICE_REQUEST_FAIL, attributes);
    }
}
