package se.fabege.app.analytics;

import android.os.Bundle;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;
import com.google.firebase.analytics.FirebaseAnalytics;

import java.util.Map;

import se.fabege.app.base.GlobalState;

/**
 * Created by Vu Phan on 2017-02-16.
 */

public class AnalyticsImpl implements Analytics {

    private static Analytics instance = null;

    public static Analytics getInstance() {
        if (instance == null) {
            instance = new AnalyticsImpl();
        }
        return instance;
    }

    private AnalyticsImpl() {
    }

    @Override
    public void trackPageView(Event event) {
        Bundle params = new Bundle();
        params.putString("name", event.getEventName());

        FirebaseAnalytics firebaseAnalytics = FirebaseAnalytics.getInstance(GlobalState.getContext());
        firebaseAnalytics.logEvent("Page_View", params);
    }

    @Override
    public void trackEvent(Event event) {
//        Answers.getInstance().logCustom(createCustomEvent(event.getEventName(), event.getAttributes()));

        Bundle params = new Bundle();
        for (Map.Entry<String, String> entry : event.getAttributes().entrySet()) {
            params.putString(entry.getKey(), entry.getValue());
        }
        FirebaseAnalytics firebaseAnalytics = FirebaseAnalytics.getInstance(GlobalState.getContext());
        firebaseAnalytics.logEvent(event.getEventName(), params);
    }

    private CustomEvent createCustomEvent(String eventName, Map<String, String> attributes) {
        CustomEvent customEvent = new CustomEvent(eventName);

        for (Map.Entry<String, String> attribute : attributes.entrySet()) {
            customEvent.putCustomAttribute(attribute.getKey(), attribute.getValue());
        }

        return customEvent;
    }
}
