package se.fabege.app.analytics;

import java.util.Map;

/**
 * Created by Vu Phan on 2017-02-16.
 */

public class Event {

    private String eventName;
    private Map<String, String> attributes;

    public Event(String eventName, Map<String, String> attributes) {
        this.eventName = eventName;
        this.attributes = attributes;
    }

    public String getEventName() {
        return eventName;
    }

    public Map<String, String> getAttributes() {
        return attributes;
    }

}
