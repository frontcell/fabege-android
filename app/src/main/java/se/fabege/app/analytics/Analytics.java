package se.fabege.app.analytics;

/**
 * Created by Vu Phan on 2017-02-16.
 */

public interface Analytics {
    void trackPageView(Event event);

    void trackEvent(Event event);
}
