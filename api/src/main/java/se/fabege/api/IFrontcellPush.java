package se.fabege.api;

import retrofit.Callback;
import retrofit.client.Response;
import retrofit.http.POST;
import retrofit.http.Query;

/**
 * Created by Grindah on 2015-04-27.
 */
public interface IFrontcellPush {
    @POST("/push/register/android")
    Response updatePush(@Query("deviceId") String deviceId, @Query("propertyIds") String propertyIds);

    @POST("/push/register/android")
    void updatePush(@Query("deviceId") String deviceId, @Query("propertyIds") String propertyIds, Callback<String> cb);
}
