package se.fabege.api;

import android.util.Base64;
import android.util.Log;

import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

/**
 * Created by Grindah on 2015-04-02.
 */
public final class TokenFactory {
    private final static String TAG = "TokenFactory";
    private final static String USER = "WS_Fabege_FC";
    private final static String PASSWORD = "d6FSSkU65H6rbA";

    private final static DateTimeFormatter sFormatter = ISODateTimeFormat.dateTimeNoMillis().withZone(DateTimeZone.UTC);

    private TokenFactory() {
        // no instances
    }

    public static final class Token {

        private final String mUserName = USER;
        private final String mTimestamp;
        private final String mHash;

        public Token(String hash, String timestamp) {
            mTimestamp = timestamp;
            mHash = hash;
        }

        public String getTimestamp() {
            return mTimestamp;
        }

        public String getHash() {
            return mHash;
        }

        public String getUserName() {
            return mUserName;
        }

        @Override
        public String toString() {
            return "Token{" +
                    "mTimestamp='" + mTimestamp + '\'' +
                    ", mHash='" + mHash + '\'' +
                    '}';
        }
    }

    private static String sha1(String message, String password) throws
            UnsupportedEncodingException, NoSuchAlgorithmException,
            InvalidKeyException {

        SecretKeySpec key = new SecretKeySpec((password).getBytes("UTF-8"), "HmacSHA1");
        Mac mac = Mac.getInstance("HmacSHA1");
        mac.init(key);

        byte[] bytes = mac.doFinal(message.getBytes("UTF-8"));
        String encoded = new String(Base64.encode(bytes, Base64.NO_WRAP)).replace('+', '-').replace('=', '_').replace('/', '~');

        return encoded;
    }

    public static Token generateToken() {
        Token result = null;
        try {
            String timestamp = sFormatter.print(System.currentTimeMillis());
            String hash = sha1(USER + timestamp, PASSWORD);

            result = new Token(hash, timestamp);
            Log.d(TAG, result.toString());
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        }
        return result;
    }
}
