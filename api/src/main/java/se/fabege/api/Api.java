package se.fabege.api;


import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.client.Response;
import retrofit.http.Body;
import retrofit.http.Query;
import retrofit.mime.TypedByteArray;
import retrofit.mime.TypedInput;
import se.fabege.api.dto.StructureResponse;
import se.fabege.api.dto.session.SessionKey;
import se.fabege.api.dto.work.WorkResponse;
import se.fabege.api.ext.BomSimpleXmlConverter;

/**
 * Created by Grindah on 2015-04-02.
 */
public class Api implements IFabegeApi {
    private static Api ourInstance = new Api();
    private final IFabegeApi mService;

    public static Api getInstance() {
        return ourInstance;
    }

    private Api() {
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint("http://www.dedu.se/DeDUService")
                .setLogLevel(RestAdapter.LogLevel.NONE)
                .setConverter(new BomSimpleXmlConverter())
                .build();

        mService = restAdapter.create(IFabegeApi.class);
    }

    @Override
    public void login(String username, String timeStamp, String hash, Callback<SessionKey> cb) {
        mService.login(username, timeStamp, hash, cb);
    }

    @Override
    public SessionKey login(@Query("Username") String username, @Query("Timestamp") String timeStamp, @Query("Hash") String hash) {
        return mService.login(username, timeStamp, hash);
    }

    @Override
    public void getStructures(@Query("TemplateName") String templateName, @Query("SessionKey") String sessionKey, @Query("XmlFile") String xmlFile, Callback<StructureResponse> cb) {
        mService.getStructures(templateName, sessionKey, xmlFile, cb);
    }

    @Override
    public StructureResponse getStructures(@Query("TemplateName") String templateName, @Query("SessionKey") String sessionKey, @Query("XmlFile") String xmlFile) {
        return mService.getStructures(templateName, sessionKey, xmlFile);
    }

    @Override
    public void getOngoingWork(@Query("TemplateName") String templateName, @Query("SessionKey") String sessionKey, @Query("XmlFile") String xmlFile, Callback<WorkResponse> cb) {
        mService.getOngoingWork(templateName, sessionKey, xmlFile, cb);
    }

    @Override
    public WorkResponse getOngoingWork(@Query("TemplateName") String templateName, @Query("SessionKey") String sessionKey, @Query("XmlFile") String xmlFile) {
        return mService.getOngoingWork(templateName, sessionKey, xmlFile);
    }

    @Override
    public void createTask(@Query("TemplateName") String templateName, @Query("SessionKey") String sessionKey, @Body TypedInput xmlFile, Callback<String> cb) {
        mService.createTask(templateName, sessionKey, xmlFile, cb);
    }

    @Override
    public Response createTask(@Query("TemplateName") String templateName, @Query("SessionKey") String sessionKey, @Body TypedInput xmlFile) {
        return mService.createTask(templateName, sessionKey, xmlFile);
    }

    @Override
    public Response uploadFile(@Query("SessionKey") String sessionKey, @Query("xmlData") String xmlFile, @Body TypedByteArray imageData) {
        return mService.uploadFile(sessionKey, xmlFile, imageData);
    }

    @Override
    public void uploadFile(@Query("SessionKey") String sessionKey, @Query("xmlData") String xmlFile, @Body TypedByteArray imageData, Callback<String> cb) {
        mService.uploadFile(sessionKey, xmlFile, imageData, cb);
    }
}
