package se.fabege.api.ext;

import java.lang.ref.WeakReference;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Grindah on 2014-05-20.
 */
public class SafeCallback<T> implements Callback<T> {
    private final WeakReference<Callback<T>> mWeakCallback;

    public SafeCallback(Callback<T> cb) {
        mWeakCallback = new WeakReference<Callback<T>>(cb);
    }

    @Override
    public void success(T t, Response response) {
        final Callback<T> cb = mWeakCallback.get();
        if (cb != null) {
            cb.success(t, response);
        }
    }

    @Override
    public void failure(RetrofitError error) {
        final Callback<T> cb = mWeakCallback.get();
        if (cb != null) {
            cb.failure(error);
        }
    }
}
