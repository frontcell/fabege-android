package se.fabege.api.dto.work;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 * Created by Grindah on 2015-04-21.
 */
@Root(strict = false)
public class WorkItem {

    @Attribute(name = "ID")
    public int ID;

    @Element(name = "DriftStorningLista", required = false)
    public IncidentList incidentList;

    @Element(name = "AktuelltIHusetLista", required = false)
    public NewsList newsList;

    @Override
    public String toString() {
        return "WorkItem{" +
                "ID=" + ID +
                ", incidentList=" + incidentList +
                ", newsList=" + newsList +
                '}';
    }
}
