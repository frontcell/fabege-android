package se.fabege.api.dto;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 * Created by Grindah on 2015-04-17.
 */
@Root(name = "DocumentFile")
public class DocumentFile {

    @Element(name = "ObjectID")
    public String objectId;

    @Element(name = "ObjectType")
    public String objectType = "Task";

    @Element(name = "Filename")
    public String fileName = "image.jpg";
}
