package se.fabege.api.dto.work;

import org.simpleframework.xml.Element;

/**
 * Created by Grindah on 2015-04-21.
 */
public class ListEntry {
    @Element(name = "Rubrik", required = false)
    public String title;

    @Element(name = "memoBeskrivning", required = false)
    public String description;

    @Element(name = "PubliceraTill", required = false)
    public String date;
}
