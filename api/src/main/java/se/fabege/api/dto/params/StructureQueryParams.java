package se.fabege.api.dto.params;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 * Created by Grindah on 2015-04-02.
 */
@Root(name = "GetStructureInputParameters")
public class StructureQueryParams {
    @Element(name = "PropertyId", required = false)
    public Integer propertyId;

    @Element(name = "OnlyMarkedForInternet")
    int onlyInternet = 1;
}
