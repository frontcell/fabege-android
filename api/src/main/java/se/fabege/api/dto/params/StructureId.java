package se.fabege.api.dto.params;

import org.simpleframework.xml.ElementList;

import java.util.List;

/**
 * Created by Grindah on 2015-04-20.
 */
public class StructureId {
    @ElementList(name = "int", required = false, inline = true)
    public List<Integer> ids;

}