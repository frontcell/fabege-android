package se.fabege.api.dto.work;

import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.List;

/**
 * Created by Grindah on 2015-04-21.
 */
@Root(strict = false)
public class IncidentList {
    @ElementList(entry = "DriftStorning", inline = true, required = false)
    public List<Incident> incidents;

    @Override
    public String toString() {
        return "IncidentList{" +
                "incidents=" + incidents +
                '}';
    }
}
