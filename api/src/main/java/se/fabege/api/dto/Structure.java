package se.fabege.api.dto;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 * Created by Grindah on 2015-04-06.
 */
@Root(name = "Structure")
public class Structure {
    @Element(name = "PropertyId", required = false)
    public Integer propertyId;

    @Element(name = "SourceId", required = false)
    public Integer sourceId;

    @Element(name = "DestinationId", required = false)
    public String destinationId;

    @Element(name = "Name", required = false)
    public String name;

    @Element(name = "Descr", required = false)
    public String description;

    @Element(name = "Longitude", required = false)
    public Double longitude;

    @Element(name = "Latitude", required = false)
    public Double latitude;

    @Override
    public String toString() {
        return "Structure{" +
                "propertyId=" + propertyId +
                ", sourceId=" + sourceId +
                ", destinationId='" + destinationId + '\'' +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", longitude=" + longitude +
                ", latitude=" + latitude +
                '}';
    }
}