package se.fabege.api.dto;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 * Created by Grindah on 2015-04-17.
 */
@Root(name = "Task")
public class Task {

    @Element(name = "Placement", required = false)
    public String placement;

    @Element(name = "Description", required = false)
    public String description;

    @Element(name = "SendTaskMail", required = false)
    public String sendTaskMail = "1";

    @Element(name = "Profession", required = false)
    public Profession profession;

    @Element(name = "ReportedBy", required = false)
    public ReportedBy reportedBy;

    @Element(name = "Structure", required = false)
    public House structure;


    @Root(name = "Profession")
    public static class Profession {
        @Element(name = "Id", required = false)
        public String id;
    }

    @Root(name = "ReportedBy")
    public static class ReportedBy {
        @Element(name = "Name", required = false)
        public String name;

        @Element(name = "Email", required = false)
        public String email;
        @Element(name = "Phone", required = false)
        public String phone;
    }

    @Root(name = "Structure")
    public static class House {
        @Element(name = "DestinationId", required = false)
        public String destinationId;
    }
}




