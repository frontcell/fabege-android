package se.fabege.api.dto.work;

import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Grindah on 2015-04-21.
 */
@Root(name = "Fastigheter", strict = false)
public class WorkResponse {
    @ElementList(entry = "Fastighet", inline = true, required = false)
    public List<WorkItem> workItems;

    public List<ListEntry> getEntriesForId(int id) {
        ArrayList<ListEntry> result = new ArrayList<>();
        if (workItems != null) {
            for (WorkItem item : workItems) {
                if (item.ID == id) {
                    // found the one we are looking for
                    if (item.incidentList != null) {
                        result.addAll(item.incidentList.incidents);
                    }

                    if (item.newsList != null) {
                        result.addAll(item.newsList.news);
                    }
                }
            }
        }
        return result;
    }

    @Override
    public String toString() {
        return "WorkResponse{" +
                "workItems=" + workItems +
                '}';
    }
}
