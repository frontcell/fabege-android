package se.fabege.api.dto.work;

import org.simpleframework.xml.Root;

/**
 * Created by Grindah on 2015-04-21.
 */
@Root(strict = false)
public class News extends ListEntry {

    @Override
    public String toString() {
        return "News{" +
                "title='" + title + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
