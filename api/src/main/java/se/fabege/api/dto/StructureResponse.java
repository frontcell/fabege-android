package se.fabege.api.dto;

import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.List;

/**
 * Created by Grindah on 2015-04-06.
 */
@Root(name = "ArrayOfStructure")
public class StructureResponse {
    @ElementList(entry = "Structure", inline = true)
    public List<Structure> structureList;

    @Override
    public String toString() {
        return "StructureResponse{" +
                "structureList=" + structureList +
                '}';
    }
}
