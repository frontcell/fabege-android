package se.fabege.api.dto.session;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 * Created by Grindah on 2015-04-02.
 */

@Root
public class SessionKey {
    @Element(name = "Value")
    public String mKey = null;

    @Override
    public String toString() {
        return "Session{" +
                "mKey='" + mKey + '\'' +
                '}';
    }
}
