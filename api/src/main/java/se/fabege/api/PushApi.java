package se.fabege.api;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.client.Response;
import retrofit.http.Query;

/**
 * Created by Grindah on 2015-04-27.
 */
public class PushApi implements IFrontcellPush {
    private static PushApi ourInstance = new PushApi();
    private final IFrontcellPush mService;

    public static PushApi getInstance() {
        return ourInstance;
    }

    private PushApi() {
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint("http://fabege.frontcell.se")
                .setLogLevel(RestAdapter.LogLevel.NONE)
                .build();

        mService = restAdapter.create(IFrontcellPush.class);
    }

    @Override
    public Response updatePush(@Query("deviceId") String deviceId, @Query("propertyIds") String propertyIds) {
        return mService.updatePush(deviceId, propertyIds);
    }

    @Override
    public void updatePush(@Query("deviceId") String deviceId, @Query("propertyIds") String propertyIds, Callback<String> cb) {
        mService.updatePush(deviceId, propertyIds, cb);
    }
}
