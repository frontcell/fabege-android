package se.fabege.api;

import retrofit.Callback;
import retrofit.client.Response;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.Headers;
import retrofit.http.POST;
import retrofit.http.Query;
import retrofit.mime.TypedByteArray;
import retrofit.mime.TypedInput;
import se.fabege.api.dto.StructureResponse;
import se.fabege.api.dto.session.SessionKey;
import se.fabege.api.dto.work.WorkResponse;

/**
 * Created by Grindah on 2015-04-02.
 */
public interface IFabegeApi {
    @GET("/Login")
    void login(@Query("Username") String username, @Query("Timestamp") String timeStamp, @Query("Hash") String hash, Callback<SessionKey> cb);

    @GET("/Login")
    SessionKey login(@Query("Username") String username, @Query("Timestamp") String timeStamp, @Query("Hash") String hash);

    @GET("/TemplatedXML")
    void getStructures(@Query("TemplateName") String templateName, @Query("SessionKey") String sessionKey, @Query("XmlFile") String xmlFile, Callback<StructureResponse> cb);

    @GET("/TemplatedXML")
    StructureResponse getStructures(@Query("TemplateName") String templateName, @Query("SessionKey") String sessionKey, @Query("XmlFile") String xmlFile);

    @GET("/TemplatedXML")
    void getOngoingWork(@Query("TemplateName") String templateName, @Query("SessionKey") String sessionKey, @Query("XmlFile") String xmlFile, Callback<WorkResponse> cb);

    @GET("/TemplatedXML")
    WorkResponse getOngoingWork(@Query("TemplateName") String templateName, @Query("SessionKey") String sessionKey, @Query("XmlFile") String xmlFile);

    @Headers("Content-Type: application/xml; charset=utf-8")
    @POST("/TemplatedXML")
    void createTask(@Query("TemplateName") String templateName, @Query("SessionKey") String sessionKey, @Body TypedInput xmlFile, Callback<String> cb);

    //@Headers("Content-Type: application/xml; charset=utf-8")
    @POST("/TemplatedXML")
    Response createTask(@Query("TemplateName") String templateName, @Query("SessionKey") String sessionKey, @Body TypedInput xmlFile);

    @POST("/UploadFile")
    Response uploadFile(@Query("SessionKey") String sessionKey, @Query("xmlData") String xmlFile, @Body TypedByteArray imageData);

    @POST("/UploadFile")
    void uploadFile(@Query("SessionKey") String sessionKey, @Query("xmlData") String xmlFile, @Body TypedByteArray imageData, Callback<String> cb);

}
